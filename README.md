This project will contain my things related to the old terminals in the 264x series from Hewlett Packard.
These terminals were some of the earliest "intelligent" terminals around as they were driven by a microprocessor
and some even were directly programmable (2647).

I currently own a 2640A with chips with datecodes from 1975 so its a very early one.