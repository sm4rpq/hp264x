EESchema Schematic File Version 4
LIBS:DisplayControlPCA_60112-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3700 2550 0    50   Input ~ 0
TPON
Text HLabel 3700 2450 0    50   Input ~ 0
VRT_CLK
Text HLabel 6550 3050 2    50   Output ~ 0
LC0
Text HLabel 6550 3150 2    50   Output ~ 0
LC1
Text HLabel 6550 3250 2    50   Output ~ 0
LC2
Text HLabel 6550 3350 2    50   Output ~ 0
LC3
$Comp
L power:VCC #PWR?
U 1 1 5F461168
P 5200 2150
F 0 "#PWR?" H 5200 2000 50  0001 C CNN
F 1 "VCC" H 5217 2323 50  0000 C CNN
F 2 "" H 5200 2150 50  0001 C CNN
F 3 "" H 5200 2150 50  0001 C CNN
	1    5200 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F461606
P 5200 3850
F 0 "#PWR?" H 5200 3600 50  0001 C CNN
F 1 "GND" H 5205 3677 50  0000 C CNN
F 2 "" H 5200 3850 50  0001 C CNN
F 3 "" H 5200 3850 50  0001 C CNN
	1    5200 3850
	1    0    0    -1  
$EndComp
$Comp
L sm4rpq:GAL22V10 U?
U 1 1 5F801EFE
P 5200 2900
F 0 "U?" H 4950 3550 50  0000 C CNN
F 1 "GAL22V10" H 5450 3550 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W7.62mm_LongPads" H 5200 2900 50  0001 C CNN
F 3 "" H 5200 2900 50  0001 C CNN
	1    5200 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3050 6550 3050
Wire Wire Line
	5750 3150 6550 3150
Wire Wire Line
	5750 3250 6550 3250
Wire Wire Line
	5750 3350 6550 3350
Wire Wire Line
	3700 2450 4650 2450
Wire Wire Line
	3700 2550 4650 2550
Text HLabel 6550 2950 2    50   Output ~ 0
~L14~
Text HLabel 6550 2850 2    50   Output ~ 0
L11
Text HLabel 6550 2750 2    50   Output ~ 0
~L0~
Text HLabel 6550 2650 2    50   Output ~ 0
CLEN
Wire Wire Line
	6550 2950 5750 2950
Wire Wire Line
	5750 2850 6550 2850
Wire Wire Line
	6550 2750 5750 2750
Wire Wire Line
	5750 2650 6550 2650
NoConn ~ 4650 3450
NoConn ~ 4650 3550
Text HLabel 3700 2650 0    50   Input ~ 0
~ADDR4~
Text HLabel 3700 2750 0    50   Input ~ 0
~ADDR5~
Text HLabel 3700 2850 0    50   Input ~ 0
~ADDR9~
Text HLabel 3700 2950 0    50   Input ~ 0
~ADDR10~
Text HLabel 3700 3050 0    50   Input ~ 0
~ADDR11~
Text HLabel 3700 3150 0    50   Input ~ 0
~I_O~
Text HLabel 3700 3250 0    50   Input ~ 0
~WRT~
Text HLabel 3700 3350 0    50   Input ~ 0
~REQ~
Wire Wire Line
	3700 2650 4650 2650
Wire Wire Line
	3700 2750 4650 2750
Wire Wire Line
	4650 2850 3700 2850
Wire Wire Line
	3700 2950 4650 2950
Wire Wire Line
	4650 3050 3700 3050
Wire Wire Line
	3700 3150 4650 3150
Wire Wire Line
	4650 3250 3700 3250
Wire Wire Line
	3700 3350 4650 3350
Text HLabel 6550 2450 2    50   Output ~ 0
~CYS~
Text HLabel 6550 2550 2    50   Output ~ 0
~CXS~
Wire Wire Line
	5750 2450 6550 2450
Wire Wire Line
	6550 2550 5750 2550
$EndSCHEMATC
