EESchema Schematic File Version 4
LIBS:DisplayControlPCA_60112-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4000 3550 3350 3550
Text HLabel 3350 3550 0    50   Input ~ 0
~DSPY_CLK~
Text HLabel 6100 3850 2    50   Output ~ 0
~D6_E~
Text HLabel 6100 3950 2    50   Output ~ 0
~D2_E~
Text HLabel 6100 4050 2    50   Output ~ 0
~D1_E~
Text HLabel 6100 4450 2    50   Output ~ 0
~D0_E~
Text HLabel 3350 3850 0    50   Input ~ 0
TPON
$Comp
L power:VCC #PWR0123
U 1 1 5F410D59
P 4550 3250
F 0 "#PWR0123" H 4550 3100 50  0001 C CNN
F 1 "VCC" H 4567 3423 50  0000 C CNN
F 2 "" H 4550 3250 50  0001 C CNN
F 3 "" H 4550 3250 50  0001 C CNN
	1    4550 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0125
U 1 1 5F411538
P 4550 4950
F 0 "#PWR0125" H 4550 4700 50  0001 C CNN
F 1 "GND" H 4555 4777 50  0000 C CNN
F 2 "" H 4550 4950 50  0001 C CNN
F 3 "" H 4550 4950 50  0001 C CNN
	1    4550 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 4050 5100 4050
Wire Wire Line
	5100 3950 6100 3950
Wire Wire Line
	6100 3850 5100 3850
Text HLabel 6100 3750 2    50   Output ~ 0
SHFT
Wire Wire Line
	6100 3750 5800 3750
NoConn ~ 4000 3750
NoConn ~ 4000 3950
NoConn ~ 4000 4050
NoConn ~ 4000 4150
NoConn ~ 4000 4250
NoConn ~ 4000 4350
NoConn ~ 4000 4450
NoConn ~ 4000 4650
$Comp
L sm4rpq:GAL22V10 U11
U 1 1 5F726833
P 4550 4000
F 0 "U11" H 4300 4600 50  0000 C CNN
F 1 "GAL22V10" H 4800 4600 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W7.62mm_LongPads" H 4550 4000 50  0001 C CNN
F 3 "" H 4550 4000 50  0001 C CNN
	1    4550 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3850 4000 3850
Wire Wire Line
	4000 3650 3350 3650
Text HLabel 3350 3650 0    50   Input ~ 0
~DBIT0~
NoConn ~ 5100 4150
NoConn ~ 5100 4250
NoConn ~ 5100 4350
Wire Wire Line
	6100 4450 5100 4450
NoConn ~ 5100 3650
Text HLabel 6100 3550 2    50   Output ~ 0
~D8_E~
NoConn ~ 4000 4550
Wire Wire Line
	5100 3550 6100 3550
Text Notes 5300 4450 2    50   ~ 0
Q3
Text Notes 5300 4350 2    50   ~ 0
Q2
Text Notes 5300 4250 2    50   ~ 0
Q1
Text Notes 5300 4150 2    50   ~ 0
Q0
Text Notes 5700 3650 2    50   ~ 0
Latched_DBIT0
$Comp
L Connector:TestPoint TP3
U 1 1 5F4E4941
P 5800 3300
F 0 "TP3" H 5858 3418 50  0000 L CNN
F 1 "SHFT" H 5858 3327 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 6000 3300 50  0001 C CNN
F 3 "~" H 6000 3300 50  0001 C CNN
	1    5800 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3750 5800 3300
Connection ~ 5800 3750
Wire Wire Line
	5800 3750 5100 3750
$EndSCHEMATC
