EESchema Schematic File Version 4
LIBS:DisplayControlPCA_60112-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3350 2650 0    50   Input ~ 0
~14~
Wire Wire Line
	6050 3150 6750 3150
Text HLabel 6750 3150 2    50   Output ~ 0
A
Text HLabel 6750 3250 2    50   Output ~ 0
B
Text HLabel 6750 3350 2    50   Output ~ 0
C
Text HLabel 6750 3450 2    50   Output ~ 0
D
Text HLabel 6750 3550 2    50   Output ~ 0
E
Wire Wire Line
	6750 3550 6050 3550
Wire Wire Line
	6750 3250 6050 3250
Wire Wire Line
	6050 3350 6750 3350
$Comp
L Connector:TestPoint TP1
U 1 1 5F4015DD
P 3950 3550
F 0 "TP1" V 4050 3700 50  0000 C CNN
F 1 "CLR" V 3850 3650 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 4150 3550 50  0001 C CNN
F 3 "~" H 4150 3550 50  0001 C CNN
	1    3950 3550
	0    -1   -1   0   
$EndComp
Text GLabel 4050 3300 1    50   Input ~ 0
R1D
Wire Wire Line
	4050 3300 4050 3550
Wire Wire Line
	4050 3550 3950 3550
Text HLabel 3350 2750 0    50   Input ~ 0
TPON
Text HLabel 6750 3050 2    50   Output ~ 0
VBLNK
$Comp
L sm4rpq:GAL22V10 U1
U 1 1 5F4EC2B4
P 5500 3100
F 0 "U1" H 5200 3700 50  0000 C CNN
F 1 "GAL22V10" H 5750 3700 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W7.62mm_LongPads" H 5500 3100 50  0001 C CNN
F 3 "" H 5500 3100 50  0001 C CNN
	1    5500 3100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0101
U 1 1 5F4ECE70
P 5500 2350
F 0 "#PWR0101" H 5500 2200 50  0001 C CNN
F 1 "VCC" H 5517 2523 50  0000 C CNN
F 2 "" H 5500 2350 50  0001 C CNN
F 3 "" H 5500 2350 50  0001 C CNN
	1    5500 2350
	1    0    0    -1  
$EndComp
Text HLabel 3350 2850 0    50   Input ~ 0
~103~
Text HLabel 3350 2950 0    50   Input ~ 0
~L0~
Wire Wire Line
	3350 2850 4950 2850
Wire Wire Line
	3350 2950 4950 2950
Wire Wire Line
	6050 2950 6750 2950
Text HLabel 6750 2950 2    50   Output ~ 0
EVEN
Wire Wire Line
	6050 2850 6750 2850
Wire Wire Line
	6050 2750 6750 2750
Text HLabel 6750 2850 2    50   Output ~ 0
GVS
Text HLabel 6750 2750 2    50   Output ~ 0
INTSET
Wire Wire Line
	6750 3050 6050 3050
Wire Wire Line
	6050 3450 6750 3450
$Comp
L power:GND #PWR0102
U 1 1 5F5B632A
P 5500 4050
F 0 "#PWR0102" H 5500 3800 50  0001 C CNN
F 1 "GND" H 5505 3877 50  0000 C CNN
F 2 "" H 5500 4050 50  0001 C CNN
F 3 "" H 5500 4050 50  0001 C CNN
	1    5500 4050
	1    0    0    -1  
$EndComp
NoConn ~ 4950 3150
NoConn ~ 4950 3250
NoConn ~ 4950 3350
NoConn ~ 4950 3450
NoConn ~ 4950 3550
NoConn ~ 4950 3650
NoConn ~ 4950 3750
Wire Wire Line
	6050 2650 6750 2650
Wire Wire Line
	4050 3550 4600 3550
Wire Wire Line
	4600 3550 4600 3050
Wire Wire Line
	4600 3050 4950 3050
Connection ~ 4050 3550
Text HLabel 6750 2650 2    50   Output ~ 0
VDR
Wire Wire Line
	3350 2750 4950 2750
Wire Wire Line
	3350 2650 4950 2650
$EndSCHEMATC
