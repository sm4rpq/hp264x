EESchema Schematic File Version 4
LIBS:DisplayControlPCA_60112-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4650 4150 4150 4150
Wire Wire Line
	4650 4050 4150 4050
Wire Wire Line
	4650 3950 4150 3950
Wire Wire Line
	4650 3850 4150 3850
Wire Wire Line
	5750 3550 6300 3550
$Comp
L power:VCC #PWR0170
U 1 1 5F5A0316
P 5200 2750
F 0 "#PWR0170" H 5200 2600 50  0001 C CNN
F 1 "VCC" H 5217 2923 50  0000 C CNN
F 2 "" H 5200 2750 50  0001 C CNN
F 3 "" H 5200 2750 50  0001 C CNN
	1    5200 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0171
U 1 1 5F5A038E
P 5200 4450
F 0 "#PWR0171" H 5200 4200 50  0001 C CNN
F 1 "GND" H 5205 4277 50  0000 C CNN
F 2 "" H 5200 4450 50  0001 C CNN
F 3 "" H 5200 4450 50  0001 C CNN
	1    5200 4450
	1    0    0    -1  
$EndComp
Text HLabel 4150 4700 0    50   Input ~ 0
E
Text HLabel 4150 4150 0    50   Input ~ 0
D
Text HLabel 4150 4050 0    50   Input ~ 0
C
Text HLabel 4150 3950 0    50   Input ~ 0
B
Text HLabel 4150 3850 0    50   Input ~ 0
A
Text HLabel 6300 3050 2    50   Output ~ 0
CURY0
Text HLabel 6300 3150 2    50   Output ~ 0
CURY1
Text HLabel 6300 3250 2    50   Output ~ 0
CURY2
Text HLabel 6300 3350 2    50   Output ~ 0
CURY3
Text HLabel 6300 3450 2    50   Output ~ 0
CURY4
Text HLabel 6300 3550 2    50   Output ~ 0
CYEN
$Comp
L sm4rpq:GAL22V10 U41
U 1 1 5F4C8662
P 5200 3500
F 0 "U41" H 4950 4150 50  0000 C CNN
F 1 "GAL22V10" H 5450 4150 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W7.62mm_LongPads" H 5200 3500 50  0001 C CNN
F 3 "" H 5200 3500 50  0001 C CNN
	1    5200 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3050 6300 3050
Wire Wire Line
	6300 3150 5750 3150
Wire Wire Line
	5750 3250 6300 3250
Wire Wire Line
	6300 3350 5750 3350
Wire Wire Line
	5750 3450 6300 3450
Wire Wire Line
	5850 4700 5850 3950
Wire Wire Line
	5850 3950 5750 3950
Wire Wire Line
	4150 4700 5850 4700
Text HLabel 4150 3050 0    50   Input ~ 0
~CYS~
Text HLabel 4150 3250 0    50   Input ~ 0
~BUS0~
Text HLabel 4150 3350 0    50   Input ~ 0
~BUS1~
Text HLabel 4150 3450 0    50   Input ~ 0
~BUS2~
Text HLabel 4150 3550 0    50   Input ~ 0
~BUS3~
Text HLabel 4150 3650 0    50   Input ~ 0
~BUS4~
Text HLabel 4150 3750 0    50   Input ~ 0
~BUS7~
Wire Wire Line
	4150 3750 4650 3750
Wire Wire Line
	4650 3650 4150 3650
Wire Wire Line
	4150 3550 4650 3550
Wire Wire Line
	4650 3450 4150 3450
Wire Wire Line
	4150 3350 4650 3350
Wire Wire Line
	4650 3250 4150 3250
Wire Wire Line
	4150 3050 4650 3050
Wire Wire Line
	4300 1950 4250 1950
$Comp
L Device:R R?
U 1 1 5F51F66E
P 4300 1650
AR Path="/5F4E30B3/5F51F66E" Ref="R?"  Part="1" 
AR Path="/5F4E32CC/5F51F66E" Ref="R4"  Part="1" 
F 0 "R4" H 4370 1696 50  0000 L CNN
F 1 "470" H 4370 1605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4230 1650 50  0001 C CNN
F 3 "~" H 4300 1650 50  0001 C CNN
	1    4300 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 1500 4300 1350
Wire Wire Line
	4300 1800 4300 1950
$Comp
L Device:C C?
U 1 1 5F51F676
P 4050 1350
AR Path="/5F4E30B3/5F51F676" Ref="C?"  Part="1" 
AR Path="/5F4E32CC/5F51F676" Ref="C17"  Part="1" 
F 0 "C17" V 3900 1350 50  0000 C CNN
F 1 ".1" V 4200 1350 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 4088 1200 50  0001 C CNN
F 3 "~" H 4050 1350 50  0001 C CNN
	1    4050 1350
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 1350 4200 1350
Connection ~ 4300 1350
$Comp
L power:GND #PWR?
U 1 1 5F51F67E
P 3850 1400
AR Path="/5F4E30B3/5F51F67E" Ref="#PWR?"  Part="1" 
AR Path="/5F4E32CC/5F51F67E" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 3850 1150 50  0001 C CNN
F 1 "GND" H 3855 1227 50  0000 C CNN
F 2 "" H 3850 1400 50  0001 C CNN
F 3 "" H 3850 1400 50  0001 C CNN
	1    3850 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1350 3850 1350
Wire Wire Line
	3850 1350 3850 1400
Wire Wire Line
	2900 1950 2850 1950
Text HLabel 2850 1950 0    50   Input ~ 0
PWR_ON
Wire Wire Line
	4550 1350 4550 3150
Wire Wire Line
	4550 3150 4650 3150
Wire Wire Line
	4300 1350 4550 1350
Text HLabel 6300 3650 2    50   Output ~ 0
~BLNK~
Wire Wire Line
	5750 3650 6300 3650
NoConn ~ 5750 3750
NoConn ~ 5750 3850
$Comp
L 74xx:74LS04 U22
U 5 1 5F6ECF96
P 3950 1950
F 0 "U22" H 3950 2267 50  0000 C CNN
F 1 "74S04" H 3950 2176 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 3950 1950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 3950 1950 50  0001 C CNN
	5    3950 1950
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U22
U 6 1 5F6EDAB9
P 3200 1950
F 0 "U22" H 3200 2267 50  0000 C CNN
F 1 "74S04" H 3200 2176 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 3200 1950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 3200 1950 50  0001 C CNN
	6    3200 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 1950 3650 1950
$EndSCHEMATC
