The display control board, PCA 02640-60112, contains two bipolar PROMs containing the character basic character set of
the 2640 terminal. One prom has the characters with ascii codes 32-95. The other one is optional and contains ASCII
codes 0-31 and 96-127 which includes to lower case character set. Even if these were once upon a time standard
components some kind of inverse radioactive decay has made them convert into unobtanium. So if you have a terminal
without the lowercase set another solution is needed.

This board replaces the sockets for the bipolar PROMs with a socket for a still obtainable 27256. On the original
board there are a mix of 74Sxx, 74Hxx, 74xx and 74LSxx. While these may still be sourced on ebay or from friends of
friends I made an effort to merge most of them into a few GAL devices. So 28 ICs on the original board became
8 on this board.

The GAL programming has been checked on a breadboard for most of the circuits and I have confidence enogh to say that if the rest is not working it would be easy to fix. Still a 74S04 is specified on the BOM but I expect a HCT device to
work.

![Kicad 3D rendring of board](02640-60112-galified.jpg)

I will not be producing this board even if it would not be that expensive if you cut off the unused ends. But how fun is a 264x terminal without the display enhancement board? Another product not available so my current plan is to make
a new board that combines the display control and display enhancement boards into a single board. Possibly also with additional 0-waitstate memory and one big CPLD instead of lots of small GAL devices.



A lazy solution without making a new board would likely be to just plug in a modern PROM with a few pins bent out of the socket and some wires added. While bipolar PROMs where fast at that time the timing requirement is no match for 
modern EPROMS. If I read the timing diagrams correctt access time < 200ns should be good enough!

![Annotated Timing diagram from HP13255](rom_timing.png)
