EESchema Schematic File Version 4
LIBS:DisplayControlPCA_60112-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS166 U510
U 1 1 5F452AE6
P 5600 3500
F 0 "U510" H 5300 4350 50  0000 L CNN
F 1 "74166" H 5650 4350 50  0000 L CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 5600 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS166" H 5600 3500 50  0001 C CNN
	1    5600 3500
	1    0    0    -1  
$EndComp
Text GLabel 5000 2800 0    50   Input ~ 0
*
Text GLabel 5000 3600 0    50   Input ~ 0
*
Wire Wire Line
	5000 2800 5100 2800
Wire Wire Line
	5000 3600 5100 3600
Wire Wire Line
	5100 2900 4250 2900
Wire Wire Line
	5100 3000 4250 3000
Wire Wire Line
	5100 3100 4250 3100
Wire Wire Line
	5100 3200 4250 3200
Wire Wire Line
	5100 3300 4250 3300
Wire Wire Line
	5100 3400 4250 3400
Wire Wire Line
	5100 3500 4250 3500
Wire Wire Line
	5100 3900 4250 3900
Wire Wire Line
	5100 3800 4250 3800
Wire Wire Line
	5100 4200 4250 4200
$Comp
L power:VCC #PWR0105
U 1 1 5F454465
P 5600 2500
F 0 "#PWR0105" H 5600 2350 50  0001 C CNN
F 1 "VCC" H 5617 2673 50  0000 C CNN
F 2 "" H 5600 2500 50  0001 C CNN
F 3 "" H 5600 2500 50  0001 C CNN
	1    5600 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5F454A11
P 5600 4500
F 0 "#PWR0106" H 5600 4250 50  0001 C CNN
F 1 "GND" H 5605 4327 50  0000 C CNN
F 2 "" H 5600 4500 50  0001 C CNN
F 3 "" H 5600 4500 50  0001 C CNN
	1    5600 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5F454EB8
P 5050 4050
F 0 "#PWR0107" H 5050 3800 50  0001 C CNN
F 1 "GND" H 4900 4000 50  0000 C CNN
F 2 "" H 5050 4050 50  0001 C CNN
F 3 "" H 5050 4050 50  0001 C CNN
	1    5050 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 4000 5050 4000
Wire Wire Line
	5050 4000 5050 4050
Wire Wire Line
	6100 2800 7050 2800
Text HLabel 4250 2900 0    50   Input ~ 0
A
Text HLabel 4250 3000 0    50   Input ~ 0
B
Text HLabel 4250 3100 0    50   Input ~ 0
C
Text HLabel 4250 3200 0    50   Input ~ 0
D
Text HLabel 4250 3300 0    50   Input ~ 0
E
Text HLabel 4250 3400 0    50   Input ~ 0
F
Text HLabel 4250 3500 0    50   Input ~ 0
G
Text HLabel 4250 3800 0    50   Input ~ 0
~LOAD~
Text HLabel 4250 3900 0    50   Input ~ 0
SHFT
Text HLabel 4250 4200 0    50   Input ~ 0
TPON
Text HLabel 7050 2800 2    50   Output ~ 0
~BITS~
$EndSCHEMATC
