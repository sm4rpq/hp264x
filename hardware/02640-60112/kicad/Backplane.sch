EESchema Schematic File Version 4
LIBS:DisplayControlPCA_60112-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x22_Top_Bottom J?
U 1 1 5F3FBF49
P 4700 3350
F 0 "J?" H 4750 4567 50  0000 C CNN
F 1 "Conn_02x22_Top_Bottom" H 4750 4476 50  0000 C CNN
F 2 "" H 4700 3350 50  0001 C CNN
F 3 "~" H 4700 3350 50  0001 C CNN
	1    4700 3350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
