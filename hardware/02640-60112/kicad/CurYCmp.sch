EESchema Schematic File Version 4
LIBS:DisplayControlPCA_60112-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 15 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L sm4rpq:DM8160N U41
U 1 1 5F59EAFB
P 5500 3400
F 0 "U41" H 6044 3446 50  0000 L CNN
F 1 "DM8160N" H 6044 3355 50  0000 L CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 5500 3400 50  0001 C CNN
F 3 "" H 5500 3400 50  0001 C CNN
	1    5500 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3500 4500 3500
Wire Wire Line
	5000 3600 4500 3600
Wire Wire Line
	5000 3700 4500 3700
Wire Wire Line
	5000 3800 4500 3800
Wire Wire Line
	5000 3900 4500 3900
$Comp
L power:GND #PWR0169
U 1 1 5F59F2B5
P 4900 4550
F 0 "#PWR0169" H 4900 4300 50  0001 C CNN
F 1 "GND" H 4905 4377 50  0000 C CNN
F 2 "" H 4900 4550 50  0001 C CNN
F 3 "" H 4900 4550 50  0001 C CNN
	1    4900 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2500 4900 2500
Wire Wire Line
	4900 2500 4900 3400
Wire Wire Line
	5000 4300 4900 4300
Connection ~ 4900 4300
Wire Wire Line
	4900 4300 4900 4550
Wire Wire Line
	5000 3400 4900 3400
Connection ~ 4900 3400
Wire Wire Line
	4900 3400 4900 4300
Wire Wire Line
	5000 2600 4500 2600
Wire Wire Line
	5000 2700 4500 2700
Wire Wire Line
	5000 2800 4500 2800
Wire Wire Line
	5000 2900 4500 2900
Wire Wire Line
	5000 3000 4500 3000
Wire Wire Line
	6000 2500 6550 2500
$Comp
L power:VCC #PWR0170
U 1 1 5F5A0316
P 5500 2200
F 0 "#PWR0170" H 5500 2050 50  0001 C CNN
F 1 "VCC" H 5517 2373 50  0000 C CNN
F 2 "" H 5500 2200 50  0001 C CNN
F 3 "" H 5500 2200 50  0001 C CNN
	1    5500 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0171
U 1 1 5F5A038E
P 5500 4600
F 0 "#PWR0171" H 5500 4350 50  0001 C CNN
F 1 "GND" H 5505 4427 50  0000 C CNN
F 2 "" H 5500 4600 50  0001 C CNN
F 3 "" H 5500 4600 50  0001 C CNN
	1    5500 4600
	1    0    0    -1  
$EndComp
Text HLabel 4500 3500 0    50   Input ~ 0
E
Text HLabel 4500 3600 0    50   Input ~ 0
D
Text HLabel 4500 3700 0    50   Input ~ 0
C
Text HLabel 4500 3800 0    50   Input ~ 0
B
Text HLabel 4500 3900 0    50   Input ~ 0
A
Text HLabel 4500 3000 0    50   Input ~ 0
CURY0
Text HLabel 4500 2900 0    50   Input ~ 0
CURY1
Text HLabel 4500 2800 0    50   Input ~ 0
CURY2
Text HLabel 4500 2700 0    50   Input ~ 0
CURY3
Text HLabel 4500 2600 0    50   Input ~ 0
CURY4
Text HLabel 6550 2500 2    50   Output ~ 0
CYEN
$EndSCHEMATC
