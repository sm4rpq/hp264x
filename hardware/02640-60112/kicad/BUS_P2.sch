EESchema Schematic File Version 4
LIBS:DisplayControlPCA_60112-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 17 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L sm4rpq:264X_DispCon P2
U 1 1 5F5E2223
P 5600 3400
F 0 "P2" H 5575 4325 50  0000 C CNN
F 1 "264X_DispCon" H 5575 4234 50  0000 C CNN
F 2 "sm4rpq:2640_Back" H 5650 2950 50  0001 C CNN
F 3 "" H 5650 2950 50  0001 C CNN
	1    5600 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0183
U 1 1 5F5E2F6D
P 6200 4100
F 0 "#PWR0183" H 6200 3850 50  0001 C CNN
F 1 "GND" V 6205 3972 50  0000 R CNN
F 2 "" H 6200 4100 50  0001 C CNN
F 3 "" H 6200 4100 50  0001 C CNN
	1    6200 4100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0184
U 1 1 5F5E4A19
P 6200 2700
F 0 "#PWR0184" H 6200 2450 50  0001 C CNN
F 1 "GND" V 6205 2572 50  0000 R CNN
F 2 "" H 6200 2700 50  0001 C CNN
F 3 "" H 6200 2700 50  0001 C CNN
	1    6200 2700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0185
U 1 1 5F5E4ED4
P 4950 2700
F 0 "#PWR0185" H 4950 2450 50  0001 C CNN
F 1 "GND" V 4955 2572 50  0000 R CNN
F 2 "" H 4950 2700 50  0001 C CNN
F 3 "" H 4950 2700 50  0001 C CNN
	1    4950 2700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0186
U 1 1 5F5E55B2
P 4950 4100
F 0 "#PWR0186" H 4950 3850 50  0001 C CNN
F 1 "GND" V 4955 3972 50  0000 R CNN
F 2 "" H 4950 4100 50  0001 C CNN
F 3 "" H 4950 4100 50  0001 C CNN
	1    4950 4100
	0    1    1    0   
$EndComp
NoConn ~ 6200 4000
Text GLabel 6200 2800 2    50   Input ~ 0
LC1
Text GLabel 6200 2900 2    50   Input ~ 0
LC3
Text GLabel 4950 2800 0    50   Input ~ 0
LC0
Text GLabel 4950 2900 0    50   Input ~ 0
LC2
Text GLabel 4950 3000 0    50   Input ~ 0
BIT0
Text GLabel 4950 3100 0    50   Input ~ 0
BIT2
Text GLabel 4950 3200 0    50   Input ~ 0
BIT4
Text GLabel 4950 3300 0    50   Input ~ 0
BIT6
Text GLabel 4950 3700 0    50   Output ~ 0
~DBIT1~
Text GLabel 4950 3800 0    50   Output ~ 0
~DBIT3~
Text GLabel 4950 3900 0    50   Output ~ 0
~DBIT5~
Text GLabel 4950 4000 0    50   Output ~ 0
~DBIT7~
Text GLabel 6200 3600 2    50   Output ~ 0
~DBIT0~
Text GLabel 6200 3700 2    50   Output ~ 0
~DBIT2~
Text GLabel 6200 3800 2    50   Output ~ 0
~DBIT4~
Text GLabel 6200 3900 2    50   Output ~ 0
~DBIT6~
Text GLabel 6200 3500 2    50   Input ~ 0
~E1~
Text GLabel 4950 3500 0    50   Input ~ 0
LCSEL
Text GLabel 4950 3600 0    50   Input ~ 0
UCSEL
Text GLabel 6200 3000 2    50   Input ~ 0
BIT1
Text GLabel 6200 3100 2    50   Input ~ 0
BIT3
Text GLabel 6200 3200 2    50   Input ~ 0
BIT5
Text GLabel 4950 3400 0    50   Input ~ 0
~BSS1~
Text GLabel 6200 3300 2    50   Input ~ 0
~BSS0~
$Comp
L power:GND #PWR0187
U 1 1 5F78AE86
P 6200 3400
F 0 "#PWR0187" H 6200 3150 50  0001 C CNN
F 1 "GND" V 6205 3272 50  0000 R CNN
F 2 "" H 6200 3400 50  0001 C CNN
F 3 "" H 6200 3400 50  0001 C CNN
	1    6200 3400
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
