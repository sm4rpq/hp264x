EESchema Schematic File Version 4
LIBS:DisplayControlPCA_60112-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS163 U27
U 1 1 5F45E80F
P 5100 2850
F 0 "U27" H 4900 3500 50  0000 C CNN
F 1 "74163" H 5300 3500 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 5100 2850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS163" H 5100 2850 50  0001 C CNN
	1    5100 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5F45F1DB
P 4450 3600
F 0 "#PWR0108" H 4450 3350 50  0001 C CNN
F 1 "GND" H 4455 3427 50  0000 C CNN
F 2 "" H 4450 3600 50  0001 C CNN
F 3 "" H 4450 3600 50  0001 C CNN
	1    4450 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 2350 4450 2350
Wire Wire Line
	4450 2350 4450 2450
Wire Wire Line
	4600 2450 4450 2450
Connection ~ 4450 2450
Wire Wire Line
	4450 2450 4450 2550
Wire Wire Line
	4600 2550 4450 2550
Connection ~ 4450 2550
Wire Wire Line
	4450 2550 4450 2650
Wire Wire Line
	4600 2650 4450 2650
Connection ~ 4450 2650
Wire Wire Line
	4450 2650 4450 3600
Wire Wire Line
	4600 2950 4500 2950
Wire Wire Line
	4600 3050 4500 3050
Wire Wire Line
	4500 3050 4500 2950
Connection ~ 4500 2950
Wire Wire Line
	4500 2950 4250 2950
Text GLabel 4250 2950 0    50   Input ~ 0
*
Wire Wire Line
	4600 2850 3550 2850
Wire Wire Line
	4600 3150 3550 3150
Wire Wire Line
	4600 3350 3550 3350
Wire Wire Line
	5600 2350 6400 2350
Wire Wire Line
	5600 2450 6400 2450
Wire Wire Line
	5600 2550 6400 2550
Wire Wire Line
	5600 2650 6400 2650
Text HLabel 3550 2850 0    50   Input ~ 0
TPON
Text HLabel 3550 3150 0    50   Input ~ 0
VRT_CLK
Text HLabel 3550 3350 0    50   Input ~ 0
~14~
Text HLabel 6400 2350 2    50   Output ~ 0
LC0
Text HLabel 6400 2450 2    50   Output ~ 0
LC1
Text HLabel 6400 2550 2    50   Output ~ 0
LC2
Text HLabel 6400 2650 2    50   Output ~ 0
LC3
$Comp
L power:VCC #PWR0109
U 1 1 5F461168
P 5100 2050
F 0 "#PWR0109" H 5100 1900 50  0001 C CNN
F 1 "VCC" H 5117 2223 50  0000 C CNN
F 2 "" H 5100 2050 50  0001 C CNN
F 3 "" H 5100 2050 50  0001 C CNN
	1    5100 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5F461606
P 5100 3650
F 0 "#PWR0110" H 5100 3400 50  0001 C CNN
F 1 "GND" H 5105 3477 50  0000 C CNN
F 2 "" H 5100 3650 50  0001 C CNN
F 3 "" H 5100 3650 50  0001 C CNN
	1    5100 3650
	1    0    0    -1  
$EndComp
NoConn ~ 5600 2850
$EndSCHEMATC
