EESchema Schematic File Version 4
LIBS:DisplayControlPCA_60112-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2050 2300 0    50   Input ~ 0
D0
Text HLabel 2050 3900 0    50   Input ~ 0
~D0~
Text HLabel 2050 2900 0    50   Input ~ 0
~D8~
Text HLabel 2050 5150 0    50   Input ~ 0
D8
Text HLabel 2050 4750 0    50   Input ~ 0
~DBIT0~
Text HLabel 9850 2900 2    50   Output ~ 0
SHFT
$Comp
L 74xx:74HC74 U31
U 1 1 5F40718E
P 5000 2300
F 0 "U31" H 4800 2550 50  0000 C CNN
F 1 "74S74" H 5200 2550 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 5000 2300 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 5000 2300 50  0001 C CNN
	1    5000 2300
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS112 U33
U 1 1 5F407A2E
P 5000 3450
F 0 "U33" H 4800 3700 50  0000 C CNN
F 1 "74S112" H 5200 3700 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 5000 3450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS112" H 5000 3450 50  0001 C CNN
	1    5000 3450
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS112 U24
U 1 1 5F40867C
P 5000 4650
F 0 "U24" H 4800 4900 50  0000 C CNN
F 1 "74LS112" H 5200 4900 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 5000 4650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS112" H 5000 4650 50  0001 C CNN
	1    5000 4650
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U32
U 1 1 5F408DB2
P 3450 2200
F 0 "U32" H 3450 2000 50  0000 C CNN
F 1 "74LS32" H 3450 2400 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 3450 2200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 3450 2200 50  0001 C CNN
	1    3450 2200
	1    0    0    1   
$EndComp
$Comp
L 74xx:74LS32 U32
U 2 1 5F40A3B0
P 3450 3000
F 0 "U32" H 3450 3200 50  0000 C CNN
F 1 "74LS32" H 3450 2800 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 3450 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 3450 3000 50  0001 C CNN
	2    3450 3000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U32
U 4 1 5F40AEA3
P 3450 4000
F 0 "U32" H 3450 3800 50  0000 C CNN
F 1 "74LS32" H 3450 4200 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 3450 4000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 3450 4000 50  0001 C CNN
	4    3450 4000
	1    0    0    1   
$EndComp
$Comp
L 74xx:74HC04 U22
U 6 1 5F40B43F
P 3450 4750
F 0 "U22" H 3500 4900 50  0000 C CNN
F 1 "74S04" H 3450 4550 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 3450 4750 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT04.pdf" H 3450 4750 50  0001 C CNN
	6    3450 4750
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS27 U34
U 3 1 5F40C681
P 6650 2400
F 0 "U34" H 6650 2200 50  0000 C CNN
F 1 "74LS27" H 6650 2600 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 6650 2400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS27" H 6650 2400 50  0001 C CNN
	3    6650 2400
	1    0    0    1   
$EndComp
$Comp
L 74xx:74LS00 U37
U 3 1 5F40D71A
P 7850 2500
F 0 "U37" H 7800 2300 50  0000 C CNN
F 1 "74S00" H 7850 2700 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 7850 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 7850 2500 50  0001 C CNN
	3    7850 2500
	1    0    0    1   
$EndComp
$Comp
L 74xx:74LS00 U37
U 2 1 5F40F63B
P 6650 3250
F 0 "U37" H 6600 3450 50  0000 C CNN
F 1 "74S00" H 6650 3050 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 6650 3250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 6650 3250 50  0001 C CNN
	2    6650 3250
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS10 U47
U 2 1 5F4101A3
P 6650 3850
F 0 "U47" H 6600 4050 50  0000 C CNN
F 1 "74S10" H 6650 3650 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 6650 3850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS10" H 6650 3850 50  0001 C CNN
	2    6650 3850
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS10 U47
U 1 2 5F4111E2
P 9000 2900
F 0 "U47" H 8950 3100 50  0000 C CNN
F 1 "74S10" H 9000 2700 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 9000 2900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS10" H 9000 2900 50  0001 C CNN
	1    9000 2900
	1    0    0    -1  
$EndComp
Text HLabel 2050 4100 0    50   Input ~ 0
~DSPY_CLK~
Wire Wire Line
	3150 2100 3050 2100
Wire Wire Line
	3050 2100 3050 1700
Wire Wire Line
	3050 1700 5650 1700
Wire Wire Line
	5650 1700 5650 2200
Wire Wire Line
	5650 2200 5300 2200
Wire Wire Line
	3750 2200 4700 2200
Wire Wire Line
	4700 3350 4600 3350
Wire Wire Line
	4600 3350 4600 3450
Wire Wire Line
	4600 3550 4700 3550
Wire Wire Line
	4700 3450 4600 3450
Connection ~ 4600 3450
Wire Wire Line
	4600 3450 4600 3550
Wire Wire Line
	3750 3000 5000 3000
Wire Wire Line
	5000 3000 5000 3150
Wire Wire Line
	5000 4000 5000 3750
Wire Wire Line
	4700 2300 4600 2300
Wire Wire Line
	4600 2300 4600 2600
Wire Wire Line
	4600 2600 3050 2600
Wire Wire Line
	3050 2600 3050 3100
Wire Wire Line
	3050 3100 3150 3100
Wire Wire Line
	3050 3100 3050 4100
Connection ~ 3050 3100
Wire Wire Line
	3750 4000 5000 4000
Wire Wire Line
	3050 4100 3150 4100
Wire Wire Line
	2050 4100 3050 4100
Connection ~ 3050 4100
Wire Wire Line
	4700 4750 3750 4750
Wire Wire Line
	4700 4550 3050 4550
Wire Wire Line
	3050 4550 3050 4750
Wire Wire Line
	3050 4750 3150 4750
Wire Wire Line
	3050 4750 2050 4750
Connection ~ 3050 4750
Wire Wire Line
	3150 2300 2050 2300
Wire Wire Line
	2050 2900 2850 2900
Wire Wire Line
	3150 3900 2050 3900
Wire Wire Line
	4700 4650 4600 4650
Wire Wire Line
	4600 4650 4600 5150
Wire Wire Line
	4600 5150 2050 5150
Wire Wire Line
	5300 3350 6350 3350
Wire Wire Line
	6350 2400 5300 2400
Wire Wire Line
	6350 2300 6250 2300
Wire Wire Line
	6250 2300 6250 3150
Wire Wire Line
	6250 3150 6350 3150
Wire Wire Line
	4600 2300 4600 1550
Wire Wire Line
	4600 1550 6250 1550
Wire Wire Line
	6250 1550 6250 2300
Connection ~ 4600 2300
Connection ~ 6250 2300
Wire Wire Line
	6950 2400 7550 2400
Wire Wire Line
	7550 2600 5000 2600
Wire Wire Line
	5000 2600 5000 2700
Wire Wire Line
	5000 2700 2850 2700
Wire Wire Line
	2850 2700 2850 2900
Connection ~ 5000 2600
Connection ~ 2850 2900
Wire Wire Line
	2850 2900 3150 2900
Wire Wire Line
	5300 4550 6100 4550
Wire Wire Line
	6100 4550 6100 3850
Wire Wire Line
	6100 2500 6350 2500
Wire Wire Line
	9850 2900 9550 2900
Wire Wire Line
	6950 3250 8500 3250
Wire Wire Line
	8500 3250 8500 2800
Wire Wire Line
	8500 2800 8700 2800
Wire Wire Line
	8700 2900 8350 2900
Wire Wire Line
	8350 2900 8350 2500
Wire Wire Line
	8350 2500 8150 2500
Wire Wire Line
	8600 3850 8600 3000
Wire Wire Line
	8600 3000 8700 3000
Wire Wire Line
	6950 3850 8600 3850
Wire Wire Line
	6350 3750 6250 3750
Wire Wire Line
	6250 3750 6250 3150
Connection ~ 6250 3150
Wire Wire Line
	6350 3850 6100 3850
Connection ~ 6100 3850
Wire Wire Line
	6100 3850 6100 2500
Wire Wire Line
	5300 3550 5600 3550
Wire Wire Line
	5600 3550 5600 3950
Wire Wire Line
	5600 3950 6350 3950
$Comp
L power:VCC #PWR0101
U 1 1 5F4479D2
P 5000 4350
F 0 "#PWR0101" H 5000 4200 50  0001 C CNN
F 1 "VCC" H 5017 4523 50  0000 C CNN
F 2 "" H 5000 4350 50  0001 C CNN
F 3 "" H 5000 4350 50  0001 C CNN
	1    5000 4350
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 5F4481B1
P 5000 4950
F 0 "#PWR0102" H 5000 4800 50  0001 C CNN
F 1 "VCC" H 5018 5123 50  0000 C CNN
F 2 "" H 5000 4950 50  0001 C CNN
F 3 "" H 5000 4950 50  0001 C CNN
	1    5000 4950
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 5F448DE1
P 5000 1250
F 0 "R5" H 5070 1296 50  0000 L CNN
F 1 "4.7k" H 5070 1205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4930 1250 50  0001 C CNN
F 3 "~" H 5000 1250 50  0001 C CNN
	1    5000 1250
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0103
U 1 1 5F44979C
P 5000 1100
F 0 "#PWR0103" H 5000 950 50  0001 C CNN
F 1 "VCC" H 5017 1273 50  0000 C CNN
F 2 "" H 5000 1100 50  0001 C CNN
F 3 "" H 5000 1100 50  0001 C CNN
	1    5000 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 1400 5000 2000
$Comp
L power:GND #PWR0178
U 1 1 5F7C7F40
P 4600 3700
F 0 "#PWR0178" H 4600 3450 50  0001 C CNN
F 1 "GND" H 4605 3527 50  0000 C CNN
F 2 "" H 4600 3700 50  0001 C CNN
F 3 "" H 4600 3700 50  0001 C CNN
	1    4600 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3550 4600 3700
Connection ~ 4600 3550
NoConn ~ 5300 4750
$Comp
L Connector:TestPoint TP?
U 1 1 5F49EB2C
P 9550 2700
AR Path="/5F4617C4/5F49EB2C" Ref="TP?"  Part="1" 
AR Path="/5F4069AD/5F49EB2C" Ref="TP3"  Part="1" 
F 0 "TP3" V 9650 2850 50  0000 C CNN
F 1 "SHFT" V 9450 2800 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 9750 2700 50  0001 C CNN
F 3 "~" H 9750 2700 50  0001 C CNN
	1    9550 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 2700 9550 2900
Connection ~ 9550 2900
Wire Wire Line
	9550 2900 9300 2900
$EndSCHEMATC
