EESchema Schematic File Version 4
LIBS:DisplayControlPCA_60112-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 13 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L sm4rpq:264X_Display_Bus_Conn_(P3) P3
U 1 1 5F494EB3
P 5550 3600
F 0 "P3" H 5525 4875 50  0000 C CNN
F 1 "264X_Display_Bus_Conn_(P3)" H 5525 4784 50  0000 C CNN
F 2 "sm4rpq:264X_BUS" H 5550 3600 50  0001 C CNN
F 3 "" H 5550 3600 50  0001 C CNN
	1    5550 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0163
U 1 1 5F4958EF
P 6150 2650
F 0 "#PWR0163" H 6150 2400 50  0001 C CNN
F 1 "GND" V 6155 2522 50  0000 R CNN
F 2 "" H 6150 2650 50  0001 C CNN
F 3 "" H 6150 2650 50  0001 C CNN
	1    6150 2650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0164
U 1 1 5F495E1E
P 6150 4550
F 0 "#PWR0164" H 6150 4300 50  0001 C CNN
F 1 "GND" V 6155 4422 50  0000 R CNN
F 2 "" H 6150 4550 50  0001 C CNN
F 3 "" H 6150 4550 50  0001 C CNN
	1    6150 4550
	0    -1   -1   0   
$EndComp
Text GLabel 6150 2550 2    50   Output ~ 0
DSPY_CLK
Text GLabel 6150 3750 2    50   Output ~ 0
~BIT0~
Text GLabel 6150 3850 2    50   Output ~ 0
~BIT1~
Text GLabel 6150 3950 2    50   Output ~ 0
~BIT2~
Text GLabel 6150 4050 2    50   Output ~ 0
~BIT3~
Text GLabel 6150 4150 2    50   Output ~ 0
~BIT4~
Text GLabel 6150 4250 2    50   Output ~ 0
~BIT5~
Text GLabel 6150 4350 2    50   Output ~ 0
~BIT6~
NoConn ~ 4900 2850
NoConn ~ 4900 3350
NoConn ~ 4900 3650
NoConn ~ 4900 3750
NoConn ~ 4900 3850
NoConn ~ 4900 3950
NoConn ~ 4900 4250
NoConn ~ 4900 4450
NoConn ~ 4900 4550
NoConn ~ 4900 4650
NoConn ~ 6150 3350
NoConn ~ 6150 3450
Text GLabel 6150 3150 2    50   Output ~ 0
VRT_CLK
Text GLabel 4900 3250 0    50   Input ~ 0
EVEN
Text GLabel 4900 3150 0    50   Input ~ 0
VDR
Text GLabel 4900 3050 0    50   Input ~ 0
CLEN
Text GLabel 6150 3250 2    50   Input ~ 0
VBLNK
Text GLabel 4900 2550 0    50   Input ~ 0
~D0~
Text GLabel 4900 2650 0    50   Input ~ 0
~D6~
Text GLabel 4900 2950 0    50   Input ~ 0
~D1~
Text GLabel 4900 3450 0    50   Input ~ 0
GVS
Text GLabel 6150 2850 2    50   Input ~ 0
~D8~
Text GLabel 6150 2750 2    50   Input ~ 0
~D2~
Text GLabel 6150 2950 2    50   Input ~ 0
~14~
Text GLabel 6150 3050 2    50   Input ~ 0
L11
Text GLabel 6150 3550 2    50   Input ~ 0
INTSET
Text GLabel 4900 3550 0    50   Input ~ 0
CYEN
Text GLabel 6150 4650 2    50   Input ~ 0
~BITS~
Text GLabel 4900 4350 0    50   Input ~ 0
~BLNK~
Text GLabel 6150 4450 2    50   Input ~ 0
~CYS~
Text GLabel 6150 3650 2    50   Input ~ 0
~CXS~
Text GLabel 4900 2750 0    50   Output ~ 0
~103~
Text GLabel 4900 4050 0    50   Output ~ 0
~BSS0~
Text GLabel 4900 4150 0    50   Output ~ 0
~BSS1~
$EndSCHEMATC
