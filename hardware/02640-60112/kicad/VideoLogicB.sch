EESchema Schematic File Version 4
LIBS:DisplayControlPCA_60112-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS11 U43
U 2 1 5F41B486
P 2700 1450
F 0 "U43" H 2700 1650 50  0000 C CNN
F 1 "74LS11" H 2700 1200 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 2700 1450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS11" H 2700 1450 50  0001 C CNN
	2    2700 1450
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U23
U 3 1 5F41CF8F
P 2700 2550
F 0 "U23" H 2650 2750 50  0000 C CNN
F 1 "74LS00" H 2700 2350 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 2700 2550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 2700 2550 50  0001 C CNN
	3    2700 2550
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U23
U 4 1 5F41ED93
P 2700 4750
F 0 "U23" H 2700 4433 50  0000 C CNN
F 1 "74LS00" H 2700 4524 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 2700 4750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 2700 4750 50  0001 C CNN
	4    2700 4750
	1    0    0    1   
$EndComp
$Comp
L 74xx:74LS112 U33
U 2 1 5F420945
P 4500 3650
F 0 "U33" H 4300 3900 50  0000 C CNN
F 1 "74S112" H 4700 3900 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 4500 3650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS112" H 4500 3650 50  0001 C CNN
	2    4500 3650
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS27 U34
U 2 1 5F421386
P 4000 1900
F 0 "U34" H 3950 2100 50  0000 C CNN
F 1 "74LS27" H 4000 1700 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 4000 1900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS27" H 4000 1900 50  0001 C CNN
	2    4000 1900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS11 U43
U 3 1 5F423BDB
P 5250 1450
F 0 "U43" H 5200 1650 50  0000 C CNN
F 1 "74LS11" H 5250 1250 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 5250 1450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS11" H 5250 1450 50  0001 C CNN
	3    5250 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 4750 4500 4750
Wire Wire Line
	4500 4750 4500 3950
Wire Wire Line
	2400 2650 2250 2650
Wire Wire Line
	2250 2650 2250 2450
Wire Wire Line
	2250 2450 2400 2450
Wire Wire Line
	3000 1450 3200 1450
Wire Wire Line
	3200 1450 3200 3650
Wire Wire Line
	3200 3650 4200 3650
Connection ~ 3200 1450
Wire Wire Line
	3200 1450 4950 1450
Wire Wire Line
	3000 3550 4200 3550
NoConn ~ 3000 3750
NoConn ~ 4800 3550
Wire Wire Line
	4300 1900 4650 1900
Wire Wire Line
	4650 1900 4650 1550
Wire Wire Line
	4650 1550 4950 1550
Wire Wire Line
	4950 1350 3400 1350
Wire Wire Line
	3400 1350 3400 1650
Wire Wire Line
	3400 1650 2250 1650
Wire Wire Line
	2250 1650 2250 2450
Connection ~ 2250 2450
Wire Wire Line
	2400 1350 1250 1350
Wire Wire Line
	2400 1450 1850 1450
Wire Wire Line
	2400 1550 1950 1550
Wire Wire Line
	2400 3550 1950 3550
Wire Wire Line
	1950 3550 1950 1550
Connection ~ 1950 1550
Wire Wire Line
	1950 1550 1250 1550
Wire Wire Line
	2400 3650 1850 3650
Wire Wire Line
	1850 3650 1850 1450
Connection ~ 1850 1450
Wire Wire Line
	1850 1450 1250 1450
Wire Wire Line
	2700 3950 2700 4100
Wire Wire Line
	2700 4100 1250 4100
Wire Wire Line
	5550 1450 7400 1450
Wire Wire Line
	3000 2550 7400 2550
Wire Wire Line
	3700 1800 1250 1800
Text HLabel 1250 1350 0    50   Input ~ 0
B
Text HLabel 1250 1450 0    50   Input ~ 0
C
Text HLabel 1250 1550 0    50   Input ~ 0
E
Wire Wire Line
	3700 2000 1250 2000
Wire Wire Line
	2400 4850 1850 4850
Wire Wire Line
	2400 4650 1250 4650
Wire Wire Line
	3700 1900 3400 1900
Wire Wire Line
	3400 1900 3400 5050
Wire Wire Line
	3400 5050 1850 5050
Wire Wire Line
	1850 5050 1850 4850
Connection ~ 1850 4850
Wire Wire Line
	1850 4850 1250 4850
Wire Wire Line
	4800 3750 7400 3750
Wire Wire Line
	4650 1900 7400 1900
Connection ~ 4650 1900
$Comp
L power:GND #PWR0118
U 1 1 5F43A0DC
P 4100 3850
F 0 "#PWR0118" H 4100 3600 50  0001 C CNN
F 1 "GND" H 4105 3677 50  0000 C CNN
F 2 "" H 4100 3850 50  0001 C CNN
F 3 "" H 4100 3850 50  0001 C CNN
	1    4100 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3750 4100 3750
Wire Wire Line
	4100 3750 4100 3850
Wire Wire Line
	2700 3350 2700 3250
Text GLabel 2700 3250 1    50   Input ~ 0
*
Wire Wire Line
	2250 1650 1250 1650
Connection ~ 2250 1650
Text HLabel 1250 1650 0    50   Input ~ 0
A
Text HLabel 1250 4650 0    50   Input ~ 0
14
Text HLabel 7400 1450 2    50   Output ~ 0
GVS
Text HLabel 7400 1900 2    50   Output ~ 0
INTSET
Text HLabel 7400 2550 2    50   Output ~ 0
EVEN
Text HLabel 7400 3750 2    50   Output ~ 0
VDR
$Comp
L 74xx:74HC74 U31
U 2 1 5F4412F9
P 2700 3650
F 0 "U31" H 2550 3900 50  0000 C CNN
F 1 "74S74" H 2850 3900 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 2700 3650 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 2700 3650 50  0001 C CNN
	2    2700 3650
	1    0    0    -1  
$EndComp
Text HLabel 1250 2000 0    50   Input ~ 0
~103~
Text HLabel 1250 4100 0    50   Input ~ 0
TPON
Text HLabel 1250 4850 0    50   Input ~ 0
VBLNK
Text HLabel 1250 1800 0    50   Input ~ 0
U46_8
Wire Wire Line
	2700 4100 3750 4100
Wire Wire Line
	3750 4100 3750 3200
Wire Wire Line
	3750 3200 4500 3200
Wire Wire Line
	4500 3200 4500 3350
Connection ~ 2700 4100
$EndSCHEMATC
