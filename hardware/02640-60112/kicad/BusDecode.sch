EESchema Schematic File Version 4
LIBS:DisplayControlPCA_60112-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS27 U58
U 1 2 5F3F16A8
P 4950 4000
F 0 "U58" H 4900 4200 50  0000 C CNN
F 1 "74LS27" H 4950 3800 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 4950 4000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS27" H 4950 4000 50  0001 C CNN
	1    4950 4000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS27 U58
U 2 2 5F3F3DFB
P 4950 2350
F 0 "U58" H 4900 2150 50  0000 C CNN
F 1 "74LS27" H 4950 2550 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 4950 2350 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS27" H 4950 2350 50  0001 C CNN
	2    4950 2350
	1    0    0    1   
$EndComp
$Comp
L 74xx:74LS27 U58
U 3 2 5F3F4DBC
P 4950 3150
F 0 "U58" H 4900 3350 50  0000 C CNN
F 1 "74LS27" H 4950 2950 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 4950 3150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS27" H 4950 3150 50  0001 C CNN
	3    4950 3150
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS20 U59
U 1 1 5F3F56FF
P 6450 3850
F 0 "U59" H 6400 4100 50  0000 C CNN
F 1 "74LS20" H 6450 3600 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 6450 3850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS20" H 6450 3850 50  0001 C CNN
	1    6450 3850
	1    0    0    -1  
$EndComp
Text HLabel 3950 2450 0    50   Input ~ 0
~ADDR5~
Text HLabel 3950 3050 0    50   Input ~ 0
~WRITE~
Text HLabel 3950 3150 0    50   Input ~ 0
~REQ~
Text HLabel 3950 3250 0    50   Input ~ 0
~I_O~
Text HLabel 3950 3700 0    50   Input ~ 0
~ADDR11~
Text HLabel 3950 3900 0    50   Input ~ 0
~ADDR9~
Text HLabel 3950 4000 0    50   Input ~ 0
~ADDR4~
Text HLabel 3950 4100 0    50   Input ~ 0
~ADDR10~
Wire Wire Line
	4650 2350 4550 2350
Wire Wire Line
	4550 2350 4550 2250
Wire Wire Line
	4550 2250 4650 2250
$Comp
L 74xx:74LS20 U59
U 2 1 5F3FDEC6
P 6450 2400
F 0 "U59" H 6400 2150 50  0000 C CNN
F 1 "74LS20" H 6450 2650 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 6450 2400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS20" H 6450 2400 50  0001 C CNN
	2    6450 2400
	1    0    0    1   
$EndComp
Wire Wire Line
	6150 2350 5250 2350
Wire Wire Line
	5250 4000 6050 4000
Wire Wire Line
	6050 4000 6050 2550
Wire Wire Line
	6050 2550 6150 2550
Connection ~ 6050 4000
Wire Wire Line
	6050 4000 6150 4000
Wire Wire Line
	6150 3900 5950 3900
Wire Wire Line
	5950 3900 5950 3150
Wire Wire Line
	5950 2450 6150 2450
Wire Wire Line
	6150 3700 5850 3700
Wire Wire Line
	5850 3700 5850 2250
Wire Wire Line
	5850 2250 6150 2250
Wire Wire Line
	6150 3800 5750 3800
Wire Wire Line
	5750 3800 5750 2600
Wire Wire Line
	5750 2600 4550 2600
Wire Wire Line
	4550 2600 4550 2450
Wire Wire Line
	4550 2450 3950 2450
Wire Wire Line
	4650 2450 4550 2450
Connection ~ 4550 2450
Wire Wire Line
	5250 3150 5950 3150
Connection ~ 5950 3150
Wire Wire Line
	5950 3150 5950 2450
Wire Wire Line
	4650 4100 3950 4100
Wire Wire Line
	4650 4000 3950 4000
Wire Wire Line
	4650 3900 3950 3900
$Comp
L power:GND #PWR0112
U 1 1 5F404A66
P 4450 2250
F 0 "#PWR0112" H 4450 2000 50  0001 C CNN
F 1 "GND" V 4455 2122 50  0000 R CNN
F 2 "" H 4450 2250 50  0001 C CNN
F 3 "" H 4450 2250 50  0001 C CNN
	1    4450 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	4450 2250 4550 2250
Connection ~ 4550 2250
Wire Wire Line
	3950 3050 4650 3050
Wire Wire Line
	3950 3150 4650 3150
Wire Wire Line
	3950 3250 4650 3250
Text HLabel 7400 3850 2    50   Output ~ 0
~CXS~
Text HLabel 7400 2400 2    50   Output ~ 0
~CYS~
Wire Wire Line
	6750 2400 7400 2400
Wire Wire Line
	6750 3850 7400 3850
Wire Wire Line
	5850 3700 3950 3700
Connection ~ 5850 3700
$EndSCHEMATC
