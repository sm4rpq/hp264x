EESchema Schematic File Version 4
LIBS:DisplayControlPCA_60112-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS175 U?
U 1 1 5F3E5FCF
P 4650 2700
AR Path="/5F3E5FCF" Ref="U?"  Part="1" 
AR Path="/5F3DCEA8/5F3E5FCF" Ref="U11"  Part="1" 
F 0 "U11" H 4400 3250 50  0000 C CNN
F 1 "74S175" H 4850 3250 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 4650 2700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS175" H 4650 2700 50  0001 C CNN
	1    4650 2700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS175 U?
U 1 1 5F3E5FD5
P 4650 5150
AR Path="/5F3E5FD5" Ref="U?"  Part="1" 
AR Path="/5F3DCEA8/5F3E5FD5" Ref="U12"  Part="1" 
F 0 "U12" H 4400 5700 50  0000 C CNN
F 1 "74S175" H 4850 5700 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 4650 5150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS175" H 4650 5150 50  0001 C CNN
	1    4650 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3100 3050 3100
Wire Wire Line
	3050 3100 3050 5550
Wire Wire Line
	3050 5550 4150 5550
Wire Wire Line
	3050 3100 2400 3100
Connection ~ 3050 3100
Wire Wire Line
	4150 3200 3150 3200
Wire Wire Line
	3150 3200 3150 5650
Wire Wire Line
	3150 5650 4150 5650
Wire Wire Line
	3150 5650 2400 5650
Connection ~ 3150 5650
Wire Wire Line
	5150 2300 5250 2300
Wire Wire Line
	5250 2300 5250 1750
Wire Wire Line
	5250 1750 4050 1750
Wire Wire Line
	4050 1750 4050 2500
Wire Wire Line
	4050 2500 4150 2500
Wire Wire Line
	5150 2500 5350 2500
Wire Wire Line
	5350 2500 5350 1650
Wire Wire Line
	5350 1650 3950 1650
Wire Wire Line
	3950 1650 3950 2700
Wire Wire Line
	3950 2700 4150 2700
Wire Wire Line
	5150 2700 5450 2700
Wire Wire Line
	5450 2700 5450 1550
Wire Wire Line
	5450 1550 3850 1550
Wire Wire Line
	3850 1550 3850 2900
Wire Wire Line
	3850 2900 4150 2900
Wire Wire Line
	5150 2900 5250 2900
Wire Wire Line
	5250 4200 4050 4200
Wire Wire Line
	4050 4200 4050 4750
Wire Wire Line
	4050 4750 4150 4750
Wire Wire Line
	5150 4750 5350 4750
Wire Wire Line
	5350 4750 5350 4300
Wire Wire Line
	5350 4100 3950 4100
Wire Wire Line
	3950 4100 3950 4950
Wire Wire Line
	3950 4950 4150 4950
Wire Wire Line
	5150 4950 5450 4950
Wire Wire Line
	5450 4000 3850 4000
Wire Wire Line
	3850 4000 3850 5150
Wire Wire Line
	3850 5150 4150 5150
Wire Wire Line
	5150 5150 5550 5150
Wire Wire Line
	5550 3900 3750 3900
Wire Wire Line
	3750 3900 3750 5350
Wire Wire Line
	3750 5350 4150 5350
$Comp
L 74xx:74LS30 U?
U 1 1 5F3E6006
P 6850 4200
AR Path="/5F3E6006" Ref="U?"  Part="1" 
AR Path="/5F3DCEA8/5F3E6006" Ref="U21"  Part="1" 
F 0 "U21" H 6800 4000 50  0000 C CNN
F 1 "74H30" H 6850 4400 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 6850 4200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS30" H 6850 4200 50  0001 C CNN
	1    6850 4200
	1    0    0    1   
$EndComp
Wire Wire Line
	5550 3900 5550 4500
Wire Wire Line
	5450 4000 5450 4400
Wire Wire Line
	5250 2900 5250 4200
Wire Wire Line
	6550 4200 5250 4200
Connection ~ 5250 4200
Wire Wire Line
	6550 4300 5350 4300
Connection ~ 5350 4300
Wire Wire Line
	5350 4300 5350 4100
Wire Wire Line
	6550 4400 5450 4400
Connection ~ 5450 4400
Wire Wire Line
	5450 4400 5450 4950
Wire Wire Line
	6550 4500 5550 4500
Connection ~ 5550 4500
Wire Wire Line
	5550 4500 5550 5150
Wire Wire Line
	6550 4100 5650 4100
Wire Wire Line
	5650 4100 5650 2500
Wire Wire Line
	5650 2500 5350 2500
Connection ~ 5350 2500
Wire Wire Line
	6550 4000 5750 4000
Wire Wire Line
	5750 4000 5750 2700
Wire Wire Line
	5750 2700 5450 2700
Connection ~ 5450 2700
Wire Wire Line
	6550 3900 5850 3900
Wire Wire Line
	5850 3900 5850 2300
Wire Wire Line
	5850 2300 5250 2300
Connection ~ 5250 2300
Wire Wire Line
	6550 3800 5950 3800
Wire Wire Line
	5950 3800 5950 5350
Wire Wire Line
	5950 5350 5150 5350
Wire Wire Line
	4150 2300 3750 2300
Wire Wire Line
	3750 2300 3750 3600
Wire Wire Line
	3750 3600 7250 3600
Wire Wire Line
	7250 3600 7250 4200
Wire Wire Line
	7250 4200 7150 4200
Wire Wire Line
	5150 4850 6800 4850
Text Label 7700 4850 0    50   ~ 0
D0
Connection ~ 5650 2500
Connection ~ 5250 2900
Text Label 7700 2900 0    50   ~ 0
~D8~
Text Label 7700 3000 0    50   ~ 0
D8
Connection ~ 5450 4950
Text Label 7700 4950 0    50   ~ 0
~D1~
Text Label 7700 5150 0    50   ~ 0
~D2~
Connection ~ 5550 5150
Text HLabel 2400 3100 0    50   Input ~ 0
~DSPY_CLK~
$Comp
L 74xx:74HC04 U22
U 1 1 5F3E8459
P 7250 4650
F 0 "U22" H 7300 4800 50  0000 C CNN
F 1 "74S04" H 7350 4500 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 7250 4650 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT04.pdf" H 7250 4650 50  0001 C CNN
	1    7250 4650
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U22
U 2 1 5F3E9275
P 7250 3250
F 0 "U22" H 7400 3350 50  0000 C CNN
F 1 "74S04" H 7400 3150 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 7250 3250 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT04.pdf" H 7250 3250 50  0001 C CNN
	2    7250 3250
	1    0    0    -1  
$EndComp
Text HLabel 9150 2500 2    50   Output ~ 0
~D6_E~
Wire Wire Line
	5650 2500 9150 2500
Wire Wire Line
	6950 3250 6800 3250
Wire Wire Line
	6800 3250 6800 3000
Connection ~ 6800 3000
Wire Wire Line
	6800 3000 5150 3000
Wire Wire Line
	7550 3250 9150 3250
Text HLabel 9150 3250 2    50   Output ~ 0
~D8_E~
Wire Wire Line
	6950 4650 6800 4650
Wire Wire Line
	6800 4650 6800 4850
Connection ~ 6800 4850
Wire Wire Line
	7550 4650 9150 4650
Wire Wire Line
	5450 4950 9150 4950
Wire Wire Line
	5550 5150 9150 5150
Text HLabel 9150 5150 2    50   Output ~ 0
~D2_E~
Text HLabel 9150 4950 2    50   Output ~ 0
~D1_E~
Text HLabel 9150 4650 2    50   Output ~ 0
~D0_E~
Text HLabel 9150 4850 2    50   Output ~ 0
D0_I
Text HLabel 9150 3000 2    50   Output ~ 0
D8_I
Text HLabel 9150 2900 2    50   Output ~ 0
~D8_I~
Wire Wire Line
	5250 2900 9150 2900
Wire Wire Line
	6800 3000 9150 3000
Wire Wire Line
	6800 4850 9150 4850
Text HLabel 2400 5650 0    50   Input ~ 0
TPON
Wire Wire Line
	5350 4750 6500 4750
Wire Wire Line
	6500 4750 6500 5800
Wire Wire Line
	6500 5800 9150 5800
Connection ~ 5350 4750
Text HLabel 9150 5800 2    50   Output ~ 0
~D0_I~
$Comp
L power:VCC #PWR0123
U 1 1 5F410D59
P 4650 2000
F 0 "#PWR0123" H 4650 1850 50  0001 C CNN
F 1 "VCC" H 4667 2173 50  0000 C CNN
F 2 "" H 4650 2000 50  0001 C CNN
F 3 "" H 4650 2000 50  0001 C CNN
	1    4650 2000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0124
U 1 1 5F410F33
P 4650 4450
F 0 "#PWR0124" H 4650 4300 50  0001 C CNN
F 1 "VCC" H 4667 4623 50  0000 C CNN
F 2 "" H 4650 4450 50  0001 C CNN
F 3 "" H 4650 4450 50  0001 C CNN
	1    4650 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0125
U 1 1 5F411538
P 4650 3500
F 0 "#PWR0125" H 4650 3250 50  0001 C CNN
F 1 "GND" H 4655 3327 50  0000 C CNN
F 2 "" H 4650 3500 50  0001 C CNN
F 3 "" H 4650 3500 50  0001 C CNN
	1    4650 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0126
U 1 1 5F4117C3
P 4650 5950
F 0 "#PWR0126" H 4650 5700 50  0001 C CNN
F 1 "GND" H 4655 5777 50  0000 C CNN
F 2 "" H 4650 5950 50  0001 C CNN
F 3 "" H 4650 5950 50  0001 C CNN
	1    4650 5950
	1    0    0    -1  
$EndComp
NoConn ~ 5150 2400
NoConn ~ 5150 2600
NoConn ~ 5150 2800
NoConn ~ 5150 5050
NoConn ~ 5150 5250
NoConn ~ 5150 5450
$EndSCHEMATC
