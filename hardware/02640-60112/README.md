The 264x series terminals have a very special way of rendering fonts. They use a 9x15 matrix somewhat
extended horizontally by shifting pixels. I wanted to understand how this is done so I started to
enter the quite small and hard to read schematic into kicad. Just could not stop and ended up with what
I believe is a complete schematic for the part called the display control.

![Display control board schematic](60112_A.png)

Then it was just a matter of creating a few additional PCB footprints for the connectors and layout the
PCB. However drawing all the traces was beyond me so I let the autorouter (freeroute) do it.

I have no intention of sending this to a manufacturer. Neither can I guarantee that I have captured it
complete and correct. Use wisely.

![Kicad 3D rendring of board](02640-60112-kicad.jpg)
![Photo borrowed from the hp-museum](02640-60112-hpmuseum.jpg)
