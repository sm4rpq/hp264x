unit Options;

interface

type
  TOption = (LAdr,LChar,LByte);
  TOptions = class(TObject)
  private
    FInFile: string;
    FOutFile: string;
    FAddr: boolean;
    constructor Create(); virtual;
    destructor Destroy();
  public
    property InputFile : string read FInFile;
    property OutputFile : string read FOutFile;
    property Addr : boolean read FAddr;
  end;

var
  gOptions : TOptions;

implementation

uses
  SysUtils;

{ TOptions }

constructor TOptions.Create;
var
  i : integer;
  Param : string;
  Options : set of TOption;

begin
  Options:=[];
  for i:=1 to ParamCount do begin
    Param:=ParamStr(i);
    if Copy(Param,1,1)='-' then begin
      case Param[2] of
        'a' : include(Options,LAdr);   // -a[ddr]
        'c' : include(Options,LChar);  // -c[haracter]
        'o' : include(Options,LByte);  // -b[yte]
      end;

    end else begin
      if not(FileExists(Param)) then
        //WriteLn('Error: File "'+Param+'"does not exist')
      else
        FInFile:=Param;
    end;
  end;

  FAddr:=(LAdr in Options) or (LChar in Options) or (LByte in Options);
  FOutFile:=ChangeFileExt(FInFile,'.asm');
end;

destructor TOptions.Destroy;
begin

end;

initialization
  gOptions:=TOptions.Create;

finalization
  gOptions.Free;

end.
