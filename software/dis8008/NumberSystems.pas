unit NumberSystems;

interface

function Byte2Oct(B : Byte) : string;
function Word2Oct(W : integer) : string;
function Word2Oct2(W : integer) : string;
function Oct2Int(Str : string) : integer;

implementation

uses
  SysUtils;

function Byte2Oct(B : Byte) : string;

var
  i : integer;

begin
  Result:='';
  for i:=0 to 2 do begin
    Result:=IntToStr(B mod 8)+Result;
    B:=B div 8;
  end;
end;

function Word2Oct(W : Integer) : string;
begin
  Result:='';
  Result:=Byte2Oct(W div 256)+Byte2Oct(W mod 256);
//  repeat
//    Result:=IntToStr(W mod 8)+Result;
//    W:=W div 8;
//  until W=0;
end;

function Word2Oct2(W : Integer) : string;
begin
  Result:='';
  repeat
    Result:=IntToStr(W mod 8)+Result;
    W:=W div 8;
  until W=0;
end;

function Oct2Int(Str : string) : integer;

var
  i : integer;

begin
  Result:=0;

  if (length(str)=6) then begin
    Result:=Oct2Int(Copy(Str,1,3))*256+Oct2Int(Copy(Str,4,3));
  end else begin
    for i:=1 to length(Str) do
      Result:=Result*8+Ord(Str[i])-48;
  end;
end;


end.
