unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Memory, ControlFile, Symtable;

type
  TOpcode = record
    Opcode : integer;
    Mnemonic : string;
    Args : string;
    Bytes : integer;
    Break : bool;
  end;
  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Mem : TMemory;
    Symtab : TSymbolTable;
    CtlFile : TControlFile;
    Opcodes : array[0..255] of TOpcode;
    Outfile : TextFile;

    procedure LoadOpcodes;
    procedure DisAsmPass1;
    procedure AssLine(Lbl,Mnemonic,Args,Bytes,Comment : string; Break : boolean);
    procedure DisAsmPass2;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  NumberSystems,Options;

{$R *.dfm}

procedure TForm1.DisAsmPass1;

var
  PC : integer;
  Break : boolean;
  B1,B2,B3 : byte;
  Adr : integer;
  Args : string;
  FillIn : integer;
  RefEquate : string;
  RefEquateBin : integer;

begin
  while CtlFile.EntryPoints.Count>0 do begin
    PC:=integer(CtlFile.Entrypoints[0]);
    CtlFile.EntryPoints.Delete(0);
    Break:=false;

    while not(Break) do begin
      B1:=Mem.Rom[PC].Value;
      B2:=Mem.Rom[PC+1].Value;
      B3:=Mem.Rom[PC+2].Value;

      Mem.Rom[PC].Mnemonic:=Opcodes[B1].Mnemonic;
      Mem.Rom[PC].ValueKind:=EMnemonic;
      Args:=Opcodes[B1].Args;
      Mem.Rom[PC].Break:=Opcodes[B1].Break;

      if pos('$a',Args)>0 then begin //2 bytes follow LSB/MSB for argument
        //Address in B3:B2
        Adr:=B3*256+B2;

        //Update referred address
        if Mem.Rom[Adr].ValueKind<>EMnemonic then
          CtlFile.EntryPoints.Add(Pointer(Adr));
        if Mem.Rom[Adr].Lbl='' then
          Mem.Rom[Adr].Lbl:='L_'+Word2Oct(Adr);

        //Delete placeholder from argument string
        FillIn:=Pos('$a',Args);
        Delete(Args,FillIn,2);

        //Insert referred addresses label
        Insert(Mem.Rom[Adr].Lbl,Args,FillIn);

        //Final Result (excluding any label for current address)
        Mem.Rom[PC].Args:=Args;

        Mem.Rom[PC+1].ValueKind:=EAdditional;
        Mem.Rom[PC+2].ValueKind:=EAdditional;

        if B1=70 then begin //Call
          if (Mem.Rom[PC].Comment='') and (Mem.Rom[Adr].InvokeComment<>'') then
            Mem.Rom[PC].Comment:=Mem.Rom[Adr].InvokeComment;
        end;

      end else if pos('$b',Args)>0 then begin //1 byte follows for argument
        //Immediate in B2

        FillIn:=Pos('$b',Args);
        Delete(Args,FillIn,2);

        RefEquate:=Mem.Rom[PC].Ref;
        if RefEquate='' then
          Insert(Byte2Oct(B2),Args,FillIn)
        else begin
          RefEquateBin:=Symtab.getAddress(RefEquate);
          if (RefEquateBin div 256)=B2 then
            Insert(RefEquate+' >> 10Q',Args,FillIn)
          else if (RefEquateBin mod 256)=B2 then
            Insert(RefEquate+' & 377Q',Args,FillIn)
          else
            Insert(Byte2Oct(B2),Args,FillIn);
        end;

        Mem.Rom[PC].Args:=Args;

        Mem.Rom[PC+1].ValueKind:=EAdditional;
      end else
        Mem.Rom[PC].Args:=Opcodes[B1].Args;

      if (Mem.Rom[PC].Value=$36) and (Mem.Rom[PC].Comment='') then begin //MVI L,$b
        if Mem.Rom[PC].Comment='' then begin
          RefEquate:=Symtab.getSymbol(B2);
          if RefEquate<>'' then
            Mem.Rom[PC].Comment:='    <----'+RefEquate;
        end;
      end;

      Break:=Opcodes[B1].Break;
      PC:=PC+Opcodes[B1].Bytes;
    end;
  end; //While entrypoints...
end;

procedure TForm1.DisAsmPass2;

var
  i,j : integer;
  Lbl   : string;
  Mnemonic : string;
  Args : string;
  Bytes : string;
  Comment : string;
  ByteCount : integer;
  Ref : integer;
  Add : integer;
  Compress : boolean;
  CompressCount : integer;
  CompressStart : integer;

begin
  AssignFile(Outfile,gOptions.OutputFile);
  Rewrite(Outfile);

  WriteLn(OutFile,'; This is an auto-generated file. Do not edit!');
  WriteLn(OutFile,'            CPU    8008NEW');
  WriteLn(OutFile,'            RADIX  8');
  WriteLn(OutFile,'            PAGE   101Q');
  WriteLn(OutFile,'');
  WriteLn(OutFile,'            ORG    000000');
  WriteLn(OutFile,'');
  Compress:=False;
  CompressCount:=0;
  CompressStart:=0;
  for i:=0 to 8191 do begin
    if Mem.Rom[i].PageBreak then begin
      if not(Mem.Rom[i-1].Break) then begin
        WriteLn(OutFile);
      end;
      WriteLn(OutFile,Format('%-11s %-6s',['','NEWPAGE']));
    end;
    if assigned(Mem.Rom[i].PreComments) then begin
      for j:=0 to Mem.Rom[i].PreComments.Count-1 do begin
        if length(Mem.Rom[i].PreComments[j])>0 then
          WriteLn(OutFile,'; '+Mem.Rom[i].PreComments[j])
        else
          WriteLn(OutFile);
      end;
    end;
    if Mem.Rom[i].ValueKind<>EAdditional then begin //Byte not used from another byte
      if length(Mem.Rom[i].Lbl)>10 then begin//Label too long to fit in-line
        WriteLn(OutFile,Mem.Rom[i].Lbl+':');
        Lbl:='';
        Compress:=False;
      end else if Mem.Rom[i].Lbl<>'' then begin//Label defined
        Lbl:=Mem.Rom[i].Lbl;
        Compress:=False;
      end else //Label not defined
        Lbl:='';

      if Mem.Rom[i].ValueKind=EMnemonic then begin //If its an instruction
        Mnemonic:=Mem.Rom[i].Mnemonic;
        Args:=Mem.Rom[i].Args;
        Bytes:='';
        ByteCount:=Opcodes[Mem.Rom[i].Value].Bytes;
        Compress:=False;
      end else if Mem.Rom[i].ValueKind=EAdr then begin //Its an address reference
        Mnemonic:='DW';
        Ref:=Mem.Rom[i].Value+Mem.Rom[i+1].Value*256;
        Add:=Ref and $C000;
        Ref:=Ref and $3FFF;
        if Mem.Rom[Ref].Lbl<>'' then
          Args:=Mem.Rom[Ref].Lbl
        else
          Args:=Word2Oct(Mem.Rom[i].Value+Mem.Rom[i+1].Value*256);
        if Add>0 then
          Args:=Args+'+'+Word2Oct(Add);
        ByteCount:=2;
        Compress:=False;
      end else if Mem.Rom[i].ValueKind<>EAdditional then begin //Its something else
        if Mem.Rom[i].Value=0 then begin //DB 0 can be compressed
          if Mem.Rom[i].Compress=ckCompressOn then begin //Start signal for compression?
            Compress:=True;
            CompressStart:=i;
          end;
          if Compress then //If compression selected count bytes to compress
            inc(CompressCount);
        end else
          Compress:=False;
        Mnemonic:='DB';
        Args:=Byte2Oct(Mem.Rom[i].Value);
        ByteCount:=1;
      end else //Please the compiler
        ByteCount:=1;

      //Comment string for data inclusion
      Bytes:=Word2Oct(i)+' ';
      for j:=0 to ByteCount-1 do begin
        if chr(Mem.Rom[i+j].Value) in [#33..#126] then
          Bytes:=Bytes+chr(Mem.Rom[i+j].Value)
        else
          Bytes:=Bytes+'.';
      end;

      if (Mem.Rom[i].Compress=ckCompressOff) and (CompressCount>0) then begin
        AssLine(Mem.Rom[CompressStart].Lbl,'DB',Word2Oct2(CompressCount)+' dup (0)','',Mem.Rom[CompressStart].Comment,True);
        CompressCount:=0;
      end;

      if not(Compress) then begin
        Comment:=Mem.Rom[i].Comment;

        AssLine(Lbl,Mnemonic,Args,Bytes,Comment,Mem.Rom[i].Break);
      end;
    end;
  end;

  WriteLn(OutFile,'');
  WriteLn(OutFile,Format('%-11s %-6s',['','NEWPAGE']));
  Symtab.WriteDefines(OutFile);
  for i:=8192 to 16383 do begin
    if Mem.Rom[i].Lbl<>'' then
      WriteLn(OutFile,Format('%-11s %-6s',[Mem.Rom[i].Lbl+':','EQU '+Word2Oct(i)]));
  end;

  WriteLn(OutFile,'');
  WriteLn(OutFile,'            END');

  CloseFile(Outfile);
end;


procedure TForm1.LoadOpcodes;

var
  OpcFile : TStringList;
  i,j     : integer;
  Line    : TStringList;
  S       : string;
  Opcode  : byte;

begin
  OpcFile:=TStringList.Create;
  OpcFile.LoadFromFile('D:\d\Projekt\2640\firmware\8008.csv');
  Line:=TStringList.Create;

  for i:=1 to OpcFile.Count-1 do begin
    Line.DelimitedText:=OpcFile[i];
    S:=Line[2];
    if S<>'' then begin
      Delete(S,length(S),1);
      Opcode:=0;
      for j:=1 to length(s) do begin
        Opcode:=Opcode*8;
        Opcode:=Opcode+(ord(S[j])-48);
      end;
      Opcodes[Opcode].Opcode:=Opcode;
      Opcodes[Opcode].Mnemonic:=Line[6];
      Opcodes[Opcode].Args:=Line[7];
      Opcodes[Opcode].Bytes:=StrToInt(Line[8]);
      Opcodes[Opcode].Break:=Line[9]='True';
    end;
  end;

  OpcFile.Free;

end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  LoadOpcodes;

  Mem:=TMemory.Create;
  CtlFile:=TControlFile.Create();
  CtlFile.Mem:=Mem;
  Mem.Clear;
  Symtab:=TSymbolTable.Create();
  CtlFile.Symtab:=Symtab;

  CtlFile.Load(gOptions.InputFile);

  DisasmPass1;
  DisasmPass2;
  Mem.Free;

  Application.Terminate;
end;

procedure TForm1.AssLine(Lbl, Mnemonic, Args, Bytes, Comment: string;
  Break: boolean);

var
  OutLine : string;

begin
  if Lbl<>'' then Lbl:=Lbl+':';
  OutLine:=Format('%-11s %-6s %-15s',[Lbl,Mnemonic,Args]);
  if gOptions.Addr then
    OutLine:=OutLine + Format(' ; %-12s',[Bytes]);
  if Comment<>'' then
    OutLine:=OutLine + ' ; '+trim(Format('%-80s',[Comment]));
  WriteLn(OutFile,OutLine);
  if Break then
    WriteLn(OutFile,'');

end;

end.
