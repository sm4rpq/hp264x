unit symtable;

interface

uses
  Classes;

type
  TSymbolTable = class(TObject)
  private
    SymToAdr : TStringList;
    AdrToSym : TStringList;
    Equates : TStringList;
  public
    constructor Create(); virtual;
    destructor Destroy();
    procedure Insert(isLabel : boolean; Symbol : string; Address : word);

    function getAddress(Symbol : string) : word;
    function getSymbol(Address : word) : string;

    procedure WriteDefines(var OutFile : TextFile);
  end;

implementation

uses
  SysUtils, NumberSystems;

{ TSymbolTable }

constructor TSymbolTable.Create;
begin
  SymToAdr:=TStringList.Create;
  AdrToSym:=TStringList.Create;
  Equates:=TStringList.Create;
end;

destructor TSymbolTable.Destroy;
begin
  SymToAdr.Free;
  AdrToSym.Free;
  Equates.Free;
end;

function TSymbolTable.getAddress(Symbol: string): word;

var
  RefEquateVal : string;
  RefEquateBin : word;

begin
  RefEquateVal:=SymToAdr.Values[Symbol]; //Fetch value of referred equate
  RefEquateBin:=Oct2Int(RefEquateVal); //Get its binary value
  Result:=RefEquateBin;
end;

function TSymbolTable.getSymbol(Address: word): string;
begin
  Address:=Address or $3F00;
  Result:=AdrToSym.Values[IntToStr(Address)]; 
end;

procedure TSymbolTable.Insert(isLabel : boolean; Symbol: string; Address: word);
begin
  SymToAdr.Values[Symbol]:=Word2Oct(Address);
  AdrToSym.Values[IntToStr(Address)]:=Symbol;
  if not(isLabel) then
     Equates.Add(Format('%-11s %-8s',[Symbol+':','EQU '+Word2Oct(Address)]));
end;

procedure TSymbolTable.WriteDefines(var OutFile: TextFile);

var
  i : integer;

begin
  for i:=0 to Equates.Count-1 do begin
    WriteLn(OutFile,Equates[i]);
  end;
end;

end.
