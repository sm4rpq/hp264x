unit Memory;

interface

uses
  Classes;

type
  TKind = (EUnknown,EMnemonic,EAdditional,EDB,EDW,EAdr,ETxt);
  TCompressKind = (ckNone,ckCompressOn,ckCompressOff);
  TPgmByte = record
    Value : Byte;
    Lbl : string;
    Mnemonic : string;
    Args : string;
    PreComments : TStringList;
    Comment : string;
    PageBreak : boolean;
    Break : boolean;
    ValueKind : TKind;
    Ref : string;
    Compress : TCompressKind;
    InvokeComment : string;
  end;
  TMemory = class(TObject)
  private
  public
    Rom : array[0..16383] of TPgmByte;
    procedure Clear;
    procedure Load(RomFile : string);
  end;

implementation

uses
  SysUtils;

procedure TMemory.Clear;

var
  i : integer;
  
begin
  for i:=0 to 16383 do begin
    Rom[i].Value:=0;
    Rom[i].ValueKind:=EUnknown;
    Rom[i].Lbl:='';
  end;
end;

procedure TMemory.Load(RomFile : string);

var
  HexFile : TStringList;
  i,j : integer;

  Line : string;
  Temp : string;
  ByteCount : integer;
  Adr : integer;
  RecType : integer;
  AByte : byte;

begin
  HexFile:=TStringList.Create;
  HexFile.LoadFromFile(RomFile);

  for i:=0 to HexFile.count-1 do begin
    Line:=HexFile[i];

    Temp:=Copy(Line,2,2);
    ByteCount:=StrToInt('0x'+LowerCase(Temp));

    Temp:=Copy(Line,4,4);
    Adr:=StrToInt('0x'+LowerCase(Temp));

    Temp:=Copy(Line,8,2);
    RecType:=StrToInt('0x'+LowerCase(Temp));

    if RecType=0 then begin
      for j:=0 to ByteCount-1 do begin
        Temp:=Copy(Line,10+j*2,2);
        AByte:=StrToInt('0x'+LowerCase(Temp));

        Rom[Adr+j].Value:=AByte;
        Rom[Adr+j].Mnemonic:='';
        Rom[Adr+j].Args:='';
      end;
    end;
  end;

  HexFile.Free;
end;

end.
