unit ControlFile;

interface

uses
  Classes,
  Memory, Symtable;

type
  TControlFile = class(TObject)
  private
  public
    Mem : TMemory;
    Symtab : TSymbolTable;
    Entrypoints : TList;
    constructor Create(); virtual;
    destructor Destroy();
    procedure Load(CtlFile : String);
  end;

implementation

uses
  SysUtils, Dialogs,
  NumberSystems;

constructor TControlFile.Create();
begin
  Entrypoints:=TList.Create;
end;

destructor TControlFile.Destroy();
begin
  EntryPoints.Free;
end;

procedure TControlFile.Load(CtlFile : string);

var
  ControlFile : TStringList;
  i : integer;
  Row : string;
  Parsed : TStringList;
  Adr : integer;
  Ref : integer;
  CurPath : string;
  FilePath : string;

begin
  ControlFile:=TStringList.Create;
  ControlFile.LoadFromFile(CtlFile);
  Parsed:=TStringList.Create;
  CurPath:=ExtractFilePath(CtlFile);
  if CurPath='' then
    GetDir(0,CurPath);

  for i:=0 to ControlFile.Count-1 do begin
    Row:=ControlFile[i];
    if (Row<>'') then begin //Skip blank lines
      Parsed.DelimitedText:=Row;
      try
        if (Parsed[0]<>'#') then begin //Skip blank lines and comments
          if Parsed[0]='INCLUDE' then begin
            FilePath:=ExtractFilePath(Parsed[1]);
            if FilePath='' then
              Load(CurPath+Parsed[1])
            else
              Load(Parsed[1]);
          end else if Parsed[0]='LOAD' then begin
            FilePath:=ExtractFilePath(Parsed[1]);
            if FilePath='' then
              Mem.Load(CurPath+Parsed[1])
            else
              Mem.Load(Parsed[1]);

          end else if Parsed[0]='C' then begin //Interpret as code
            Adr:=Oct2Int(Parsed[1]);
            EntryPoints.Add(Pointer(Adr));

          end else if Parsed[0]='L' then begin //Sets label at adr
            Adr:=Oct2Int(Parsed[1]);
            Mem.Rom[Adr].Lbl:=Parsed[2];
            Symtab.Insert(true,Parsed[2],Adr);

          end else if Parsed[0]=';' then begin //Online comment at adr
            Adr:=Oct2Int(Parsed[1]);
            Mem.Rom[Adr].Comment:=Parsed[2];

          end else if Parsed[0]='I' then begin //Online comment at adr
            Adr:=Oct2Int(Parsed[1]);
            Mem.Rom[Adr].InvokeComment:=Parsed[2];

          end else if Parsed[0]=':' then begin //Pre-line comment at adr
            Adr:=Oct2Int(Parsed[1]);
            if not assigned(Mem.Rom[Adr].PreComments) then
              Mem.Rom[Adr].PreComments:=TStringList.Create;
            Mem.Rom[Adr].PreComments.Add(Parsed[2]);

          end else if Parsed[0]='A' then begin //Interpret as a pointer to something
            Adr:=Oct2Int(Parsed[1]);
            Mem.Rom[Adr].ValueKind:=EAdr;
            Mem.Rom[Adr+1].ValueKind:=EAdditional;
            Ref:=Mem.Rom[Adr].Value+Mem.Rom[Adr+1].Value*256;
            Ref:=Ref and 16383;
            try
              if Mem.Rom[Ref].Lbl='' then
                Mem.Rom[Ref].Lbl:='R_'+Word2Oct(Ref);
            except
              showMessage(IntToStr(Ref));
            end;

          end else if Parsed[0]='E' then begin //Define an equate
            Adr:=Oct2Int(Parsed[1]);
            Symtab.Insert(False,Parsed[2],Adr);

          end else if Parsed[0]='F' then begin //Reference an equate
            Adr:=Oct2Int(Parsed[1]);
            Mem.Rom[Adr].Ref:=Parsed[2];

          end else if Parsed[0]='P' then begin //Page directive
            Adr:=Oct2Int(Parsed[1]);
            Mem.Rom[Adr].PageBreak:=True;
          end else if Parsed[0]='Z' then begin //Compress directive
            Adr:=Oct2Int(Parsed[1]);
            Mem.Rom[Adr].Compress:=ckCompressOn;
            Adr:=Oct2Int(Parsed[2]);
            Mem.Rom[Adr].Compress:=ckCompressOff;
          end;
        end; //Not comment
      except
        ShowMessage(Row);
      end;
    end; //Empty line
  end;

  ControlFile.Free;
end;


end.
