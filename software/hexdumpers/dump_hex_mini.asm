; Mini dump program for HP 2640 terminal.
; This program dumps the ROMs of the 2640 terminal to the serial port as HEX.
; No line terminations, no init, nothing except just a dump of the rom but small
; enough to type in directly from the keyboard if needed.
;
; The program does absolutely work on 2640A. Tested.
; It should work on 2640B,C,N and S. They have almost the same hardware and seem
; to have the same memory layout and amount of ROM.
; With minor modification it would probably work on a 2644A. The 2644A has a
; larger ROM space so the CPI 40Q should be replace with CPI 60Q to get all the
; rom content. Otherwise it should probably work unchanged.
; No other hardware requirements as it loads into the upper 1K of RAM that is
; the minimum needed for a 2640 to work at all.
;
; The program will NOT work on any of the other 264X series as they all use
; other processors (8080 and 8085) and some even have banked memory.
;
; Assemble with Alfred Arnolds AS using -L -listradix 8
;
        CPU     8008NEW
        ORG     36000Q

        XRA     A
        MOV     H,A
        MOV     L,A

BYTE:	MOV     A,M             ; Get Byte from ROM
        RRC                     ; Upper nibble to lower
        RRC
        RRC
        RRC
        CALL    SENDDGT         ; Send nibble
        MOV     A,M
        CALL    SENDDGT         ; Send nibble

        INR     L               ; Point to next location
        JNZ     NEXT            ; If LSB not overflowing skip
        INR     H               ; Increment MSB if LSB overflowed

NEXT:	MOV     A,H             ; Get MSB of byte pointer
        CPI     40Q             ; Check for out of ROM
        JM      BYTE            ; Repeat while in ROM
        JMP     0               ; Reboot

; Sends the value in A as one HEX character over serial
SENDDGT:ANI     017Q            ; Mask nibble
        ADI     '0'             ; Add 0 to get 0..?
        CPI     ':'             ; Check if ascii 0..9
        JM      .A              ; Yes, skip additional add
        ADI     7               ; Add 7 more to move :..? into A..F
.A:     MOV     C,A             ; Transmits from C
.B:     MVI     A,060Q          ; Get Serial port status
        IN      0Q
        ANI     002Q            ; Mask Bit 1 = Transmit Holding Register Empty/*Full
        JZ      .B              ; Bit will be 1 if register empty
        MOV     A,C             ; Get byte to transmit
        OUT     30Q             ; Output byte to UART

        RET                     ; Done

        END