LOAD "1818-0174.HEX"

P 020000
C 020000
L 020000 E_CLRMEM

L 020076 C_CR
F 020076 KEYOPTAH
; 020076 "Let HL point to Keyboard options A-H register"
; 020100 "Get Keyboard Options"
; 020101 "Mask B1 = Space Overwrite Latch"
; 020103 "Jump if 0, space overwrite latch disabled"

; 020112 "Let HL point to Cursor X position"
; 020114 "Clear A"
; 020115 "Set Cursor X position to 0"

L 020112 E_HOMELINE

C 020117
L 020117 E_MEMLCKON

C 021102
L 021102 E_DELCHAR

C 022137
L 022137 E_INSLINE

C 022312 Jump via tabell?
L 022312 E_DELLINE

C 023046
L 023046 E_SETTAB

C 023054
L 023054 E_CLEARTAB

C 023115
L 023115 C_HT

C 023213
C 023222
C 023303 Jump via tabell?
L 023303 E_FMTMODE_ON
; 023303 "Format mode LED on"

L 023310 E_HOMECURUP

; 024273 "Output Cursor X Position"

L 025022 OUTCURY
; 025025 "Output Cursor Y Position (and Display on/DMA on)"
C 025321
C 025324
; 025345 "Output Cursor X Position"
C 025360

C 026001
; 026117 "Output Cursor X Position"
; 026253 "Output Cursor X Position"
; 026277 "Output Cursor X Position"
; 026324 "Rotate 'Auto-LF' into Carry"

P 027230
: 027230 "I guess this is some status reporting. It gets lots of things and"
: 027230 "seem to send it to the serial port after som formatting"

; 027302 "Rotate 'Caps-Lock' into Carry"

; 027352 "Let HL point to DISABBITS"
; 027354 "Get Disable bits"
; 027355 "Mask bit 5 = Self test running flag"
; 027357 "If Bit 5 set, jump back to selftest code"

P 027373
; 027375 "CRC 0"
; 027376 "CRC 1"
; 027377 "ROM Checksum"

