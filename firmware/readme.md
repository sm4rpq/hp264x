Disassembly of firmware from a 2640A and a 2640B. They are quite similar as 3/4 of the ROMs are identical.
The 1/4 that is different is the first part of the code where comm, keyboard and printer handling seem to be.

Work in progress at low pace. Most done in the 2640A code so similar code blocks in the 2640B may exist but
is not identified directly.

Using a disassembler I write at the same time, adding features as needed. Numbers are in split octal. That is: After reaching
377 the next number is 001000 instead of 400. This was done as the 8008 processor is best handled with octal numbers and
split octal is easiest to identify addresses from when there are no 16-bit registers and still 16-bit addresses.

The problem is that I have no assembler that swallows this code. I was looking at Alfred Arnolds Macro Assembler that is very
competent but does not understand split octal. I have also looked at Herb Johnsons A8008, but Thats distributed in source
form and I have no C-compiler at home. (I am quite fed up with C/C++ at work). SBASM is also a candidate but it does not support
1975-variant of 8008 opcodes which for me is easier to work with as an old 8085 programmer.

