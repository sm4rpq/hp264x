; This is an auto-generated file. Do not edit!
            CPU    8008NEW
            RADIX  8
            PAGE   101Q

            ORG    000000

; Firmware for the 2640A terminal.

; This is a decode of the updated set of ROMs called 1818-0172 -- 1818-0175
; There was an earlier set 1818-0130 -- 1818-0133 but these have not been
; found yet by someone and uploaded to bitsavers
; St.Adr ROM        Rev Date     Reason for Revision
; 0k     1818-0172  April '76    Serial Printer Operation
; 2k     1818-0173  April '76    Cursor Positioning Screen
; 4k     1818-0174  April '76    Relative
; 6k     1818-0175  April '76    Cursor Home Down

; RAM is located at top of address space. At least 1k RAM from
; 15k need to exist. If more memory is added it will be dynamically
; detected and needs to be configured to provide continuous memory
; downwards until it meets ROMs for a max of 8k RAM. (The 8008 CPU
; only has 16k address space)

; This disassembly uses octal as far as possible and to make addresses
; possible to understand also split octal notation where 16 bits are
; specified/shown.

; Disassembly done for research and education purposes only.

RESET:      JMP    BOOTCODE       

            DB     000            
INITALTC:   DB     020             ; 'Default' alternative character set
NORMALTABPTR:
            DW     NORMALTAB       ; Pointer to 'default' parse table
            DB     000            

            NEWPAGE
; Interrupt routine to handle incoming bytes over the serial line
; Since interrupts are really not available on the 8008 RST10 statements
; are inserted here and there and everywhere in the rest of the code
; the hardware detects the RST 10 opcode and flips a bit if interrupt
; is not signalled making the opcode into a MOV instead

RST10:      MOV    A,L             ; Save registers C,L,H
            MOV    B,H            
            MVI    L,INTREGSL & 377Q ; 'Push' registers starting here
            MVI    H,INTREGSL >> 10Q
            MOV    M,A             ; L at 077347
            INR    L              
            MOV    M,B             ; H at 077350
            INR    L              
            MOV    M,C             ; C at 077351
            MVI    A,060           ; Input status word from serial port
            IN     0              
            MOV    B,A             ; Save RX status in B
            RRC                    ; Rotate status bit 0 into carry
            RNC                    ; Return if status bit 0 was 0 (no data received)
            MVI    A,020           ; Input received data from serial port
            IN     0              
            ANI    177             ; Mask B7 from received byte. IE receive only 7 bits
            JZ     RXIGNORE        ; Jump if NUL received
            CPI    177            
            JZ     RXIGNORE        ; Jump if DEL received
            MOV    C,A             ; Save received character in C
            MOV    A,B             ; Get rx status
            ANI    014             ; Mask bits B3 (Parity error) and B2 (Overrun error)
            JZ     RXOK           
RXERROR:    MVI    C,377           ; Override received character with O377
            MVI    L,COMMSTATUS & 377Q ; Let HL point to Communication status register
            MOV    A,M             ; Fetch from COMMSTATUS
            ORI    200             ; Set bit 7 (not used on comm card)
            MOV    M,A             ; Write back
RXOK:       MVI    L,RXBUFPTRIN & 377Q ; Get Receive buffer input point
            MOV    A,M             ; Fetch LSB part of receive buffer input pointer
            SUI    001             ; Decrement by 1 to get next point to place input characters
            CPI    RXBUFLOX & 377Q ; Passed end of input buffer?
            JNZ    RXUPDBUFP       ; Jump if NOT passed end of buffer
            ADI    121             ; 'ADI RXBUFHI-RXBUFLOX'   Sets pointer back to RXBUFHI
RXUPDBUFP:  MOV    M,A             ; Write back new low part of pointer to RXBUFPTRIN
            DCR    L               ; HL points to RXBUFPTROUT
            CMP    M               ; RXBUFPTRIN=RXBUFPTROUT?
            MOV    L,A             ; Set HL to point to what RXBUFIN contains (Input point)
            JZ     RXERROR         ; If the RXBUFPTRIN=RXBUFPTROUT the buffer was full. Jump to error handler
            MOV    M,C             ; Store the received character where RXBUFPTRIN points
RXIGNORE:   MVI    L,INTREGSC & 377Q ; 'Pull' registers starting here
            MOV    C,M             ; Restore C from 077351
            DCR    L              
            MOV    B,M             ; Restore H from 077350
            DCR    L              
            MOV    L,M             ; Restore L from 077347
            MOV    H,B            
            RET                   

            NEWPAGE
; This is a list of conversions from scan-code to character code
; codes <=177Q are actual character codes
; codes >=200Q are pseudo-characters handled outside the scan routine

; codes 200-206 are actions to perform locally
; codes 220-259 are numeric keypad characters +160Q
; codes 261-262 are tab control
; codes 301-326 are display control
; codes 360-367 are function keys

KEYMAP:     DB     000             ; Keyboard scan code 000 = Lock
            DB     033             ; Keyboard scan code 001 = Esc
            DB     000             ; Keyboard scan code 002 = Cntl
            DB     000             ; Keyboard scan code 003 = LEFT SHIFT
            DB     000             ; Keyboard scan code 004 = RIGHT SHIFT
            DB     241             ; Keyboard scan code 005 = NUM 1
            DB     244             ; Keyboard scan code 006 = NUM 4
            DB     012             ; Keyboard scan code 007 = Line Feed
            DB     202             ; Keyboard scan code 010 = Remote
            DB     061             ; Keyboard scan code 011
            DB     161             ; Keyboard scan code 012
            DB     172             ; Keyboard scan code 013
            DB     015             ; Keyboard scan code 014 = Return
            DB     242             ; Keyboard scan code 015 = NUM 2
            DB     245             ; Keyboard scan code 016 = NUM 5
            DB     134             ; Keyboard scan code 017
            DB     000             ; Keyboard scan code 020 = Caps Lock
            DB     062             ; Keyboard scan code 021
            DB     167             ; Keyboard scan code 022
            DB     170             ; Keyboard scan code 023
            DB     135             ; Keyboard scan code 024
            DB     243             ; Keyboard scan code 025 = NUM 3
            DB     246             ; Keyboard scan code 026 = MUM 6
            DB     364             ; Keyboard scan code 027 = F5, Back Space
            DB     204             ; Keyboard scan code 030 = Memory Lock
            DB     063             ; Keyboard scan code 031
            DB     145             ; Keyboard scan code 032
            DB     143             ; Keyboard scan code 033
            DB     072             ; Keyboard scan code 034
            DB     304             ; Keyboard scan code 035 = Arrow Left
            DB     323             ; Keyboard scan code 036 = Roll Up
            DB     365             ; Keyboard scan code 037 = F6, Cancel Line
            DB     000             ; Keyboard scan code 040 = Auto LF
            DB     064             ; Keyboard scan code 041
            DB     162             ; Keyboard scan code 042
            DB     166             ; Keyboard scan code 043
            DB     073             ; Keyboard scan code 044
            DB     310             ; Keyboard scan code 045 = Arrow UpLeft
            DB     301             ; Keyboard scan code 046 = Arrow Up
            DB     366             ; Keyboard scan code 047 = F7, Test
            DB     200             ; Keyboard scan code 050 = Break
            DB     065             ; Keyboard scan code 051
            DB     164             ; Keyboard scan code 052
            DB     142             ; Keyboard scan code 053
            DB     154             ; Keyboard scan code 054
            DB     303             ; Keyboard scan code 055 = Arrow Right
            DB     325             ; Keyboard scan code 056 = Next Page
            DB     367             ; Keyboard scan code 057 = F8, Print
            DB     305             ; Keyboard scan code 060 = (no switch)
            DB     066             ; Keyboard scan code 061
            DB     171             ; Keyboard scan code 062
            DB     040             ; Keyboard scan code 063 = Space
            DB     153             ; Keyboard scan code 064
            DB     326             ; Keyboard scan code 065 = Prev Page
            DB     312             ; Keyboard scan code 066 = Clear Dsply
            DB     205             ; Keyboard scan code 067 = Insert Character
            DB     201             ; Keyboard scan code 070 = Display Functions
            DB     067             ; Keyboard scan code 071
            DB     165             ; Keyboard scan code 072
            DB     156             ; Keyboard scan code 073
            DB     152             ; Keyboard scan code 074
            DB     302             ; Keyboard scan code 075 = Arrow Down
            DB     261             ; Keyboard scan code 076 = Set Tab
            DB     320             ; Keyboard scan code 077 = Delete Character
            DB     000             ; Keyboard scan code 100 = Block Mode
            DB     070             ; Keyboard scan code 101
            DB     151             ; Keyboard scan code 102
            DB     155             ; Keyboard scan code 103
            DB     150             ; Keyboard scan code 104
            DB     324             ; Keyboard scan code 105 = Roll Down
            DB     262             ; Keyboard scan code 106 = Clear Tab
            DB     315             ; Keyboard scan code 107 = Delete Line
            DB     000             ; Keyboard scan code 110 = Blind (not used)
            DB     011             ; Keyboard scan code 111 = Tab
            DB     157             ; Keyboard scan code 112
            DB     054             ; Keyboard scan code 113
            DB     147             ; Keyboard scan code 114
            DB     236             ; Keyboard scan code 115 = Num .
            DB     251             ; Keyboard scan code 116 = Num 9
            DB     314             ; Keyboard scan code 117 = Insert Line
            DB     000             ; Keyboard scan code 120 = Blind (not used)
            DB     071             ; Keyboard scan code 121
            DB     160             ; Keyboard scan code 122
            DB     056             ; Keyboard scan code 123
            DB     146             ; Keyboard scan code 124
            DB     240             ; Keyboard scan code 125 = Num 0
            DB     250             ; Keyboard scan code 126 = Num 8
            DB     363             ; Keyboard scan code 127 = F4, Format Mode
            DB     000             ; Keyboard scan code 130 = Blind (not used)
            DB     060             ; Keyboard scan code 131
            DB     100             ; Keyboard scan code 132
            DB     057             ; Keyboard scan code 133
            DB     144             ; Keyboard scan code 134
            DB     000             ; Keyboard scan code 135 = (no switch)
            DB     247             ; Keyboard scan code 136 = Num 7
            DB     362             ; Keyboard scan code 137 = F3, End Unprotected Field
            DB     000             ; Keyboard scan code 140 = Blind (not used)
            DB     055             ; Keyboard scan code 141
            DB     133             ; Keyboard scan code 142
            DB     000             ; Keyboard scan code 143 = (no switch)
            DB     163             ; Keyboard scan code 144
            DB     000             ; Keyboard scan code 145 = (no switch)
            DB     000             ; Keyboard scan code 146 = (no switch)
            DB     361             ; Keyboard scan code 147 = F2, Start Unprotected Field
            DB     203             ; Keyboard scan code 150 = Enter
            DB     136             ; Keyboard scan code 151
            DB     137             ; Keyboard scan code 152
            DB     000             ; Keyboard scan code 153 = (no switch)
            DB     141             ; Keyboard scan code 154
            DB     000             ; Keyboard scan code 155 = (no switch)
            DB     000             ; Keyboard scan code 156 = (no switch)
            DB     360             ; Keyboard scan code 157 = F1, Enhance Display

; Depending on ctl-key state and keyboard option switch E
; function keys may need a re-map from value listed above to
; values listed here

FUNCKMAP:   DB     277             ; F1, Enhance Display
            DB     333             ; F2, Start Unprotected Field
            DB     335             ; F3, End Unprotected Field
            DB     206             ; F4, Format Mode
            DB     010             ; F5, Back Space
            DB     030             ; F6, Cancel Line
            DB     372             ; F7, Test
            DB     260             ; F8, Print

; This jump table lists adresses of handlers for function keys when
; their local function is used, that is when not used as a function
; key to be sent to the serial line

JMPTAB1:    DW     SENDBREAK       ; 200 = Break
            DW     FK_DISPFUNK     ; 201 = Display functions
            DW     FK_REMOTE       ; 202 = Remote
            DW     FK_ENTER        ; 203 = Enter
            DW     FK_MEMLCK       ; 204 = Memory Lock
            DW     FK_INSCH        ; 205 = Insert Character
            DW     FK_FMTMDE       ; 206 = Format mode

            NEWPAGE
; Keyboard scanning routine

; Each call to this routine will scan one column of the keyboard
; and detect if a key was pressed. If it was the actual ascii value
; of the pressed key will be returned in C. Some status value in A
; and flags will be set but not clear how yet.
; If the returned code is >=200Q it is indication that some special
; key was pressed.

KEYSCAN:    RST    010Q            ; Poll serial interrupt
            MVI    L,COLNDX & 377Q ; Let HL point to Column Index variable
            MOV    B,M            
            DCR    B               ; Decrement the Column Index for use (It's initialized to 1 more than number of columns)
            JM     SCANRESET       ; If it was 0 (became negative), jump to re-initialize of scanning
            MOV    M,B             ; Store the new (decremented) value again
            MVI    L,COLSTATEPTR & 377Q ; Let HL Point to pointer to column state bytes
            MOV    E,M             ; Get column status pointer (LSB only, MSB assumed 077)
            DCR    E               ; Decrement to point to next column status byte (No checking, follows COLNDX)
            MOV    M,E             ; Store again
            MOV    L,E             ; Let HL point to column status byte
            MOV    A,M             ; Get column status
            OUT    31Q             ; Output previous key state (Sets thresholds for comparators)
            MOV    A,B             ; Get Column Index
            ORI    020             ; Set A4 for keyboard address on following IN
            IN     1               ; Input keyboard column status
            MOV    E,A             ; Save new Column Status in E
            XRA    M               ; XOR with old column state to find any changed key state
            RZ                     ; If no key changed state, just return. Nothing more to do

            MOV    B,A             ; Store key-change-state in B
            XRA    A               ; Clear A
            SUB    B               ; Inverts all bits of key-change except the lowest 1. '00010010' becomes '11101110'
            ANA    B               ; And with key-state-change. If only 1 key's state changed => We are back to original
            MOV    C,A             ; Store single bit in C. If >1 keys changed we get only lowest changed bit
            ANA    E               ; AND with key press status. Sets flags. If a new key was pressed, Z:=0
            MOV    A,M             ; HL still points to column status. Get old column status.
            JNZ    KEYDOWN         ; If a key was pressed, Z is not set, Jump

KEYUP:      XRA    C               ; Mask of lowest set bit. Aka clears the bit for 'lowest' key released
            MOV    M,A             ; Store 'old' column status with bit for lowest released key cleared
            CALL   GETKEYOFS       ; Calculate scancode in keycode table. Returns with A,B=Offset and HL=KEYSCANCDE
            CMP    M               ; Compare this scancode with earlier scancode
            JZ     L_030126        ; Jump if same scancode => Same key as last pressed released again.
NOKEY:      XRA    A               ; Other key released. Clear A to indicate no key pressed
            RET                    ; Done, return to sender

; A Word on handling of key status:
; By clearing and setting only one bit at the time the thresholds are kept
; as they were to ensure detection of pressed/released buttons on next scan
; when more than one key's state is changed at the same time
; even though only one key's state is handled for each column scan.
KEYDOWN:    ORA    C               ; Turn on bit for lowest changed (set) key in column
            MOV    M,A             ; Store 'old' column status with bit for lowest pressed key set
            MVI    L,KEYREPTMR & 377Q ; Let HL point to Key repeat timer
            MVI    M,000           ; Disable key repeat timer
            CALL   GETKEYOFS       ; Calculate offset in keycode table. Returns with A,B=Offset and HL=KEYSCANCDE
            MOV    M,A             ; Store scancode at KEYSCANCDE
            MVI    H,KEYMAP >> 10Q
            ADI    KEYMAP & 377Q   ; Add table address to scancode
            MOV    L,A             ; Table address calculated. Let HL point to table entry.
            MOV    A,M             ; Get keycode (Ascii-code or pseudo character code)
            MVI    H,077           ; Set H back to last page of RAM
            ORA    A               ; Set flags based on keycode
            RZ                     ; If keycode=0, return. The pressed key should not be handled here. (shift/ctrl/...)
            MOV    B,A             ; Save keycode for later
            MVI    L,COL00STATE & 377Q ; Let HL point to Column 0 state register.
            MOV    A,M             ; Get Column 0 key state.  Contains Cntl-key state.
            RRC                    ; Rotates 'Lock' key state into Carry
            RRC                    ; Rotates 'Esc' key state into Carry
            RRC                    ; Rotates 'Cntl' key state into Carry
            MOV    A,B             ; Get Keycode back
            JNC    NOTCNTL         ; Jump if 'Cntl' was not pressed
            JM     REMAP1CTL       ; Jump if bit 7 in keycode was set

            CPI    100            
            JM     NOTCNTL         ; Jump if character < '@'. Cntl does not apply.
            ANI    037             ; Charcode>='@'. Mask lower 5 bits. => 32 characters '@'..'`'
            JMP    KEYMAPPED       ; Letter now shifted to Control block. Store and return.

NOTCNTL:    JM     REMAP1NCTL      ; If charcode negative (B7 set) handle pseudo characters
            CPI    041            
            JM     KEYMAPPED       ; If charcode<'!' (space or control character) store and return.
            CPI    060            
            JZ     KEYMAPPED       ; If character='0' store and return. 0-key has no shifted variant
            MOV    A,M             ; HL still points to COL00STATE. Get Column 0 key state.
            ANI    031             ; Mask bits 4-2. which is LEFT SHIFT, RIGHT SHIFT and Cntl
            MOV    A,B             ; Get input character
            JZ     KEYMAPPED       ; Jump if all flags cleared. No shift key held down.
            ANI    100             ; Test b6 of input character code. Z=1 if not set.
            MVI    A,020          
            JZ     DOSHIFT         ; Jump if b6 not set. Aka Code<'@'
            RLC                    ; A=040, CY=0
DOSHIFT:    XRA    B               ; Inverts b4 or b5 of input char. Aka '1'->'!' and 'A'->'a'
KEYMAPPED:  MVI    L,KBDCHAR & 377Q ; Let HL point to KBDCHAR register. This is where the final character is stored.
            MOV    M,A             ; Save final shifted char at KBDCHAR
            MOV    C,A             ; Copy final char to C
            MVI    L,265           ; Let HL point to byte with disablebits (keyboard disable on bit 4)
            MOV    A,M             ; Get Disablebits
            ANI    020             ; Mask bit 4
            CPI    020             ; Check if bit 4 set
            RZ                     ; Return if bit 4 set. (Kbd disabled) Keycode returned with Z=1 (No code)
            MVI    L,233           ; Let HL point to Key repeat timer.
            MVI    M,050           ; Set Initial Delay Interval to 50. This is C1 from HP-Journal June 1975, p21
            CALL   FKDELAYS        ; Other delays may apply to some function keys. Fix it.
            ORA    H               ; Sets A to 377 and Z=0 to indicate good keyboard return. C=Keycode
            RET                   

FKDELAYS:   MOV    A,C             ; Get char from C
            ORA    A               ; Set flags
            RP                     ; Return if not special pseudo-character
            CPI    301             ; Compare with 'Arrow up'
            RM                     ; If < Arrow up, return. No special delay.
            CPI    305             ; Compare with 'Arrow Left'+1
            JM     NAVKEYDLY       ; Navigation keys, jump. Special delay
            CPI    323             ; Compare with 'Roll up'
            RM                     ; Return. No special delay
            CPI    325             ; Compare with 'Roll down'+1
            JM     NAVKEYDLY       ; Navigation keys, jump. Special delay
            CPI    360             ; Compare with 'F1'
            RM                    
            CPI    370             ; Compare with 'F8'+1
            RP                    
            MVI    M,000           ; Function keys. Disable repeat timer
            RET                   

NAVKEYDLY:  MVI    M,030           ; Set Initial Delay Intervval to 30 for navigation keys.
            RET                   

; Remapping of non-alphanumeric keys when Cntl was NOT pressed.

; Numeric pad. Remapped to 0-9 if Cntl not pressed.
REMAP1NCTL: CPI    220             ; Numeric keypad keycode limit low
            JC     REMAP2NCTL      ; If carry, Not a numeric keypad key, try function keys
            CPI    260             ; Numeric keypad keycode limit high
            JNC    REMAP2NCTL      ; If not carry, Not a numeric keypad key, try function keys
            SUI    160             ; Subtract 160Q to get keycode into ascii range
            JMP    KEYMAPPED       ; Jump and handle as non-shifted key

; Function keys remapped XOR options switch E
REMAP2NCTL: CPI    360             ; F1?
            JC     KEYMAPPED       ; If carry, Not a Function Key, return to store as is
            CPI    370             ; F8+1?
            JNC    KEYMAPPED       ; If not carry, Not a Function Key, return to store as is
            MVI    C,020           ; Expected value of options switch E to not remap
REMAPFK:    MVI    L,KEYOPTAH & 377Q ; Let HL point to keyboard options switch register
            MOV    A,M             ; Get keyboard options A-H
            ANI    020             ; Mask switch E 0=tx with cntl 1=tx without cntl
            CMP    C               ; Flag for cntl or not cntl C=020 if Cntl not pressed
            MOV    A,B             ; Get Charcode into Acc
            JZ     KEYMAPPED       ; If Options Switch=Flag, return to store as is
; If Options Switch<>Flag, look up a keycode from a table
            MVI    H,FUNCKMAP >> 10Q
            SUI    360             ; Table offset = Keycode-360 ('F1'-360 = 0 ...)
            ADI    FUNCKMAP & 377Q ; Table address = Offset + Table base
            MOV    L,A             ; Let HL point to table entry for pressed Function Key
            MOV    A,M             ; Get new keycode
            MVI    H,077           ; Restore HL to point in top RAM page
            JMP    KEYMAPPED       ; Remapping done, return to store

; Clear display key remapped from 312 to 313 when ctl is pressed
REMAP1CTL:  CPI    312             ; Clear display?
            JNZ    REMAP2CTL       ; If not clear display, jump
            MVI    A,313           ; Pseudo-charcode 313Q
            JMP    KEYMAPPED      

; Home key remapped from 310 to 306 when ctl is pressed
REMAP2CTL:  CPI    310             ; Arrow up-left (home)?
            JNZ    REMAP3CTL      
            MVI    A,306           ; Pseudo-charcode 306Q
            JMP    KEYMAPPED      

; Function keys remapped XOR options switch E
REMAP3CTL:  CPI    360             ; F1?
            JC     REMAP1NCTL      ; If carry, Not a Function Key, Jump
            CPI    370             ; F8+1?
            JNC    REMAP1NCTL      ; If not carry, Not a Function Key, Jump
            MVI    C,000          
            JMP    REMAPFK        

; Called when all columns have been scanned
; Resets column counter and status pointer
SCANRESET:  MVI    M,016           ; Re-start COLNDX at 16 (decremented before use to 15)
            MVI    L,COLSTATEPTR & 377Q ; HL points to Column status pointer
            MVI    M,COLSTATEPTR & 377Q ; Re-start at COL16STATE+1 = COLSTATEPTR
            MVI    L,256           ; Let HL point to Blink Timer
            MOV    B,M             ; Get current timer value. 0=Disabled, =1 repeat >1 count down
            DCR    B               ; Decrement timer value
            JM     KEYREPEAT       ; If it became<0 it was 0 and repeat is disabled. Finished, go on to key repeat
            MOV    M,B             ; Store new timer value
            JNZ    KEYREPEAT       ; If value still>0 it is finished, go on to key repeat
            MVI    M,020           ; Timer timeout. Start over at 20
            MVI    L,KBDLEDS & 377Q ; HL points to KBDLEDS register
            MOV    A,M             ; Get previous set LED/Buzzer state
            XRI    004             ; Invert B2 = Keyboard LED3 = Memory Lock
            MOV    M,A             ; Set back to KBDLLEDS register
            OUT    11Q             ; Output to LED Latch and Alarm Generator
KEYREPEAT:  CALL   COMCONTL        ; Set comm card settings from keyboard switches
            CALL   L_031207       
            MVI    L,KEYREPTMR & 377Q ; Let HL point to Key repeat timer
            MOV    B,M             ; Get current timer value. 0=Disabled, =1 repeat >1 count down
            DCR    B               ; Decrement timer value
            JM     NOKEY           ; If it became<0 it was 0 and repeat is disabled. Finished, return with blank
            MOV    M,B             ; Store new timer value
            JNZ    NOKEY           ; If value still>0 it is finished, return with blank
            MVI    M,003           ; Timer timeout. New Delay Interval to 3. This is C2 from HP-Journal June 1975, p21
            MVI    L,234           ; Let HL point to last keycode detected
            MOV    C,M             ; Fetch character
            JMP    L_013006       

; Translate key state change bit + scan colum into scancode
GETKEYOFS:  MVI    L,COLNDX & 377Q ; Get current column number
            MOV    A,M            
            RLC                    ; Multiply by 8. (8 keys / column)
            RLC                   
            RLC                   
            MOV    B,A             ; Upper nibble of offset ready, move to B
            DCR    B               ; Prepare for bit-counting
            MOV    A,C             ; Get bit representing newly pressed key
COUNTBITS:  INR    B               ; Increment offset for each bitshift
            RRC                    ; Rotate bit right, ends up in CY if rotated out of B0
            JNC    COUNTBITS       ; If CY was not set, more counting. If CY was set B contains col*8+Bitnumber
            MVI    L,KEYSCANCDE & 377Q ; Let HL point to Key scan code
            MOV    A,B             ; Table offset into A
            RET                   

            NEWPAGE
GATED_TX:   RST    010Q            ; Check Receive
            MVI    L,COMMSETS & 377Q ; Let HL point to Communication settings register
            MOV    A,M             ; Get latest setting for comm
            RRC                    ; Rotate CA (RTS) bit into carry
            JC     BEEP            ; If B0=1, CA (RTS) is off, just beep
DO_TX:      MVI    A,060           ; Input status word from serial port
            IN     0              
            ANI    002             ; Mask Bit 1 = Transmit Holding Register Empty/*Full
            JNZ    TX_RXCHAR       ; Jump if buffer not full
            CALL   COMCONTL        ; Set comm card settings from keyboard switches
            JMP    GATED_TX       

TX_RXCHAR:  MVI    L,RXCHAR & 377Q ; <----RXCHAR
            MOV    A,M            
            OUT    30Q             ; Output serial port character
            CPI    015             ; CR?
            JZ     L_036215        ; Jump if transmit was CR
            CPI    036             ; RS? (Record Separator)
            RNZ                    ; Return if NOT record separator
            JMP    L_036215       

SENDBREAK:  MVI    L,COMMSETS & 377Q ; Let HL point to Communication settings register
            MOV    A,M             ; Get latest setting for comm
            MOV    C,A            
            ORI    100             ; Set break bit active
            OUT    10Q             ; Output to Serial Port Control
            MVI    L,024           ; Set 24 octal = 20decimal * 10 milliseconds delay
            CALL   DLY_n10ms       ; Delay 10ms*value in L
            MOV    A,C             ; Get Pre-break comm settings
            OUT    10Q             ; Output to Serial Port Control
            JMP    MAIN_LOOP      

E_MDMDISC:  MVI    A,170           ; Turn CD on on serial port
            IN     0              
            MVI    L,144           ; Set 144 octal = 100decimal * 10 milliseconds delay
            CALL   DLY_n10ms       ; Delay 10ms*value in L
            MVI    A,160           ; Turn CD off on serial port
            IN     0              
            RET                   

            NEWPAGE
; DO_RX: Fetch a character from the receive buffer

; Returns the fetched character in A
; Special cases:
; If 'ENQ' was received, it will send an Ack and return 0
; If no character is available in the buffer will return 377
DO_RX:      RST    010Q           
            MVI    L,RXBUFPTROUT & 377Q ; Let HL point to receive buffer output pointer
            MOV    A,M             ; Get LSB part of output pointer
            INR    L               ; Let HL point to receive buffer input pointer
            CMP    M               ; Compare the pointers to see if anything in the buffer
            JZ     RXBUFEMPTY      ; If they are equal, buffer is empty. Jump away
            DCR    L               ; Let HL point to receive buffer output pointer again
            SUI    001             ; Move the pointer to next character
            CPI    RXBUFLOX & 377Q ; Hit the last position?
            JNZ    RXWRAP          ; If not moved beyond end of buffer, skip wrap
            ADI    121             ; Move receive buffer output pointer to other end of ringbuffer
RXWRAP:     MOV    M,A             ; Write new buffer output position
            MOV    L,A             ; Let HL point to the character to get from the buffer
            MOV    A,M             ; Get next received character from ringbuffer
            CPI    005             ; Was it ENQ?
            RNZ                    ; If not, return with the received character in A
RXENQ:      CALL   COMCONTL        ; Set comm card settings from keyboard switches
            MVI    L,COMMSETS & 377Q ; Let HL point to Communication settings register
            MOV    A,M             ; Get latest setting for comm
            RRC                    ; Rotate CA (RTS) bit into carry
            JC     RXENQ           ; If B0=1, repeat from RXENQ
            MVI    L,RXCHAR & 377Q ; <----RXCHAR
            MVI    M,006           ; Ack
            CALL   GATED_TX       
            XRA    A              
            RET                   

RXBUFEMPTY: MVI    A,060           ; Input status word from serial port
            IN     0              
            ANI    001             ; Mask receive buffer status
            MOV    A,H             ; Set A to 377
            RZ                     ; Return if receive buffer empty
            CALL   RST10           ; Poll receiver immediately
            JMP    DO_RX          


            NEWPAGE
; This is likely the main loop
MAIN_INIT:  MVI    H,CURYPOS >> 10Q ; Restore HL to point in top RAM page
            CALL   OUTCURY         ; Out cursor Y position and set L to cursor variable
            INR    L               ; Pointer HL to CURXPOS
            MOV    A,M             ; Get Cursor X position
            OUT    13Q             ; Output Cursor X Position
MAIN_LOOP:  MVI    L,266           ; <----FLAGS
            MOV    A,M            
            ANI    010            
            JNZ    L_031274       
            CALL   KEYSCAN        
            JNZ    KBDEVENT       
            RST    010Q           
            MVI    L,COL01STATE & 377Q ; Let HL point to status of keyboard column 1
            MOV    A,M             ; Get Colum 1 status
            RRC                    ; Rotate REMOTE key state into carry
            JNC    MAIN_LOOP       ; If not pressed, repeat
            MVI    L,256           ; <----BLINKTMR
            MOV    A,M            
            ORA    A              
            JZ     L_031274       
            CALL   DO_RX           ; Get character from receive buffer. Return in A, Z=0 if available
            JMP    MAIN_LOOP      

; Jumps here when a key press has been detected
KBDEVENT:   MVI    A,376          
            CALL   ENABITS        
            MOV    A,C             ; Get keycode for pressed key
            ORA    A               ; Set flags
            JP     KBDEVASC        ; If ascii-code, jump to ascii-handler
KBDEVFUN:   CPI    207             ; Codes 200-206 are always terminal local functions
            JNC    KBDEVESCF       ; Jump if 'Esc'-function
            SUI    200             ; Gets keycode into range 0-6
            MVI    L,233           ; Let HL point to key repeat timer
            MVI    M,000           ; Turn off key repeat timer. Local functions should not be repeated.
; Prepare jump instruction in RAM to jump to function handler for terminal local function
            MVI    E,JMPTAB1 & 377Q ; Point to jump table
            MVI    H,JMPTAB1 >> 10Q
            CALL   GET_JMP_ADR     ; Get jump address. H=Pointer High E=Pointer Low A=Index
            JMP    RAMJMP          ; Jump to jump instruction in RAM which jumps to function according to table

KBDEVESCF:  CPI    360             ; Keycode for 'F1' key
            JC     KBDEVESC2      
            CPI    370             ; Keycode for 'F8'+1 key
            JC     KBDEVFUNC       ; It is a function key, jump to Function Key Handler
KBDEVESC2:  MVI    L,235          
            MVI    M,001          
            MVI    C,033           ; 'Esc'
            JMP    KBDEVCTL       

; Keyboard event ascii handler
; Called when an ASCII-character is typed on the keyboard
; That is: characters 000-177
KBDEVASC:   JM     KBDEVFUN       
            CPI    040             ; Compare with 'Space'
            JM     KBDEVCTL        ; Jump if non-printable character (control char)
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JNZ    L_003021        ; Jump if Z=0. That is: if format mode active
            MVI    L,271           ; <----ESCSEM
            MOV    A,M            
            ORA    A              
            JNZ    L_003021       
            MVI    L,321           ; Let HL point to Cursor X position
            MOV    A,M             ; Get cursor X position
            CPI    107             ; Is the cursor in column 107Q (71d)?
            CZ     BEEP            ; If it is in column 71d ring the bell
L_003021:   MOV    A,C             ; Get Character typed
            CPI    140             ; Compare with CHR$(96). First 'lowercase' character
            JM     KBDEVCTL        ; If < then already uppercase, skip any uppercasing coming up
            MVI    L,COL02STATE & 377Q ; Let HL point to Keyboard Column 2 state
            MOV    A,M             ; Get Column 2 state
            RRC                    ; Rotate Caps Lock into Carry
            JNC    KBDEVCTL        ; If 'caps lock' not set, skip any uppercasing coming up
            MOV    A,C             ; Get Character typed
            CPI    177             ; Compare with 'DEL'
            JZ     KBDEVCTL        ; If it is 'DEL' skip uppercasing
            SUI    040             ; Subtract 040 to make lowercase into uppercase
L_003046:   MOV    C,A             ; Save character
KBDEVCTL:   MVI    L,324           ; <----RXCHAR
            MOV    M,C            
            INR    L               ; Let HL point to 077325
            MOV    M,C            
            RST    010Q           
            CALL   L_033117        ; --- Clear 077204
            CALL   PARSECHAR       ; Find handler for character in C from table at PARSETABPTR. Sets RAMJMP
            MVI    H,077          
            MVI    A,277           ; Clear bit 6
            CALL   CLRFLAGS        ; Clear 'Flags' indicated by ~A
            DCR    L               ; Let HL point to Disable Bits (DISABBITS)
            MOV    A,M            
            ANI    001            
            CZ     L_031076       
            JNZ    .NONPRINT      
            MVI    L,234           ; Let HL point to Keyboard character code
            MOV    A,M             ; Get keyboard character
            ORA    A               ; Update flags
            JP     .PRINTABLE      ; Jump if Bit7 cleared = Printable or control character
            MVI    L,240           ; Let HL point to Keyboard LED status register
            MOV    A,M             ; Get keyboard LEDS status
            RRC                    ; Rotate display functions status into Carry
            JC     .PRINTABLE     
            MVI    L,KEYOPTAH & 377Q ; Let HL point to Keyboard options A-H register
            MOV    A,M             ; Get Keyboard Options
            RRC                    ; Rotate Function Key Transmission bit into carry
            JNC    .NONPRINT       ; Jump if 0 = Function keys are to be interpreted not transmitted
.PRINTABLE: MVI    L,237           ; Let HL point to Communication settings (from keyboard)
            MOV    A,M             ; Get communication settings
            RLC                    ; Rotate Bit 7 (full/half duplex) into carry
            JC     TXKEY           ; Jump if bit7=1, full duplex selected (Lesson learned: Don't trust documentation)
            MVI    A,100          
            CALL   SETFLAGS        ; Set 'Flags' indicated by A
.NONPRINT:  MVI    L,271           ; Load ESCape semaphore
            MOV    A,M            
            ORA    A              
            JNZ    .SKIP           ; Jump if it's up (In processing of any esc-sequence)
            MOV    A,C            
            CPI    040             ; ' '
            JP     .DOPRINT        ; Jump if char >=' '
            CPI    036             ; RS (Record Separator)
            JZ     .DOPRINT        ; Jump if it is RS
            MVI    L,KBDLEDS & 377Q ; Load Keyboard LED state
            MOV    A,M            
            RRC                    ; Rotate B0 = Display Functions into carry
            JNC    .SKIP           ; Jump if Display Functions is not active
.DOPRINT:   CALL   L_010302       
.SKIP:      RST    010Q           
            MVI    L,266           ; <----FLAGS
            MVI    H,077          
            MOV    A,M            
            ANI    100            
            CNZ    GATED_TX       
            RST    010Q           
            CALL   RAMJMP          ; Jump to jump instruction in RAM = Handle char as current tables says
STRETURN:   RST    010Q           
            MVI    L,271           ; <----ESCSEM
            MVI    H,077          
            MOV    B,M            
            DCR    B              
            JM     L_003236       
            MOV    M,B            
            CZ     STNORMAL       
L_003236:   MVI    L,324           ; <----RXCHAR
            MOV    A,M            
            MVI    L,252          
            MOV    M,A            
L_003244:   MVI    L,265           ; <----DISABBITS
            MOV    A,M            
            ANI    201            
            JNZ    MAIN_INIT      
            MVI    L,324           ; <----RXCHAR
            MOV    A,M            
            CPI    015            
            JNZ    L_003303       
            MVI    L,217           ; <----COL04STATE
            MOV    A,M            
            RRC                    ; Rotate Auto-LF into Carry
            JNC    MAIN_INIT      
            CALL   DLY_10ms        ; Delay 10ms
            MVI    C,012          
            JMP    KBDEVCTL       

L_003303:   MVI    L,234           ; <----KBDCHAR
            MOV    A,M            
            CPI    277            
            JZ     L_003335       
            INR    L              
            MOV    B,M            
            DCR    B              
            JNZ    MAIN_INIT      
            MOV    M,B            
            ANI    177            
            JMP    L_003046       

TXKEY:      CALL   GATED_TX       
            JMP    L_003244       

L_003335:   MVI    C,046          
            MVI    M,344          
            CALL   L_030126       
            JMP    KBDEVCTL       

; Called when SO character received in STNORMAL
C_SO:       MVI    L,270           ; <----ALTCSET
            MOV    A,M            
            JMP    L_037267       

; Called when SI character received in STNORMAL
C_SI:       XRA    A              
            JMP    L_037267       


            NEWPAGE
; This is where the settings on the COMM card is updated from the
; keyboard switch settings
COMCONTL:   MVI    L,COMMSETS & 377Q ; Let HL point to current communication settings register
            MOV    A,M             ; Fetch current communication settings
            ANI    001             ; Mask B0, Current CA (RTS) state. It's not available from keyboard switches
            MOV    B,A             ; Save for later (A destroyed by fetch from keyboard)
            MVI    A,037           ; Input keyboard comm settings
            IN     1              
            ORA    B               ; Add the kept CA (RTS) bit from above
            MOV    M,A             ; Save current settings in COMCONTL register
            OUT    10Q             ; Output to Serial Port Control
            CALL   POLLCOMSTAT     ; Get Communication Interface Status + Keep flagbit 7. On return A=New status B=Old C=actual interface
            ANI    040             ; Mask bit 5 = CB (CTS)
            MVI    L,KBDLEDS & 377Q ; Let HL point to Keyboard LED and Alarm register
            MOV    A,M             ; Fetch current setting
            JZ     L_004015        ; Jump to set if CB (CTS) is 0 (on)
            ANI    367             ; Clear bit 3 = Transmit LED
            JMP    L_004017       

L_004015:   ORI    010             ; Set bit 3 = Transmit LED
L_004017:   MOV    M,A             ; Store new state in Keyboard LED register
            OUT    11Q             ; Output to LED Latch and Alarm Generator
            MVI    L,COMMSETS & 377Q ; Let HL point to Communication settings register
            MOV    A,M             ; Get current Communication Settings
            RRC                    ; Rotate Bit 0 = CA (RTS) state into carry
            MOV    A,B             ; Get CA (RTS) status bit0 all other 0
            JNC    TXSTATE         ; Jump if CA bit 0 = CA (RTS) on => Transmit State
; Receive State
            XRA    C              
            ANI    020            
            ANA    C              
            RZ                    
            CALL   L_004101       
            MVI    E,000          
L_004043:   DCR    E              
            JZ     L_004101       
            CALL   L_004116       
            ANI    140            
            JNZ    L_004043       
            MVI    A,201          
            JMP    SETFLAGS       

TXSTATE:    XRA    C              
            ANI    100            
            ANA    C              
            RZ                    
            CALL   CLRRXBUF        ; Clear receive buffer
            MVI    A,177          
            CALL   CLRFLAGS        ; Clear 'Flags' indicated by ~A
L_004101:   MVI    L,237           ; <----COMMSETS
            MOV    A,M            
            XRI    001            
            MOV    M,A            
            OUT    10Q             ; Output to Serial Port Control
            RRC                   
            MVI    L,024          
            JNC    L_004120       
L_004116:   MVI    L,001          
L_004120:   CALL   DLY_n10ms       ; Delay 10ms*value in L

            NEWPAGE
; Reads status from Comm Interface and saves.
; Keeps the 'fake' status bit 7 not used by the interface itself.
POLLCOMSTAT:
            MVI    A,060           ; Input status word from serial port
            IN     0              
            MOV    C,A             ; Save real status in C for a while
            MVI    L,COMMSTATUS & 377Q ; Let HL point to COMMSTATUS register
            MOV    A,M             ; Get old status register content
            MOV    B,A             ; Save in B (will also be returned)
            ANI    200             ; Mask bit 7, which whill be kept from before call
            ORA    C               ; Add bits from actual interface
            MOV    M,A             ; Save as current status
            RET                   

            NEWPAGE
E_PRINTMEM: CALL   E_HOMECURUP    
            MVI    L,257          
            MVI    M,000          
            CALL   L_005244       
            JZ     L_004323       
            MVI    L,335          
            MOV    E,M            
            INR    L              
            MOV    D,M            
L_004162:   MVI    L,260          
            MVI    M,377          
L_004166:   CALL   L_030233       
            JNZ    L_004272       
            MOV    B,A            
            CPI    314            
            JZ     L_004261       
            ORA    A              
            JP     L_004227       
            CPI    300            
            JZ     L_004162       
            CPI    301            
            JNZ    L_004166       
            MVI    L,260          
            MVI    M,000          
            JMP    L_004166       

L_004227:   CPI    040            
            JM     L_004250       
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JZ     L_004252       
            MVI    L,260          
            ANA    M              
            JZ     L_004252       
L_004250:   MVI    B,040          
L_004252:   CALL   L_005142       
            JZ     L_004166       
            RET                   

L_004261:   MOV    A,E            
            ANI    360            
            ADI    002            
            MOV    E,A            
            CALL   L_030233       
L_004272:   MVI    B,015          
            CALL   L_005142       
            MVI    B,012          
            CALL   L_005142       
            CALL   L_030235       
            INR    E              
            CPI    316            
            JNZ    L_004162       
            MVI    B,014          
            CALL   L_005142       
            RET                   

L_004323:   MVI    L,257          
            MVI    H,077          
            MVI    M,377          
            MVI    L,265           ; <----DISABBITS
            MOV    A,M            
            ANI    001            
            RNZ                   
            MVI    B,337           ; Point to 'PRINT FAIL' message
            MVI    C,256          
            CALL   SETDISP        
            CALL   BEEP           
L_004351:   MOV    A,H            
            OUT    31Q             ; Output previous key state
            MVI    A,021           ; Input keyboard column 1 (status of keys 010-017)
            IN     1              
            ANI    020            
            JZ     L_004351       
            MVI    L,214           ; <----COL01STATE
            ORA    M              
            MOV    M,A            
            MVI    L,341          
            MOV    C,M            
            INR    L              
            MOV    B,M            
            CALL   SETDISP        
            ORA    A              
            JMP    OUTCURY        

L_005003:   MVI    E,377          
L_005005:   MOV    H,B            
            RST    010Q           
            MVI    A,020          
            IN     6              
            ORA    A              
            JP     L_004323       
            RRC                   
            MOV    D,A            
            JC     L_005050       
            RRC                   
            MOV    A,B            
            JNC    L_005032       
            XRI    377            
L_005032:   OUT    36Q            
            MOV    A,D            
            RRC                   
            RRC                   
            JC     L_005044       
            MVI    A,022          
            IN     6              
L_005044:   MVI    H,077          
            XRA    A              
            RET                   

L_005050:   DCR    L              
            JNZ    L_005005       
            DCR    E              
            JZ     L_004323       
            JMP    L_005005       

L_005063:   MVI    E,377          
L_005065:   MOV    H,B            
            RST    010Q           
            MVI    A,060           ; Input status word from printer port
            IN     2              
            ANI    042            
            CPI    002            
            JNZ    L_005127       
            MVI    A,120           ; Input FW Control word from printer port
            IN     2              
            ANI    340            
            JNZ    L_005121       
            MVI    A,060           ; Input status word from printer port
            IN     2              
            ANI    100            
            JZ     L_005127       
L_005121:   MOV    A,B            
            OUT    32Q            
            MVI    H,077          
            XRA    A              
            RET                   

L_005127:   DCR    L              
            JNZ    L_005065       
            DCR    E              
            JZ     L_004323       
            JMP    L_005065       

L_005142:   MVI    L,345          
            MOV    M,D            
            MVI    L,344          
            MOV    M,E            
            MVI    L,261          
            MOV    D,M            
            DCR    D              
            JNZ    L_005166       
            CALL   L_005003       
            RNZ                   
            JMP    L_005234       

L_005166:   CALL   L_005063       
            RNZ                   
            MVI    D,001          
            MOV    A,B            
            CPI    040            
            JNC    L_005234       
            CPI    015            
            JZ     L_005222       
            MVI    A,120          
            IN     2              
            ANI    340            
            JZ     L_005222       
            RAR                   
            RAR                   
            MOV    D,A            
L_005222:   MVI    B,000          
L_005224:   CALL   L_005063       
            RNZ                   
            DCR    D              
            JNZ    L_005224       
L_005234:   MVI    L,345          
            MOV    D,M            
            MVI    L,344          
            MOV    E,M            
            XRA    A              
            RET                   

L_005244:   MVI    L,261          
            XRA    A              
            ORA    M              
            RET                   

            DB     000            
            DB     000            
            DB     000            

            NEWPAGE
; Parse table for 'normal' state
NORMALTAB:  DB     040            
            DB     177            
            DW     C_PRINTABLE+200000 ; Any printable character
            DB     007            
            DB     012            
            DW     R_005304       
            DB     015            
            DB     021            
            DW     R_005314       
            DB     033            
            DB     033            
            DW     STESC+200000    ; Esc
            DB     036            
            DB     036            
            DW     C_PRINTABLE+200000 ; Rs
            DB     000            
            DB     177            
            DW     SKIPCHAR+200000 ; Any other character
R_005304:   DW     BEEP            ; 007 = Bel
            DW     C_BS            ; 010 = BS
            DW     C_HT            ; 011 = HT
            DW     C_LF            ; 012 = LF
R_005314:   DW     C_CR            ; 015 = CR
            DW     C_SO            ; 016 = SO
            DW     C_SI            ; 017 = SI
            DW     SKIPCHAR        ; 020 = DLE
            DW     C_DC1           ; 021 = DC1 = XON

            NEWPAGE
L_005326:   CALL   QRYBLKMODE      ; Check if Block Mode. Return with Z=1 if in Block Mode
            JZ     L_032235       
L_005334:   MVI    A,036           ; RS (Record Separator)
            JMP    L_037312       

L_005341:   CALL   L_036215       
            MVI    A,022           ; DC2
            CALL   L_037312       
            CALL   L_032121       
            RNZ                   
            JMP    L_032235       

L_005360:   MVI    A,037           ; US (Unit Separator)
            JMP    L_037312       

L_005365:   CPI    036             ; RS (Record Separator)
            RET                   

L_005370:   ORI    377            
            RET                   

NOTHING:    RET                   

QRYDFUNCS:  MVI    A,001          
QRYLEDS:    MVI    L,KBDLEDS & 377Q ; <----KBDLEDS
            ANA    M              
            RET                   

DIS_FUN_ON: MVI    A,001          
            JMP    LEDON          

DIS_FUN_OFF:
            MVI    A,376          
            JMP    LEDOFF         

MEM_LO_ON:  MVI    A,004          
            JMP    LEDON          

MEM_LO_OFF: MVI    A,373          
            JMP    LEDOFF         

; Called when DC1 (XON) character received in STNORMAL
C_DC1:      MVI    L,237           ; <----COMMSETS
            MOV    A,M            
            ANI    001            
            XRI    001            
            JMP    SETFLAGS       


            NEWPAGE
EMPTY0:     DB     523 dup (0)    

EMPTY1:     DB     77 dup (0)     


            NEWPAGE
P0C_DC1:    JMP    C_DC1          

P0_GATED_TX:
            JMP    GATED_TX       

P0_POLLCOMSTAT:
            JMP    POLLCOMSTAT    

P0_DO_RX:   JMP    DO_RX          

P0E_MDMDISC:
            JMP    E_MDMDISC      

P0_MAIN_INIT:
            JMP    MAIN_INIT      

P0_MAIN_LOOP:
            JMP    MAIN_LOOP      

P0_KBDEVESC2:
            JMP    KBDEVESC2      

P0_KBDEVCTL:
            JMP    KBDEVCTL       

P0_STRETURN:
            JMP    STRETURN       

L_007320:   JMP    L_005326       

L_007323:   JMP    L_005360       

L_007326:   JMP    L_005334       

L_007331:   JMP    L_005341       

L_007334:   JMP    L_005365       

L_007337:   JMP    L_005370       

P0E_PRINTMEM:
            JMP    E_PRINTMEM     

P0_NOTHING1:
            JMP    NOTHING        

P0_NOTHING2:
            JMP    NOTHING        

P0_QRYDFUNCS:
            JMP    QRYDFUNCS      

P0_QRYLEDS: JMP    QRYLEDS        

L_007361:   JMP    DIS_FUN_ON     

L_007364:   JMP    DIS_FUN_OFF    

L_007367:   JMP    MEM_LO_ON      

L_007372:   JMP    MEM_LO_OFF     


            NEWPAGE
            DB     353             ; CRC 0
            DB     350             ; CRC 1
            DB     260             ; ROM Checksum

            NEWPAGE
; Parse table for Escape sequences (Esc ...)
ESCTAB:     DB     046            
            DB     046            
            DW     STESCAMP+200000 ; Esc & = Continues in ESCAMPTAB
            DB     051            
            DB     051            
            DW     STESCCPAR+200000 ; Esc ) = Continues in ESCCPARTAB
            DB     060            
            DB     062            
            DW     R_010040       
            DB     101            
            DB     146            
            DW     R_010046       
            DB     154            
            DB     155            
            DW     R_010162       
            DB     172            
            DB     172            
            DW     SELFTEST+200000 ; Esc z = Selftest
            DB     001            
            DB     177            
            DW     STNORMAL+200000 ; Esc all other non-null characters, Not used, go to normal state
            DB     000            
            DB     000            
            DW     SKIPCHAR+200000 ; Esc NL = Accept but ignore, just wait for next char in STESC
R_010040:   DW     P0E_PRINTMEM    ; Esc 0 = Print memory
            DW     E_SETTAB        ; Esc 1 = Sets a tab at the current cursor column
            DW     E_CLEARTAB      ; Esc 2 = Clears a tab at the current cursor column.
R_010046:   DW     E_CURUP         ; Esc A = Cursor Up
            DW     E_CURDOWN       ; Esc B = Cursor Down
            DW     E_CURRIGHT      ; Esc C = Cursor Right
            DW     E_CURLEFT       ; Esc D = Cursor Left
            DW     E_RESTERM       ; Esc E = Reset terminal
            DW     E_HOMECURDN     ; Esc F = Cursor Home Down (See 2640-B-N-S reference manual B-2)
            DW     E_HOMELINE      ; Esc G = Cursor to first column of current line
            DW     E_HOMECURUP     ; Esc H = Cursor Home Up
            DW     C_HT            ; Esc I = Horizontal Tab
            DW     E_CLRMEM        ; Esc J = Clear memory (and display) from current cursor position to the end
            DW     E_CLRLINE       ; Esc K = Clear current line from cursor position to end
            DW     E_INSLINE       ; Esc L = Insert Line
            DW     E_DELLINE       ; Esc M = Delete Line
            DW     STNORMAL        ; Esc N = Not used, go to normal state
            DW     STNORMAL        ; Esc O = Not used, go to normal state
            DW     E_DELCHAR       ; Esc P = Delete Char
            DW     E_INSCH_ON      ; Esc Q = Insert Character On
            DW     E_INSCH_OFF     ; Esc R = Insert Character Off
            DW     E_ROLLUP        ; Esc S = Roll Up
            DW     E_ROLLDOWN      ; Esc T = Roll Down
            DW     E_NEXTPAGE      ; Esc U = Next Page
            DW     E_PREVPAGE      ; Esc V = Prev Page
            DW     E_FMTMODE_ON    ; Esc W = Format Mode On
            DW     E_FMTMODE_OFF   ; Esc X = Format Mode Off
            DW     STDISPFUNC      ; Esc Y = Display Functions On
            DW     STNORMAL        ; Esc Z = Display Functions Off, but only works when on. Go to normal state
            DW     E_STARTUNPROT   ; Esc [ = Start Unprotected Field
            DW     STNORMAL        ; Esc \ = Not used, go to normal state
            DW     E_ENDUNPROT     ; Esc ] = End Unprotected Field?
            DW     E_TXSTATUS      ; Esc ^ = Transmit Terminal Status?
            DW     STNORMAL        ; Esc _ = Not used, go to normal state
            DW     E_TXCURREL      ; Esc ` = Cursor Sensing Relative (See 2640B-N-S reference manual p. B-2)
            DW     E_TXCURABS      ; Esc a = Transmit cursor address
            DW     E_ENABLKBD      ; Esc b = Enable terminal keyboard
            DW     E_DISABKBD      ; Esc c = Disable terminal keyboard (except reset)
            DW     E_TXBLOCK       ; Esc d = Begin information transmission
            DW     STNORMAL        ; Esc e = Not used, go to normal state
            DW     P0E_MDMDISC     ; Esc f = Modem Disconnect (See 2640B-N-S reference manual p. B-2)
R_010162:   DW     E_MEMLCKON      ; Esc l = Memory Lock On
            DW     E_MEMLCKOFF     ; Esc m = Memory Lock Off

            NEWPAGE
L_010166:   MOV    C,A            
            MVI    L,204          
            MOV    B,M            
            DCR    B              
            RM                    
            MOV    M,B            
            CPI    040            
            RM                    
            MVI    L,262           ; <----UNKN1
            MOV    B,M            
            MVI    L,322          
            MOV    D,M            
            INR    L              
            MOV    H,M            
            MOV    L,D            
            MOV    A,M            
            ORA    A              
            RM                    
            DCR    L              
            MOV    A,M            
            ORA    A              
            JP     L_010235       
            CPI    314            
            RNZ                   
            DCR    L              
            MOV    B,A            
            MOV    A,M            
            CPI    302            
            RNZ                   
            MOV    M,B            
            INR    L              
L_010235:   MOV    A,C            
            CMP    B              
            JZ     L_010243       
            MOV    M,C            
L_010243:   MOV    E,L            
            MOV    D,H            
            CALL   L_032365       
            MVI    L,332          
            MOV    B,M            
            INR    B              
            MOV    M,B            
            CALL   C_PRINTABLE     ; Display character in register A at current output position
            MVI    L,321           ; <----CURXPOS
            MOV    A,M            
            OUT    13Q             ; Output Cursor X Position
            CMP    A              
            RET                   

E_ENHDISP:  MVI    L,276          
            MVI    M,060          
            MVI    L,325           ; <----PRCHAR
            MOV    A,M            
            ANI    017            
            ORI    200            
L_010301:   MOV    M,A            
L_010302:   RST    010Q           
            MVI    L,325           ; <----PRCHAR
            MOV    A,M            
            ORA    A              
            JP     L_011134       
            MVI    L,321           ; <----CURXPOS
            MOV    C,M            
            DCR    C              
            CALL   L_012270       
            JM     BEEP           
            JNZ    L_011110       
            MOV    C,A            
            INR    C              
            CALL   L_033107       
L_010334:   JZ     L_010354       
            DCR    E              
            CALL   L_023225       
            JNZ    L_011221       
            MVI    L,254          
            MOV    M,A            
            MVI    L,321           ; <----CURXPOS
            MOV    C,M            
L_010354:   CALL   L_030233       
            MOV    B,A            
            MVI    L,325           ; <----PRCHAR
            ORA    A              
            JP     L_011053       
            CPI    314            
            JZ     L_011053       
            ANI    100            
            MOV    A,M            
            JNZ    L_011020       
            ANI    100            
            JNZ    L_010354       
            MVI    L,276          
            MOV    A,M            
            ANA    B              
            MVI    L,325           ; <----PRCHAR
            ORA    M              
            JMP    L_011251       

L_011020:   CMP    B              
            RZ                    
            ANI    100            
            MOV    A,B            
            JZ     L_011040       
            CPI    300            
            JZ     L_010354       
            JMP    L_020143       

L_011040:   CPI    300            
            JNZ    L_010354       
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JMP    L_010334       

L_011053:   MOV    A,M            
            ANI    100            
            JNZ    L_011076       
            MVI    L,276          
            MOV    A,M            
            MVI    L,326          
            ANA    M              
            MVI    L,325           ; <----PRCHAR
            ORA    M              
            MOV    M,A            
            MVI    L,326          
            MOV    M,A            
L_011076:   MOV    A,B            
            ORA    A              
            JP     L_020143       
            MVI    C,000          
            JMP    L_011300       

L_011110:   DCR    C              
            JNZ    L_011300       
            CALL   L_033107       
            JNZ    L_011221       
            MVI    L,366          
            MVI    M,001          
            CALL   L_011341       
            JMP    L_011311       

L_011134:   CALL   L_012263       
            JZ     L_011173       
L_011142:   JM     L_011161       
            DCR    C              
            JNZ    L_011272       
            CPI    117            
            JNZ    L_011300       
            JMP    L_011213       

L_011161:   CALL   BEEP           
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            RZ                    
            JMP    L_011237       

L_011173:   MOV    C,A            
            MVI    L,240           ; <----KBDLEDS
            MOV    A,M            
            ANI    002            
            JZ     L_011213       
            MVI    L,353          
            MOV    A,M            
            ORA    A              
            JZ     L_020135       
L_011213:   CALL   L_033107       
            JZ     L_011246       
L_011221:   CALL   BEEP           
            MVI    L,325           ; <----PRCHAR
            MOV    A,M            
            ORA    A              
            RM                    
            CALL   L_024125       
            JNZ    L_010302       
L_011237:   CALL   E_HOMECURUP    
L_011242:   JNZ    L_010302       
            RET                   

L_011246:   MVI    L,325           ; <----PRCHAR
            MOV    A,M            
L_011251:   MVI    L,262           ; <----UNKN1
            CMP    M              
            RZ                    
            MOV    L,E            
            MOV    H,D            
            MOV    M,A            
            MOV    B,A            
            ADD    A              
            RNC                   
            RM                    
            MVI    L,326          
            MVI    H,077          
            MOV    M,B            
            RET                   

L_011272:   ADD    C              
            CPI    117            
            JZ     L_011110       
L_011300:   CALL   L_033105       
            JNZ    L_011221       
            CALL   L_011325       
L_011311:   ORA    A              
            RZ                    
            RST    010Q           
            MVI    L,366          
            MVI    H,077          
            MOV    A,M            
            ORA    A              
            JMP    L_011242       

L_011325:   RST    010Q           
            INR    C              
            CALL   L_014062       
            DCR    C              
            JM     L_012211       
            MVI    L,366          
            MOV    M,C            
L_011341:   MVI    L,357          
            CALL   L_032374       
            DCR    C              
            JM     L_012242       
            MVI    C,040          
            CALL   L_014166       
            RZ                    
            MOV    E,A            
            MOV    D,B            
            ORI    017            
            CALL   L_033011       
            MVI    L,366          
            MOV    A,M            
            MVI    B,000          
L_011374:   INR    B              
            SUI    016            
            JP     L_011374       
            MVI    L,316          
            MOV    M,A            
            DCR    B              
            JZ     L_012064       
            MVI    L,364          
            MOV    M,B            
            CALL   L_032372       
L_012017:   MVI    C,040          
            CALL   L_014166       
            MVI    L,361          
            MVI    H,077          
            MOV    E,M            
            JZ     L_012230       
            MOV    M,A            
            INR    L              
            MOV    D,M            
            MOV    M,B            
            MOV    L,E            
            MOV    H,D            
            MOV    E,A            
            MOV    D,B            
            DCR    L              
            ORI    017            
            MOV    M,B            
            DCR    L              
            MOV    M,A            
            MVI    L,364          
            MVI    H,077          
            MOV    B,M            
            DCR    B              
            MOV    M,B            
            JNZ    L_012017       
L_012064:   RST    010Q           
            MVI    L,316          
            XRA    A              
            SUB    M              
            MOV    C,A            
            ADD    E              
            MOV    B,A            
            DCR    B              
            MVI    L,321           ; <----CURXPOS
            MOV    A,M            
            CPI    117            
            MVI    L,325           ; <----PRCHAR
            MOV    A,M            
            MOV    L,B            
            MOV    H,D            
            JNZ    L_012116       
            ORA    A              
            JP     L_012125       
L_012116:   MVI    M,314          
            JMP    L_012125       

L_012123:   MVI    M,302          
L_012125:   DCR    L              
            DCR    C              
            JNZ    L_012123       
L_012132:   MVI    L,333          
            MVI    H,077          
            MOV    B,M            
            INR    L              
            MOV    A,M            
            MOV    L,E            
            MOV    H,D            
            DCR    L              
            MOV    M,A            
            DCR    L              
            INR    B              
            MOV    M,B            
            RST    010Q           
            MVI    L,355          
            MVI    H,077          
            MOV    C,M            
            INR    L              
            MOV    B,M            
            MVI    L,325           ; <----PRCHAR
            MOV    E,M            
            MVI    L,366          
            MOV    A,M            
            ORA    A              
            JZ     L_012174       
            MVI    E,040          
L_012174:   MVI    L,357          
            CALL   MOV_HL_M       
            MVI    A,177          
            OUT    33Q             ; Output Cursor Y Position (and Display on/DMA on)
            MOV    M,E            
            DCR    L              
            JMP    L_030314       

L_012211:   SUI    001            
            MVI    L,366          
            MOV    M,A            
            RNZ                   
            MVI    L,325           ; <----PRCHAR
            MOV    A,M            
            MOV    L,E            
            MOV    H,D            
            MOV    M,A            
            MVI    H,077          
            RET                   

L_012230:   RST    010Q           
            INR    L              
            MOV    D,M            
            MOV    L,E            
            MOV    H,D            
            MVI    M,314          
            JMP    L_012132       

L_012242:   CALL   L_014164       
            RZ                    
            MOV    E,A            
            MOV    D,B            
            ORI    017            
            MOV    L,A            
            MVI    M,314          
            CALL   L_033011       
            JMP    L_012132       

L_012263:   MVI    L,321           ; <----CURXPOS
            MVI    H,077          
            MOV    C,M            
L_012270:   RST    010Q           
            MVI    L,320           ; <----CURYPOS
            MOV    A,M            
            MVI    L,317          
            MOV    M,C            
            MVI    L,331          
            SUB    M              
            JZ     L_013133       
            CALL   L_030214       
            MOV    E,A            
            ORA    A              
            JP     L_012347       
L_012315:   RST    010Q           
            INR    L              
            INR    L              
            CALL   MOV_HL_M       
            INR    E              
            JNZ    L_012315       
L_012327:   MOV    E,L            
            MOV    D,H            
            CALL   L_032321       
            JMP    L_013164       

L_012337:   CALL   MOV_HL_M       
            INR    L              
            DCR    E              
            JZ     L_012327       
L_012347:   RST    010Q           
            MOV    A,M            
            ORA    A              
            JNZ    L_012337       
            CALL   L_032127       
            JNZ    L_013006       
            MVI    L,365          
            MOV    A,M            
            ORA    A              
            JZ     L_013011       
            MVI    L,266           ; <----FLAGS
            MOV    A,M            
            ANI    020            
            JZ     L_013006       
            MVI    L,326          
            MVI    M,260          
L_013006:   ORI    377            
            RET                   

L_013011:   MOV    M,E            
L_013012:   CALL   L_014164       
            JZ     L_013006       
            CALL   L_036042       
            MOV    D,H            
            MOV    E,L            
            MVI    L,266           ; <----FLAGS
            MVI    H,077          
            MOV    A,M            
            ANI    020            
            MVI    L,326          
            JZ     L_013054       
            MVI    M,260          
            MOV    H,D            
            MOV    L,E            
            MOV    C,L            
            DCR    L              
            MVI    M,260          
            JMP    L_013060       

L_013054:   MOV    M,A            
            MOV    H,D            
            MOV    L,E            
            MOV    C,L            
L_013060:   DCR    L              
            MVI    M,314          
            MVI    L,337          
            CALL   L_032357       
            MOV    M,B            
            DCR    L              
            MOV    M,C            
            MOV    L,C            
            MOV    H,B            
            INR    L              
            INR    L              
            MOV    M,E            
            INR    L              
            MOV    M,D            
            MOV    L,E            
            MOV    H,D            
            INR    L              
            CALL   L_030313       
            MVI    L,365          
            MOV    E,M            
            DCR    E              
            MOV    M,E            
            JNZ    L_013012       
            MVI    L,317          
            MOV    C,M            
            MVI    L,337          
            CALL   MOV_HL_M       
            JMP    L_012327       

L_013133:   MOV    A,C            
            INR    L              
            MOV    B,M            
            SUB    B              
            JP     L_013153       
            CALL   L_032326       
            CALL   L_032343       
            JMP    L_013164       

L_013153:   MOV    C,A            
            CALL   L_032350       
            INR    B              
            JNZ    L_013165       
            DCR    C              
L_013164:   DCR    E              
L_013165:   RST    010Q           
            CALL   L_013307       
            RST    010Q           
            CALL   L_032365       
            MVI    L,320           ; <----CURYPOS
            MOV    B,M            
            MVI    L,317          
            MOV    A,M            
            DCR    C              
            JM     L_013210       
            SUB    C              
L_013210:   MVI    L,331          
            MOV    M,B            
            INR    L              
            MOV    M,A            
            INR    C              
            RET                   

L_013217:   MVI    L,321           ; <----CURXPOS
            MOV    C,M            
            DCR    C              
            CALL   L_031364       
            RNZ                   
            MOV    C,A            
            INR    C              
            CALL   L_030233       
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            MOV    B,A            
            RZ                    
            CALL   L_033107       
            JNZ    L_013272       
            MVI    B,300          
            CALL   L_023227       
            JZ     L_013272       
            CALL   L_032350       
L_013262:   CALL   L_030233       
            MVI    B,000          
            JMP    L_013302       

L_013272:   CALL   L_023225       
            JZ     L_013262       
            MVI    B,001          
L_013302:   MVI    L,321           ; <----CURXPOS
            MOV    C,M            
            XRA    A              
            RET                   

L_013307:   MVI    L,353          
            MVI    M,000          
            MOV    L,E            
            MOV    H,D            
            INR    L              
            INR    C              
            JZ     L_013335       
L_013322:   DCR    L              
L_013323:   RST    010Q           
            MOV    A,M            
            ADD    A              
            JC     L_013342       
            DCR    C              
            JNZ    L_013322       
L_013335:   MOV    E,L            
            MOV    D,H            
            MVI    H,077          
            RET                   

L_013342:   MOV    A,M            
            MOV    E,L            
            MOV    D,H            
            JM     L_013360       
            MVI    L,326          
            MVI    H,077          
            MOV    M,A            
            JMP    L_014014       

L_013360:   CPI    320            
            JC     L_013373       
            CALL   L_030225       
            JMP    L_013323       

L_013373:   MOV    B,A            
            MVI    H,077          
            CPI    314            
            JZ     L_014052       
            CPI    316            
            RZ                    
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JNZ    L_014021       
L_014014:   MOV    L,E            
            MOV    H,D            
            JMP    L_013322       

L_014021:   MOV    A,B            
            SUI    301            
            JZ     L_014036       
            SUI    377            
            JNZ    L_014014       
            ADI    001            
L_014036:   MVI    L,254          
            MOV    M,A            
            MOV    A,B            
            MVI    L,253          
            SUB    M              
            JNZ    L_014014       
            MOV    M,A            
            RET                   

L_014052:   MVI    L,354          
            MOV    A,M            
            ORA    A              
            CZ     L_033107       
            RNZ                   
L_014062:   MOV    A,E            
            ANI    017            
            SUI    002            
            RZ                    
            CMP    C              
            JM     L_014075       
            MOV    A,C            
L_014075:   MOV    B,A            
            MOV    L,E            
            MOV    H,D            
L_014100:   MVI    M,040          
            DCR    L              
            DCR    C              
            DCR    B              
            JNZ    L_014100       
            MVI    M,314          
            MOV    E,L            
            DCR    C              
            JP     L_014132       
            INR    E              
            MOV    B,L            
            MVI    H,077          
            MOV    C,A            
            CALL   L_014142       
            MOV    A,C            
            MVI    C,377          
L_014132:   INR    C              
            MVI    L,353          
            MVI    H,077          
            MVI    M,001          
            RET                   

L_014142:   MVI    L,321           ; <----CURXPOS
            MOV    A,M            
            CPI    117            
            MVI    L,325           ; <----PRCHAR
            MOV    A,M            
            MOV    L,B            
            MOV    H,D            
            MVI    M,314          
            RNZ                   
            ORA    A              
            RM                    
            MVI    M,302          
            RET                   

            NEWPAGE
; Allocation routine?
L_014164:   MVI    C,302          
L_014166:   MVI    L,352          
            MVI    H,077          
            MOV    M,C            
            RST    010Q           
            MVI    L,371           ; <----FREELIST
            MOV    A,M            
            ORA    A              
            JZ     L_015004       
            INR    L              
            MOV    D,M            
            MOV    E,A            
L_014206:   INR    E              
            INR    E              
            ANI    360            
            MOV    L,A            
            MOV    H,D            
            MOV    A,M            
            MOV    C,A            
            ORA    A              
            JZ     L_014230       
            CALL   L_032230       
            JZ     L_014247       
L_014230:   RST    010Q           
            MOV    L,E            
            MOV    B,D            
            MOV    A,E            
            MOV    D,M            
            DCR    L              
            MOV    E,M            
            MVI    L,371           ; <----FREELIST
            CALL   L_032374       
            JMP    L_014272       

L_014247:   RST    010Q           
            INR    L              
            MOV    B,M            
            MOV    E,L            
            MOV    A,C            
            ANI    360            
            MOV    L,A            
            MOV    H,B            
            MOV    A,M            
            INR    L              
            MOV    H,M            
            MOV    L,E            
            MOV    E,H            
            MOV    H,D            
            MOV    M,E            
            DCR    L              
            MOV    M,A            
            MOV    A,C            
L_014272:   MVI    L,352          
            MVI    H,077          
            MOV    E,M            
            ORI    017            
            MOV    L,A            
            MOV    H,B            
            MVI    C,015          
L_014305:   MOV    M,E            
            DCR    L              
            DCR    C              
            JNZ    L_014305       
            MOV    M,E            
            RST    010Q           
            MOV    A,L            
            MOV    B,H            
            ORA    A              
            RET                   

            NEWPAGE
L_014321:   MVI    H,077          
            MVI    A,040          
            CALL   SETFLAGS        ; Set 'Flags' indicated by A
            MVI    L,256           ; <----BLINKTMR
            MOV    A,M            
            ORA    A              
            JNZ    L_014344       
            MVI    M,001          
            CALL   BEEP           
L_014344:   MVI    L,365          
            XRA    A              
            MOV    M,A            
            RET                   

L_014351:   MVI    L,255          
            MVI    H,077          
            MOV    A,M            
            ORA    A              
            RZ                    
            MOV    C,A            
            CALL   L_030223       
L_014364:   RST    010Q           
            MOV    A,M            
            ORA    A              
            RZ                    
            INR    L              
            MOV    H,M            
            MOV    L,A            
            INR    L              
            DCR    C              
            JNZ    L_014364       
            MOV    E,L            
            MOV    D,H            
            ORA    H              
            RET                   

L_015004:   RST    010Q           
            MVI    L,266           ; <----FLAGS
            MOV    A,M            
            ANI    040            
            XRI    040            
            MVI    L,255          
            ORA    M              
            JZ     L_014321       
            MVI    L,365          
            MOV    A,M            
            ORA    A              
            JNZ    L_015100       
            CALL   L_030214       
            MOV    A,M            
            ORA    A              
            MVI    H,077          
            JZ     L_015100       
            MVI    L,337          
            CALL   MOV_HL_M       
            INR    L              
            INR    L              
            MOV    E,M            
            INR    L              
            MOV    D,M            
            MVI    L,337          
            CALL   L_032374       
            MOV    L,E            
            MOV    H,D            
            INR    L              
            MOV    D,M            
            MVI    M,316          
            DCR    L              
            MOV    E,M            
            INR    E              
            MVI    M,000          
            JMP    L_015173       

L_015100:   CALL   L_030223       
            INR    L              
            INR    L              
            MOV    A,M            
            ORA    A              
            JNZ    L_015137       
            MVI    L,320           ; <----CURYPOS
            MVI    H,077          
            ORA    M              
            CNZ    E_ROLLUP       
            JZ     L_014321       
            MVI    L,320           ; <----CURYPOS
            MVI    H,077          
            MOV    B,M            
            DCR    B              
            JM     L_015137       
            MOV    M,B            
L_015137:   CALL   L_027052       
            DCR    B              
            MOV    M,B            
            MVI    L,335          
            CALL   MOV_HL_M       
            MOV    E,M            
            INR    L              
            MOV    D,M            
            INR    E              
            MVI    L,335          
            CALL   L_032374       
            MOV    L,E            
            MOV    H,D            
            INR    L              
            INR    L              
            MOV    E,M            
            MVI    M,000          
            INR    L              
            MOV    D,M            
L_015173:   CALL   L_022056       
            MOV    A,E            
            JMP    L_014206       


            NEWPAGE
L_015202:   MVI    L,325           ; <----PRCHAR
            MOV    M,B            
PRINTPRCHAR:
            CALL   L_010302       
C_PRINTABLE:
            CALL   L_015321       
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            RZ                    
            MVI    L,321           ; <----CURXPOS
            MOV    A,M            
            ORA    A              
            JNZ    L_015263       
            CALL   L_032343       
            DCR    E              
            MVI    L,326          
            MOV    A,M            
            MVI    L,367          
            MOV    M,A            
            CALL   L_025033       
            MVI    L,367          
            MOV    A,M            
            MVI    L,326          
            MOV    M,A            
            DCR    B              
            JNZ    L_015275       
            MVI    L,061          
            MOV    M,B            
            JMP    L_033117       

L_015263:   CALL   L_032350       
            DCR    E              
            MVI    B,300          
            CALL   L_023227       
            RNZ                   
L_015275:   CALL   L_037260       
            CZ     BEEP           
            CALL   L_033117        ; --- Clear 077204
            CALL   L_023225       
            RZ                    
            CALL   L_024125       
            RNZ                   
            JMP    E_HOMECURUP    

L_015321:   RST    010Q           
            MVI    L,321           ; <----CURXPOS
            MVI    H,077          
            MOV    A,M            
            CPI    117            
            JZ     L_015360       
            ADI    001            
            MOV    M,A            
            CPI    117            
            RZ                    
            MVI    L,240           ; <----KBDLEDS
            MOV    A,M            
            ANI    002            
            CZ     L_033107       
            RNZ                   
            MVI    L,204          
            MVI    M,001          
            RET                   

L_015360:   CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JNZ    CRLF           
            MVI    L,KEYOPTAH & 377Q ; Let HL point to Keyboard options A-H register
            MOV    A,M             ; Get Keyboard Options
            ANI    004             ; Mask B2 = End of line wrap.
            RNZ                    ; Return if 1 = Do not wrap when line full, stay at col 80
CRLF:       CALL   C_CR           
            JMP    C_LF           

E_MEMLCKOFF:
            MVI    A,337          
            CALL   CLRFLAGS        ; Clear 'Flags' indicated by ~A
            MVI    L,256           ; <----BLINKTMR
            XRA    A              
            MOV    M,A            
            DCR    L              
            MOV    M,A            
            JMP    L_007372       

L_016020:   MVI    L,KEYOPTAH & 377Q ; Let HL point to Keyboard options A-H register
            MOV    A,M             ; Get Keyboard Options
            ANI    004             ; Mask B2 = End of line wrap.
            RZ                     ; Return if 0 = Wrap when line full
C_LF:       MVI    L,262           ; <----UNKN1
            MVI    H,077          
            MVI    M,377          
            MVI    L,320           ; <----CURYPOS
            MOV    A,M            
            CPI    027            
            JZ     L_016052       
            ADI    001            
            MOV    M,A            
            JMP    L_016057       

L_016052:   CALL   E_ROLLUP       
            MVI    H,077          
L_016057:   MVI    L,267          
            MOV    A,M            
            ANI    020            
            RNZ                   
            MVI    C,377          
            JMP    L_012270       

E_ROLLUP:   RST    010Q           
            CALL   L_014351       
            JZ     L_016160       
            MOV    A,M            
            ORA    A              
            RZ                    
            CALL   L_027047       
            INR    B              
            MOV    M,B            
            MOV    A,E            
            MOV    B,D            
            MVI    L,341          
            CALL   L_032357       
            CALL   L_022235       
            MVI    L,255          
            MOV    A,M            
            MVI    L,331          
            CMP    M              
            JM     L_016207       
            RNZ                   
            MVI    M,000          
L_016137:   MVI    L,341          
L_016141:   MOV    E,M            
L_016142:   INR    L              
            MOV    D,M            
L_016144:   CALL   L_032321       
            DCR    E              
            CALL   L_032365       
            MVI    L,332          
            MVI    M,000          
            RET                   

L_016160:   MVI    L,255          
            MVI    H,077          
            MOV    A,M            
            ORA    A              
            RNZ                   
            CALL   L_030223       
            MOV    A,M            
            ORA    A              
            RZ                    
            MVI    D,001          
            ADD    D              
L_016200:   INR    L              
            MOV    B,M            
            CALL   L_030266       
            MVI    L,331          
L_016207:   MOV    C,M            
            DCR    C              
            JM     L_016137       
            MOV    M,C            
            ORA    H              
            RET                   

E_ROLLDOWN: RST    010Q           
            CALL   L_014351       
            JZ     L_016277       
            CALL   L_032372       
            CALL   L_030223       
            INR    L              
            INR    L              
            MOV    A,M            
            ORA    A              
            RZ                    
            MOV    E,A            
            INR    L              
            MOV    D,M            
            MOV    L,E            
            MOV    H,D            
            CALL   L_027047       
            DCR    B              
            MOV    M,B            
            MOV    A,E            
            MOV    B,D            
            CALL   L_032355       
            CALL   L_022235       
            MVI    L,255          
            MOV    A,M            
            SUI    001            
            MVI    L,331          
            CMP    M              
            JM     L_016320       
            RET                   

L_016277:   CALL   L_030221       
            INR    L              
            INR    L              
            MOV    A,M            
            INR    L              
            MOV    B,M            
            ORA    A              
            RZ                    
            MVI    D,377          
            CALL   L_030266       
            MVI    L,331          
L_016320:   MOV    A,M            
            ADI    001            
            CPI    030            
            JZ     L_016332       
            MOV    M,A            
            RET                   

L_016332:   CALL   L_030214       
            INR    L              
            INR    L              
            JMP    L_016141       


            NEWPAGE
; Sets state ESC - Next character parsed as first of an escape sequence

STESC:      CALL   L_037260       
            CNZ    QRYREMOTE      
            MVI    A,010          
            CC     SETFLAGS       
            MVI    A,ESCTAB & 377Q
            MVI    B,ESCTAB >> 10Q
L_016361:   MVI    L,PARSETABPTR & 377Q ; <----PARSETABPTR
            MOV    M,A            
            INR    L              
            MOV    M,B            
R_016366:   MVI    L,271           ; <----ESCSEM
            MVI    M,002          
            RET                   

            NEWPAGE
L_016373:   MOV    E,M            
            INR    E              
            CALL   L_016142       
            RST    010Q           
            MVI    L,331          
            MOV    B,M            
            INR    B              
            MOV    M,B            
E_HOMECURDN:
            MVI    L,331          
            MOV    A,M            
            CPI    027            
            CZ     E_ROLLUP       
            MVI    H,077          
            CALL   L_030214       
            MOV    A,M            
            ORA    A              
            JNZ    L_016373       
            MVI    L,331          
            MVI    H,077          
            MOV    B,M            
            MVI    L,320           ; <----CURYPOS
            MOV    M,B            
            JMP    E_HOMELINE     

E_CURRIGHT: MVI    A,001          
            JMP    L_017052       

E_CURLEFT:  MVI    A,377          
L_017052:   MVI    L,321           ; <----CURXPOS
            ADD    M              
            MOV    M,A            
            JM     L_017072       
            SUI    120            
            RNZ                   
            MOV    M,A            
E_CURDOWN:  MVI    A,001          
            JMP    L_017076       

L_017072:   MVI    M,117          
E_CURUP:    MVI    A,027          
L_017076:   MVI    L,320           ; <----CURYPOS
            ADD    M              
            MOV    M,A            
            SUI    030            
            RM                    
            MOV    M,A            
            RET                   

; Check if remote mode
QRYREMOTE:  MVI    L,223           ; Let HL point to Keyboard Column 10 state
            MOV    A,M             ; Get Column status
            RRC                    ; Rotate 'Remot' button state into carry
            RET                   

E_NEXTPAGE: MVI    A,030          
            MVI    L,255          
            SUB    M              
            MOV    C,A            
            CALL   L_017145       
L_017125:   MVI    L,255          
            MOV    A,M            
            MVI    L,320           ; <----CURYPOS
            MOV    M,A            
            INR    L              
            MVI    M,000          
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JNZ    L_024125       
            RET                   

L_017145:   MVI    L,311          
            MOV    M,C            
            INR    L              
L_017151:   MOV    M,C            
            CALL   E_ROLLUP       
            MVI    L,311          
            MVI    H,077          
            MOV    C,M            
            JZ     L_017174       
            DCR    C              
            JNZ    L_017151       
            INR    L              
            MOV    C,M            
            RET                   

L_017174:   INR    L              
            MOV    A,M            
            SUB    C              
            MOV    C,A            
            RET                   

E_PREVPAGE: MVI    A,350          
            MVI    L,255          
            ADD    M              
            CALL   L_017216       
            JMP    L_017125       

L_017214:   MVI    M,000          
L_017216:   MOV    C,A            
            MVI    L,311          
L_017221:   MOV    M,C            
            CALL   E_ROLLDOWN     
            MVI    L,311          
            MVI    H,077          
            RZ                    
            MOV    C,M            
            INR    C              
            JNZ    L_017221       
            RET                   

E_CLRLINE:  MVI    H,077          
            CALL   L_013217       
            RNZ                   
            CALL   L_007337       
            MVI    A,357          
            CNZ    CLRFLAGS       
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JNZ    L_025066       
            MVI    A,314          
L_017266:   MVI    L,352          
            MOV    M,A            
            MVI    L,333          
            MOV    B,M            
            INR    L              
            MOV    C,M            
            MOV    A,E            
            ANI    360            
            MOV    L,A            
            MOV    H,D            
            MOV    A,M            
            INR    B              
            MOV    M,B            
            INR    L              
            MOV    B,M            
            MOV    M,C            
            MOV    C,L            
            MVI    L,361          
            CALL   MOV_M_AB        ; Move content of register pair AB into memory at HL
            RST    010Q           
            MOV    A,E            
            ANI    017            
            SUI    002            
            JM     L_017346       
            MOV    B,A            
            MVI    L,352          
            MOV    E,M            
            MOV    L,C            
            MOV    H,D            
L_017336:   INR    L              
            DCR    B              
            MVI    M,302          
            JP     L_017336       
            MOV    M,E            
L_017346:   MVI    L,361          
            MVI    H,077          
            CALL   L_032227       
            JNZ    L_017371       
            MOV    A,M            
            SUI    003            
            MOV    E,A            
            INR    L              
            MOV    D,M            
            CALL   L_022056       
L_017371:   XRA    A              
            RET                   

            NEWPAGE
            DB     000            
            DB     000            
            DB     335             ; CRC 0
            DB     054             ; CRC 1
            DB     265             ; ROM Checksum

            NEWPAGE
E_CLRMEM:   CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JNZ    L_025234       
            CALL   E_CLRLINE      
            ORA    A              
            RM                    
            CALL   L_032343       
            MOV    L,E            
            MOV    H,D            
            MOV    A,M            
            ORA    A              
            RZ                    
            INR    L              
            MOV    B,M            
            MVI    M,316          
            DCR    L              
            MVI    M,000          
            MVI    L,371           ; <----FREELIST
            MVI    H,077          
            MOV    C,M            
            MOV    M,A            
            INR    L              
            MOV    A,M            
            MOV    M,B            
            MVI    L,337          
            MOV    B,M            
            MOV    M,E            
            INR    L              
            MOV    E,M            
            MOV    M,D            
            MOV    L,B            
            MOV    H,E            
            MOV    M,C            
            INR    L              
            MOV    M,A            
L_020057:   MVI    L,266           ; <----FLAGS
            MVI    H,077          
            MOV    A,M            
            ANI    040            
            RZ                    
            MVI    L,256           ; <----BLINKTMR
            MVI    M,000          
            JMP    L_020132       

C_CR:       MVI    L,KEYOPTAH & 377Q ; Let HL point to Keyboard options A-H register
            MOV    A,M             ; Get Keyboard Options
            ANI    002             ; Mask B1 = Space Overwrite Latch
            JZ     E_HOMELINE      ; Jump if 0, space overwrite latch disabled
            MVI    L,262           ; <----UNKN1
            MVI    M,040          
E_HOMELINE: MVI    L,321           ; Let HL point to Cursor X position
            XRA    A               ; Clear A
            MOV    M,A             ; Set Cursor X position to 0
            RET                   

E_MEMLCKON: MVI    A,040          
            CALL   SETFLAGS        ; Set 'Flags' indicated by A
            MVI    L,320           ; <----CURYPOS
            MOV    A,M            
            MVI    L,255          
            MOV    M,A            
L_020132:   JMP    L_007367       

L_020135:   CALL   L_033107       
            JNZ    L_011221       
L_020143:   MVI    L,325           ; <----PRCHAR
            MOV    B,M            
L_020146:   MOV    L,E            
            MOV    H,D            
L_020150:   MOV    A,B            
            ORA    A              
            JM     L_020156       
            INR    C              
L_020156:   MOV    E,B            
            RST    010Q           
            MOV    B,E            
            MOV    A,M            
            MOV    M,B            
            MOV    B,A            
            DCR    L              
            MOV    A,C            
            CPI    120            
            JZ     L_021054       
L_020173:   MOV    A,M            
            RLC                   
            ANA    M              
            JP     L_020150       
            MOV    A,M            
            CPI    320            
            JNC    L_020367       
            CPI    314            
            JZ     L_020315       
            CPI    302            
            JZ     L_020330       
            CPI    300            
            JNZ    L_020150       
            MOV    E,L            
            MOV    D,H            
            CALL   L_032127       
            JZ     L_020146       
            MVI    L,325           ; <----PRCHAR
            MOV    A,M            
            ORA    A              
            JM     L_020146       
            MOV    A,B            
            ORA    A              
            JP     L_020262       
L_020252:   CALL   L_020274       
            JM     L_020252       
            MVI    M,200          
L_020262:   CALL   L_020274       
            JM     L_020262       
            DCR    E              
            JMP    L_025070       

L_020274:   INR    E              
            MOV    A,E            
            ANI    017            
            JNZ    L_020310       
            CALL   L_032355       
            INR    E              
            INR    E              
L_020310:   MOV    L,E            
            MOV    H,D            
            MOV    A,M            
            ORA    A              
            RET                   

L_020315:   MOV    A,B            
            ORA    A              
            JM     L_020332       
            MOV    A,C            
            CPI    117            
            JNZ    L_020333       
L_020330:   MOV    M,B            
            RET                   

L_020332:   DCR    C              
L_020333:   MOV    E,L            
            MOV    D,H            
L_020335:   MVI    L,325           ; <----PRCHAR
            MVI    H,077          
            MOV    M,B            
            MVI    L,321           ; <----CURXPOS
            MOV    B,M            
            MOV    M,C            
            MVI    L,317          
            MOV    M,B            
            MVI    C,000          
            CALL   L_011325       
            MVI    L,317          
            MVI    H,077          
            MOV    E,M            
            MVI    L,321           ; <----CURXPOS
            MOV    M,E            
            RET                   

L_020367:   CALL   L_032226       
            JNZ    L_021012       
            MOV    E,L            
            MOV    D,H            
            CALL   L_032372       
            MOV    L,E            
            MOV    H,D            
            CALL   MOV_HL_M       
            JMP    L_020173       

L_021012:   INR    L              
            INR    L              
            MOV    E,L            
            MOV    D,H            
            MOV    A,M            
            MVI    L,367          
            MVI    H,077          
            MOV    M,B            
            MOV    B,A            
            DCR    C              
            CALL   L_020335       
            ORA    A              
            JZ     L_021044       
            MVI    L,367          
            MOV    A,M            
            MOV    L,C            
            MOV    H,B            
            MOV    M,A            
            RET                   

L_021044:   MVI    L,357          
            CALL   MOV_HL_M       
            MVI    M,314          
            RET                   

L_021054:   MOV    E,L            
            MOV    D,H            
            MOV    A,M            
            CPI    302            
            RZ                    
            CPI    320            
            JC     L_021073       
            CALL   L_032226       
            RNZ                   
L_021073:   MVI    H,077          
            MVI    A,302          
            JMP    L_017266       

E_DELCHAR:  MVI    L,330          
            MVI    M,000          
L_021106:   MVI    L,363          
            MVI    M,000          
            CALL   L_021167       
            CALL   L_033002       
            JM     L_021106       
            MVI    L,330          
            MOV    A,M            
            ORA    A              
            RZ                    
            CALL   L_032355       
            INR    E              
L_021134:   CALL   L_030233       
            RNZ                   
            CPI    314            
            RZ                    
            ADD    A              
            JC     L_021163       
            MVI    L,276          
            MVI    M,000          
            MVI    L,330          
            MOV    A,M            
            MVI    L,325           ; <----PRCHAR
            JMP    L_010301       

L_021163:   JM     L_021134       
            RET                   

L_021167:   CALL   L_013217       
            RNZ                   
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JZ     L_021203       
            DCR    B              
            RZ                    
L_021203:   MOV    L,E            
            MOV    H,D            
            MOV    A,M            
            CPI    314            
            RZ                    
            MVI    L,363          
            MVI    H,077          
            MOV    M,A            
            MOV    B,A            
            ADD    A              
            JNC    L_021234       
            JM     L_021234       
            MVI    L,330          
            MOV    M,B            
            CALL   L_032372       
L_021234:   MOV    L,E            
            MOV    H,D            
L_021236:   DCR    L              
L_021237:   MOV    D,H            
            RST    010Q           
            MOV    A,M            
            MOV    B,A            
            ADD    A              
            JC     L_021256       
            INR    C              
L_021250:   INR    L              
            MOV    M,B            
            DCR    L              
            JMP    L_021236       

L_021256:   JP     L_021250       
            MOV    A,B            
            CPI    320            
            JNC    L_021330       
            CPI    314            
            JZ     L_022131       
            CPI    302            
            JZ     L_022101       
            CPI    300            
            JNZ    L_021250       
            MOV    E,L            
            CALL   L_033002       
            JM     L_021323       
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JNZ    L_022122       
L_021323:   MOV    L,E            
            MOV    H,D            
            JMP    L_021250       

L_021330:   RST    010Q           
            MOV    E,L            
            CALL   L_032226       
            JNZ    L_022102       
            MOV    A,M            
            INR    L              
            MOV    H,M            
            MOV    L,A            
            MOV    B,M            
            CALL   L_033002       
            JM     L_021367       
            MOV    A,B            
            CPI    300            
            JNZ    L_021367       
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JNZ    L_022122       
L_021367:   MOV    A,B            
            ORA    A              
            JM     L_021375       
            INR    C              
L_021375:   MOV    L,E            
            MOV    H,D            
            INR    L              
            MOV    M,B            
            MVI    L,363          
            MVI    H,077          
            MOV    B,M            
            MOV    L,E            
            MOV    H,D            
            MOV    A,M            
            DCR    L              
            MOV    L,M            
            MOV    H,A            
            MOV    A,M            
            CPI    314            
            JZ     L_022036       
            DCR    L              
            MOV    A,B            
            ORA    A              
            JP     L_021237       
            MOV    A,M            
            CPI    302            
            JNZ    L_021237       
L_022036:   MOV    A,L            
            ANI    360            
            MOV    L,A            
            ADI    014            
            MOV    C,M            
            INR    L              
            MOV    B,M            
            MOV    L,E            
            MOV    H,D            
            MOV    E,A            
            MOV    D,M            
            CALL   L_030314       
L_022056:   RST    010Q           
            DCR    E              
            CALL   L_020057       
            MVI    L,371           ; <----FREELIST
            MOV    B,M            
            MOV    M,E            
            INR    L              
            MOV    C,M            
            MOV    M,D            
            MOV    L,E            
            MOV    H,D            
            INR    L              
            MOV    M,B            
            INR    L              
            MOV    M,C            
            RET                   

L_022101:   MOV    E,L            
L_022102:   MVI    B,302          
            CALL   L_033002       
            JM     L_022124       
            MVI    B,314          
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JZ     L_022124       
L_022122:   MVI    B,040          
L_022124:   MOV    L,E            
            MOV    H,D            
            INR    L              
            MOV    M,B            
            RET                   

L_022131:   MVI    M,302          
            INR    L              
            MVI    M,314          
            RET                   

E_INSLINE:  CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            CZ     L_031355       
            RNZ                   
            CALL   L_014164       
            RZ                    
            ADI    013            
            DCR    L              
            MOV    M,H            
            DCR    L              
            MOV    M,A            
            SUI    002            
            MOV    E,A            
            MOV    D,H            
            MVI    H,077          
            CALL   L_007337       
            MOV    L,E            
            MOV    H,D            
            JNZ    L_022201       
            MVI    M,260          
            DCR    L              
L_022201:   MVI    M,314          
            MOV    A,E            
            MVI    L,322          
            CALL   MOV_M_AB        ; Move content of register pair AB into memory at HL
            ADI    001            
            MVI    L,333          
            MOV    E,M            
            MOV    M,A            
            INR    L              
            MOV    D,M            
            MOV    M,B            
            CALL   L_022235       
            MVI    L,320           ; <----CURYPOS
            MOV    A,M            
            ORA    A              
            RNZ                   
            JMP    L_030302       

L_022235:   MOV    L,E            
            MOV    H,D            
            INR    L              
            INR    L              
            MOV    C,M            
            MOV    M,A            
            INR    L              
            MOV    D,M            
            MOV    M,B            
            MOV    L,A            
            MOV    A,H            
            MOV    H,B            
            DCR    E              
            MOV    M,E            
            INR    L              
            MOV    M,A            
            INR    L              
            MOV    M,C            
            INR    L              
            MOV    M,D            
            MOV    A,C            
            ORA    A              
            MOV    A,L            
            JZ     L_022302       
            SUI    004            
            MOV    L,C            
            MOV    H,D            
            MOV    C,A            
            INR    L              
            CALL   L_030314       
            INR    C              
            RET                   

L_022302:   SUI    003            
            MOV    C,A            
            MVI    L,335          
            JMP    MOV_M_AB       

E_DELLINE:  CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            CZ     L_031355       
            RNZ                   
            CALL   L_030214       
            MOV    E,L            
            MOV    D,H            
            MOV    A,M            
            ORA    A              
            JZ     E_CLRLINE      
            CALL   L_022376       
            MOV    A,L            
            MOV    B,H            
            SUI    003            
            MVI    L,333          
            CALL   MOV_M_AB        ; Move content of register pair AB into memory at HL
            MOV    C,A            
            MVI    L,322          
            SUI    001            
            MOV    M,A            
            INR    L              
            MOV    M,B            
            MVI    L,326          
            MVI    M,000          
            MVI    L,320           ; <----CURYPOS
            MOV    A,M            
            ORA    A              
            MOV    A,C            
            CZ     L_030302       
            JMP    L_022056       

L_022376:   MOV    A,M            
            INR    L              
            MOV    B,M            
            MOV    C,A            
            INR    L              
            MOV    A,M            
            INR    L              
            MOV    H,M            
            ORA    A              
            JNZ    L_023025       
            MVI    L,335          
            MVI    H,077          
            INR    C              
            MOV    M,C            
            INR    L              
            MOV    M,B            
            JMP    L_023035       

L_023025:   MOV    L,A            
            INR    L              
            CALL   L_030314       
            MOV    H,A            
            INR    C              
            MOV    A,L            
L_023035:   MOV    L,C            
            MOV    C,H            
            MOV    H,B            
            INR    L              
            INR    L              
            MOV    M,A            
            INR    L              
            MOV    M,C            
            RET                   

E_SETTAB:   CALL   L_023064       
            ORA    M              
            MOV    M,A            
            RET                   

E_CLEARTAB: CALL   L_023064       
            XRI    377            
            ANA    M              
            MOV    M,A            
            RET                   

L_023064:   MVI    L,321           ; <----CURXPOS
            MOV    A,M            
            MOV    B,A            
L_023070:   ANI    370            
            RRC                   
            RRC                   
            RRC                   
            ADI    277            
            MOV    L,A            
            MOV    A,B            
            ANI    007            
            MOV    B,A            
            INR    B              
            MVI    A,200          
L_023107:   RLC                   
            DCR    B              
            JNZ    L_023107       
            RET                   

C_HT:       MVI    L,262           ; <----UNKN1
            MVI    M,377          
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JNZ    L_023216       
            MVI    L,321           ; <----CURXPOS
            MOV    B,M            
            INR    B              
            MVI    A,117          
            SUB    B              
            JM     CRLF           
            ORI    007            
            MOV    C,A            
            MOV    A,B            
            CALL   L_023070       
            SUI    001            
            XRI    377            
            ANA    M              
L_023155:   MVI    B,010          
            JZ     L_023172       
L_023162:   RRC                   
            JC     L_023207       
            DCR    B              
            JNZ    L_023162       
L_023172:   MOV    A,C            
            SUI    010            
            JM     CRLF           
            MOV    C,A            
            INR    L              
            MOV    A,M            
            ORA    A              
            JMP    L_023155       

L_023207:   MVI    A,127          
            SUB    C              
            SUB    B              
            JMP    L_033270       

L_023216:   CALL   L_024125       
            RNZ                   
            JMP    E_HOMECURUP    

L_023225:   MVI    B,301          
L_023227:   MVI    L,254          
            MOV    A,M            
            MVI    L,367          
            MOV    M,A            
            MVI    C,000          
            CALL   L_023257       
            MVI    L,367          
            MOV    B,M            
            MVI    L,254          
            MOV    M,B            
            RET                   

L_023251:   MVI    B,300          
L_023253:   MVI    A,117          
            SUB    C              
            MOV    C,A            
L_023257:   MVI    L,253          
            MOV    M,B            
            MVI    L,354          
            MVI    M,001          
            CALL   L_013307       
            MVI    L,354          
            XRA    A              
            MOV    M,A            
            MVI    L,253          
            ORA    M              
            MVI    M,000          
            RET                   

E_FMTMODE_ON:
            MVI    A,020           ; Format mode LED on
            CALL   LEDON          
E_HOMECURUP:
            RST    010Q           
            MVI    L,321           ; <----CURXPOS
            MVI    H,077          
            XRA    A              
            MOV    M,A            
            MVI    L,343          
            MOV    M,A            
            MVI    L,262           ; <----UNKN1
            MVI    M,377          
            CALL   L_014351       
            JZ     L_024064       
            MOV    A,D            
            INR    L              
            INR    L              
            MOV    C,M            
            INR    L              
            MOV    B,M            
            CALL   L_030221       
            MOV    D,A            
            INR    L              
            INR    L              
            MOV    A,M            
            ORA    A              
            JZ     L_024036       
            MVI    M,000          
            INR    L              
            MOV    H,M            
            MOV    L,A            
            DCR    E              
            MOV    M,E            
            INR    L              
            MOV    M,D            
            MOV    L,E            
            MOV    E,H            
            MOV    H,D            
            INR    L              
            INR    L              
            INR    L              
            MOV    M,A            
            INR    L              
            MOV    M,E            
            CALL   L_030221       
            MOV    E,H            
            MVI    L,335          
            MVI    H,077          
            MOV    A,M            
            MOV    M,D            
            INR    L              
            MOV    D,M            
            MOV    M,E            
            MOV    H,B            
            MOV    L,A            
            MOV    H,D            
            INR    L              
            INR    L              
            MOV    E,A            
            MOV    M,C            
            INR    L              
            MOV    M,B            
            MOV    H,B            
            MOV    L,C            
            INR    L              
            MOV    B,D            
            MOV    C,A            
            DCR    C              
            CALL   L_030314       
L_024036:   CALL   L_016144       
            RST    010Q           
            CALL   L_032127       
            MVI    A,000          
            JNZ    L_024071       
            MVI    L,255          
            MOV    A,M            
            MVI    L,320           ; <----CURYPOS
            MOV    M,A            
            MVI    L,331          
            MOV    M,A            
            RET                   

L_024064:   MVI    L,255          
            MVI    H,077          
            MOV    A,M            
L_024071:   MVI    L,320           ; <----CURYPOS
            MOV    M,A            
            MVI    L,331          
            XRA    A              
            MOV    M,A            
            MOV    D,A            
            MVI    L,335          
            MOV    A,M            
            CALL   L_016200       
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            RZ                    
L_024113:   MVI    L,314          
            MVI    M,000          
            MVI    L,321           ; <----CURXPOS
            MOV    C,M            
            JMP    L_024160       

L_024125:   MVI    L,314          
            MVI    M,000          
            CALL   L_031361       
            JM     L_030132       
            MOV    C,A            
            CALL   L_033107       
L_024143:   JNZ    L_024160       
            CALL   L_023251       
            JNZ    L_024170       
            MVI    A,120          
            SUB    C              
            MOV    C,A            
L_024160:   MVI    B,301          
            CALL   L_023253       
            JZ     L_024265       
L_024170:   MOV    C,A            
            MOV    A,E            
            ANI    360            
            MOV    L,A            
            MOV    H,D            
            CALL   MOV_HL_M       
            MOV    A,M            
            DCR    L              
            MOV    E,M            
            MVI    H,077          
            CPI    316            
            JZ     L_024260       
            MOV    D,A            
            CALL   L_032372       
            MVI    L,314          
            MOV    B,M            
            INR    B              
            MOV    M,B            
            MVI    L,326          
            XRA    A              
            MOV    M,A            
            MVI    L,367          
            MOV    M,A            
            MOV    A,C            
            MVI    C,000          
            CPI    301            
            JZ     L_024160       
            CALL   L_025033       
            MVI    L,367          
            MOV    A,M            
            MVI    L,326          
            MOV    M,A            
            DCR    B              
            JMP    L_024143       

L_024260:   MVI    L,332          
            MVI    M,120          
            RET                   

L_024265:   MVI    A,120          
            SUB    C              
            MVI    L,321           ; <----CURXPOS
            MOV    M,A            
            OUT    13Q             ; Output Cursor X Position
            MVI    L,332          
            MOV    M,A            
            CALL   L_032365       
            MVI    L,314          
            MOV    A,M            
            ORA    A              
            JZ     L_025020       
            MVI    L,320           ; <----CURYPOS
            ADD    M              
            CALL   L_032350       
            MVI    L,247           ; <----LDRPTR
            MOV    M,E            
            INR    L              
            MOV    M,D            
L_024324:   MVI    C,030          
            CMP    C              
            JC     L_024365       
            SUB    C              
            MVI    L,255          
            ADD    M              
            MOV    E,A            
            MOV    A,C            
            SUB    M              
            MVI    L,311          
            MOV    M,A            
            INR    L              
L_024345:   MOV    M,E            
            CALL   E_ROLLUP       
            MVI    L,311          
            MOV    E,M            
            DCR    E              
            JNZ    L_024345       
            INR    L              
            MOV    A,M            
            JMP    L_024324       

L_024365:   MVI    L,320           ; <----CURYPOS
            MOV    M,A            
            INR    L              
            MOV    B,M            
            MVI    L,331          
            MOV    M,A            
            INR    L              
            MOV    M,B            
            CALL   L_032355       
            INR    E              
            MVI    L,333          
            CALL   L_032374       
            MVI    L,247           ; <----LDRPTR
            MOV    E,M            
            INR    L              
            MOV    D,M            
            CALL   L_032365       
L_025020:   RST    010Q           
            ORA    H              
OUTCURY:    MVI    L,320           ; <----CURYPOS
            MOV    A,M            
            OUT    33Q             ; Output Cursor Y Position (and Display on/DMA on)
            RET                   

L_025027:   MVI    L,367          
            MOV    M,A            
            DCR    E              
L_025033:   INR    E              
            CALL   L_030233       
            JNZ    L_025033       
            MVI    B,000          
            MOV    L,A            
            ADD    A              
            RNC                   
            MOV    A,L            
            JP     L_025027       
            CPI    314            
            RZ                    
            CPI    300            
            RZ                    
            CPI    301            
            RNZ                   
            INR    B              
            RET                   

L_025066:   DCR    B              
            RZ                    
L_025070:   MOV    L,E            
            MOV    H,D            
L_025072:   RST    010Q           
            MOV    A,M            
            ADD    A              
            JC     L_025107       
            MVI    M,040          
            INR    C              
L_025103:   DCR    L              
            JMP    L_025072       

L_025107:   JM     L_025141       
L_025112:   MOV    E,L            
            MOV    D,H            
            CALL   L_032372       
            MVI    L,366          
            MOV    M,C            
            CALL   L_021203       
            CALL   L_032355       
            MVI    L,366          
            MOV    C,M            
            CALL   L_024260       
            JMP    L_025070       

L_025141:   MOV    A,M            
            CPI    320            
            JNC    L_025201       
            CPI    302            
            JZ     L_025103       
            CPI    316            
            RZ                    
            CPI    301            
            JZ     L_025112       
            CPI    300            
            RZ                    
            CPI    314            
            JNZ    L_025103       
            MOV    A,L            
            ANI    360            
            MOV    L,A            
            INR    L              
L_025201:   CALL   L_030225       
            MOV    A,L            
            CALL   L_032230       
            JZ     L_025072       
            MOV    A,M            
            CPI    316            
            RZ                    
            MOV    E,L            
            MOV    D,H            
            CALL   L_025033       
            MOV    L,E            
            MOV    H,D            
            DCR    B              
            JZ     L_025103       
            XRA    A              
            RET                   

L_025234:   CALL   L_013217       
            RM                    
            JNZ    L_025253       
            MOV    L,E            
            MOV    H,D            
            DCR    B              
            JM     L_025262       
            MVI    H,077          
L_025253:   CALL   L_024125       
            RZ                    
            MOV    L,E            
            MOV    H,D            
L_025261:   DCR    L              
L_025262:   CALL   L_025072       
            CPI    316            
            RZ                    
L_025270:   DCR    L              
L_025271:   RST    010Q           
            MOV    A,M            
            RLC                   
            ANA    M              
            JP     L_025270       
            MOV    A,M            
            CPI    316            
            RZ                    
            CPI    301            
            JZ     L_025261       
            CPI    320            
            JC     L_025270       
            CALL   L_030225       
            JMP    L_025271       

L_025324:   MVI    B,020          
            CALL   QRYREMOTE       ; Check if remote mode. Return with C=1 if remote active
            JC     L_025363       
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JNZ    L_025346       
L_025342:   CALL   C_CR           
            OUT    13Q             ; Output Cursor X Position
L_025346:   MVI    L,264           ; <----KEYOPTAH
            MOV    A,M            
            ANI    300            
            XRI    100            
            CNZ    P0C_DC1        
            JMP    L_031013       

L_025363:   CALL   L_031022       
            MVI    L,267          
            MOV    A,M            
            RRC                   
            RC                    
            CALL   L_032121       
            JNZ    E_HOMECURUP    
            JMP    L_025342       

L_026004:   MVI    L,003          
            MVI    H,000          
            MOV    A,M            
            MVI    L,274          
            MVI    H,077          
            MOV    M,A            
            CALL   L_013217       
            JM     L_027033       
            JZ     L_026046       
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JZ     L_026261       
            CALL   L_033107       
            JZ     L_026232       
            JMP    L_026052       

L_026046:   DCR    B              
            JNZ    L_026060       
L_026052:   CALL   L_024113       
            JZ     L_027033       
L_026060:   CALL   P0_NOTHING2    
            MVI    L,326          
            MOV    A,M            
L_026066:   MVI    L,275          
            MOV    M,A            
L_026071:   INR    E              
L_026072:   CALL   L_030233       
            JNZ    L_026271       
            MOV    C,A            
            RLC                   
            MOV    A,C            
            JC     L_026146       
            CALL   L_037312       
            MVI    L,321           ; <----CURXPOS
            MOV    A,M            
            ADI    001            
            MOV    M,A            
            OUT    13Q             ; Output Cursor X Position
            MOV    A,C            
            CALL   L_007334       
            JNZ    L_026072       
            MOV    A,M            
            CPI    120            
            CZ     CRLF           
            CALL   QRYBLKMODE      ; Check if Block Mode. Return with Z=1 if in Block Mode
            JNZ    L_027023       
            JMP    L_027020       

L_026146:   CPI    314            
            JZ     L_026232       
            CPI    302            
            JZ     L_026072       
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JNZ    L_026174       
            CALL   L_027060       
            JMP    L_026072       

L_026174:   MOV    A,C            
            ADD    A              
            MOV    A,C            
            CP     P0_NOTHING1    
            MOV    A,C            
            CPI    300            
            JNZ    L_026072       
            CALL   QRYBLKMODE      ; Check if Block Mode. Return with Z=1 if in Block Mode
            JZ     L_027020       
L_026216:   CALL   L_024113       
            JZ     L_027020       
            CALL   L_007323       
            JMP    L_026071       

L_026232:   CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JZ     L_026261       
L_026240:   MVI    A,040          
            CALL   L_037312       
            MVI    L,321           ; <----CURXPOS
            MOV    A,M            
            ADI    001            
            MOV    M,A            
            OUT    13Q             ; Output Cursor X Position
            CPI    120            
            JNZ    L_026240       
L_026261:   MOV    A,E            
            ANI    360            
            MOV    L,A            
            MOV    H,D            
            MOV    E,M            
            INR    L              
            MOV    D,M            
L_026271:   CALL   L_032372       
            CALL   C_CR           
            OUT    13Q             ; Output Cursor X Position
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JNZ    L_026347       
            MVI    A,015          
            CALL   L_037312       
            CALL   QRYBLKMODE      ; Check if Block Mode. Return with Z=1 if in Block Mode
            JNZ    L_026344       
            MVI    L,217           ; <----COL04STATE
            MOV    A,M            
            RRC                    ; Rotate 'Auto-LF' into Carry
            JNC    L_027023       
            CALL   L_032247       
            CALL   C_LF           
            CALL   OUTCURY        
            JMP    L_027023       

L_026344:   CALL   L_032247       
L_026347:   CALL   C_LF           
            CALL   OUTCURY        
            MVI    L,361          
            CALL   MOV_HL_M       
            MOV    D,M            
            DCR    L              
            MOV    E,M            
            MVI    H,077          
            MOV    A,E            
            ORA    A              
            JZ     L_027020       
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            JZ     L_026066       
            CALL   L_025033       
            DCR    B              
            MOV    A,B            
            JZ     L_026066       
            CALL   QRYBLKMODE      ; Check if Block Mode. Return with Z=1 if in Block Mode
            JNZ    L_026216       
L_027020:   CALL   L_007320       
L_027023:   MVI    A,357          
            CALL   L_036201       
            JMP    L_033117       

L_027033:   CALL   L_007326       
            CALL   QRYBLKMODE      ; Check if Block Mode. Return with Z=1 if in Block Mode
            CZ     L_032235       
            JMP    L_027023       

L_027047:   CALL   L_022376       
L_027052:   MVI    L,343          
            MVI    H,077          
            MOV    B,M            
            RET                   

L_027060:   MOV    A,C            
            RLC                   
            RLC                   
            JC     L_027210       
            MVI    L,275          
            MOV    A,C            
            XRA    M              
            RZ                    
            ANI    017            
            JZ     L_027127       
            CALL   L_033044       
            MVI    A,046          
            CALL   L_037312       
            MVI    A,144          
            CALL   L_037312       
            MOV    A,C            
            ANI    017            
            ORI    100            
            CALL   L_037312       
            MVI    L,275          
L_027127:   MOV    A,C            
            XRA    M              
            ANI    060            
            MOV    M,C            
            RZ                    
            MOV    A,C            
            ANI    060            
            JZ     L_027203       
            MVI    L,274          
            CMP    M              
            JZ     L_027176       
            MOV    M,A            
            CALL   L_033044       
            MVI    A,051          
            CALL   L_037312       
            MOV    A,C            
            ANI    060            
            RRC                   
            RRC                   
            RRC                   
            RRC                   
            ADI    100            
            CALL   L_037312       
L_027176:   MVI    A,016          
            JMP    L_037312       

L_027203:   MVI    A,017          
            JMP    L_037312       

L_027210:   CALL   L_033044       
            MOV    A,C            
            CPI    301            
            MVI    A,133          
            JZ     L_037312       
            ADI    002            
            JMP    L_037312       


            NEWPAGE
; I guess this is some status reporting. It gets lots of things and
; seem to send it to the serial port after som formatting
L_027230:   MVI    A,375          
            CALL   L_036201       
            CALL   L_033044       
            MVI    A,134          
            CALL   L_037312       
L_027245:   MVI    L,263           ; <----RAMSIZE
            MOV    A,M            
            CALL   L_037304       
            MVI    L,264           ; <----KEYOPTAH
            MOV    A,M            
            CALL   L_037306       
            MVI    L,264           ; <----KEYOPTAH
            MOV    A,M            
            CALL   L_037302       
            CALL   QRYREMOTE       ; Check if remote mode. Return with C=1 if remote active
            MVI    L,217           ; <----COL04STATE
            MOV    A,M            
            RAL                   
            MOV    B,A            
            MVI    L,215           ; <----COL02STATE
            MOV    A,M            
            RRC                    ; Rotate 'Caps-Lock' into Carry
            MOV    A,B            
            RAL                   
            ORI    010            
            CALL   L_037306       
            MVI    L,267          
            MOV    A,M            
            ANI    034            
            CALL   L_037304       
            MVI    L,257          
            MOV    A,M            
            ANI    020            
            MOV    B,A            
            MVI    L,265           ; <----DISABBITS
            MOV    A,M            
            ANI    014            
            ORA    B              
            MOV    B,A            
            MVI    L,207           ; <----COMMSTATUS
            MOV    A,M            
            ANI    200            
            RLC                   
            RLC                   
            ORA    B              
            CALL   L_037305       
            MVI    L,265           ; Let HL point to DISABBITS
            MOV    A,M             ; Get Disable bits
            ANI    040             ; Mask bit 5 = Self test running flag
            JNZ    TEND            ; If Bit 5 set, jump back to selftest code
            MVI    L,207           ; <----COMMSTATUS
            MOV    A,M            
            ANI    177            
            MOV    M,A            
            JMP    L_007320       


            NEWPAGE
            DB     000            
            DB     000            
            DB     156             ; CRC 0
            DB     044             ; CRC 1
            DB     320             ; ROM Checksum

            NEWPAGE
; Parse table for cursor location sequence (Esc & a ...)
CURLOCTAB:  DB     053            
            DB     053            
            DW     E_CURLOCPL+200000
            DB     055            
            DB     055            
            DW     E_CURLOCMI+200000
            DB     060            
            DB     071            
            DW     E_CURLOCDGT+200000
            DB     103            
            DB     103            
            DW     E_CURLOCC+200000
            DB     122            
            DB     122            
            DW     E_CURLOCR+200000
            DB     131            
            DB     131            
            DW     E_CURLOCY+200000
            DB     143            
            DB     143            
            DW     E_CURLOCC+200000
            DB     162            
            DB     162            
            DW     E_CURLOCR+200000
            DB     171            
            DB     171            
            DW     E_CURLOCY+200000
            DB     040            
            DB     040            
            DW     R_016366+200000
            DB     000            
            DB     177            
            DW     STNORMAL+200000

            NEWPAGE
; Parse table for loader (Esc & b ...
LOADERTAB:  DB     101            
            DB     105            
            DW     R_030114       
            DB     141            
            DB     144            
            DW     R_030114       
            DB     060            
            DB     067            
            DW     LDR_DIGIT+200000 ; '0'..'7' as in octal digit to be parsed by loader
            DB     012            
            DB     012            
            DW     R_016366+200000 ; LF
            DB     015            
            DB     015            
            DW     R_016366+200000 ; CR
            DB     023            
            DB     023            
            DW     R_016366+200000 ; DC3,XOFF
            DB     040            
            DB     040            
            DW     R_016366+200000 ; ' '
            DB     000            
            DB     177            
            DW     RESET+200000    ; All other characters
R_030114:   DW     LDR_ADR         ; 'A' or 'a' as in Loader:Address
            DW     LDR_B           ; 'B' or 'b' as in Loader:Blank
            DW     LDR_C           ; 'C' or 'c' as in Loader:Checksum
            DW     LDR_D           ; 'D' or 'd' as in Loader:Data
            DW     LDR_EXEC        ; 'E' as in Loader:Execute

            NEWPAGE
L_030126:   MVI    L,233           ; <----KEYREPTMR
            MVI    M,000          
L_030132:   XRA    A              
            RET                   

            NEWPAGE
; Parse table for Esc & ...
ESCAMPTAB:  DB     141            
            DB     144            
            DW     R_030144       
            DB     000            
            DB     177            
            DW     STNORMAL+200000
R_030144:   DW     STCURLOC        ; Esc & a = SET CURSOR LOCATION
            DW     STLOADER        ; Esc & b = LOADER
            DW     STNORMAL        ; Esc & c
            DW     STENHDISP       ; Esc & d = ENHANCE DISPLAY

            NEWPAGE
; Parse table for Enhance Display (Esc & d ...)
ENHDISPTAB: DB     100            
            DB     117            
            DW     E_ENHDISP+200000
            DB     000            
            DB     177            
            DW     STNORMAL+200000

            NEWPAGE
; Parse table for charcter set selection (Esc ) ...)
ESCCPARTAB: DB     100            
            DB     103            
            DW     E_SELCSET+200000 ; Esc ) @-C select charset
            DB     000            
            DB     177            
            DW     STNORMAL+200000 ; Esc ) any other character

            NEWPAGE
; Parse table for display functions (Esc Y / Esc Z)
DISPFUNCTAB:
            DB     015             ; CR
            DB     015            
            DW     CRLF+200000    
            DB     033             ; Esc
            DB     033            
            DW     E_DISPFUNESC+200000
            DB     132             ; 'Z' - Esc Z cancels display functions when in display functions state.
            DB     132            
            DW     E_DISPFUNOFF+200000
            DB     000             ; Any other character
            DB     177            
            DW     C_PRINTABLE+200000

            NEWPAGE
L_030214:   MVI    L,333          
            JMP    MOV_HL_M       

L_030221:   MVI    H,077           ; Fetch whats at 077:341 into HL
L_030223:   MVI    L,342           ; Fetch what H:341 points to into HL
L_030225:   DCR    L               ; Fetch what HL points to into HL (HL points to high byte)
MOV_HL_M:   MOV    D,M             ; Fetch what HL points to into HL (HL points to low byte)
            INR    L              
            MOV    H,M            
            MOV    L,D            
SKIPCHAR:   RET                   

L_030233:   RST    010Q           
            DCR    E              
L_030235:   MOV    L,E            
            MOV    H,D            
            MOV    A,M            
            CPI    320            
            JC     L_030261       
            MOV    D,M            
            DCR    L              
            MOV    E,M            
            MOV    A,E            
            CALL   L_032230       
            JNZ    L_030263       
            MOV    L,E            
            MOV    H,D            
L_030261:   MOV    A,M            
            CMP    A              
L_030263:   MVI    H,077          
            RET                   

L_030266:   MOV    C,A            
            MVI    L,343          
            MVI    H,077          
            MOV    A,D            
            ORA    A              
            JZ     L_030301       
            ADD    M              
L_030301:   MOV    M,A            
L_030302:   MVI    L,341          
            MOV    M,C            
            INR    L              
            MOV    M,B            
SETDISP:    MVI    L,DISPLISTS >> 10Q ; Point to last byte of RAM = Start of display list
            MVI    H,DISPLISTS >> 10Q
L_030313:   DCR    C              
L_030314:   MVI    A,177           ; 01111111B => Display on, DMA off, Cursor Y=31 (out of range=>Not shown)
            OUT    33Q             ; Output Cursor Y Position (and Display on/DMA on)
            MOV    M,B             ; Display Link Byte to display list first entry
            DCR    L               ; Decrement display list pointer. Display list is in reversed order in memory!
            MOV    M,C             ; Display Link Byte LSB to display list second entry
            MVI    A,077           ; 00111111B => Display on, DMA on, Cursor Y=31 (out of range=>Not shown)
            OUT    33Q             ; Output Cursor Y Position (and Display on/DMA on)
            MOV    A,H            
            MVI    H,077          
            RET                   

E_TXBLOCK:  MVI    B,020          
L_030333:   CALL   L_036215       
            MVI    L,264           ; <----KEYOPTAH
            MOV    A,M            
            ADD    A              
            JP     L_031046       
            JC     L_031043       
            JMP    L_031036       

KBDEVFUNC:  MVI    L,214           ; <----COL01STATE
            MOV    A,M            
            RRC                    ; Rotate 'Remote' into Carry
            JNC    P0_MAIN_INIT   
            MVI    L,241          
            MOV    M,C            
            CALL   L_030373       
            JMP    P0_MAIN_LOOP   

L_030373:   MVI    L,264           ; Let HL point to Keyboard options A-H register
            MOV    A,M             ; Get Keyboard Options
            ANI    040             ; Mask B1 = Space Overwrite Latch
            CZ     P0C_DC1         ; Call if 0, space overwrite latch disabled
            MVI    B,010          
            CALL   QRYBLKMODE      ; Check if Block Mode. Return with Z=1 if in Block Mode
            JNZ    L_031022       
L_031013:   MVI    L,264           ; <----KEYOPTAH
            MOV    A,M            
            ADD    A              
            JP     L_031046       
L_031022:   MVI    L,264           ; <----KEYOPTAH
            MOV    A,M            
            ADD    A              
            JC     L_031043       
            ANI    100            
            CZ     P0C_DC1        
L_031036:   MVI    A,001          
            JMP    L_031047       

L_031043:   CALL   P0C_DC1        
L_031046:   MOV    A,B            
L_031047:   ORA    B              
            MVI    L,267          
            ORA    M              
            MOV    M,A            
            JMP    L_033073       

; Called when the Remote key is pressed
FK_REMOTE:  CALL   L_031170       
            JMP    P0_MAIN_LOOP   

BEEP:       MVI    L,KBDLEDS & 377Q ; <----KBDLEDS
            MVI    H,KBDLEDS >> 10Q
            MOV    A,M             ; Get old LED status
            ORI    200             ; Set bit to trigger beeper on write
            OUT    11Q             ; Output to LED Latch and Alarm Generator
            RET                   

L_031076:   MVI    L,214           ; <----COL01STATE
            MOV    A,M            
            ANI    001            
            CPI    001            
            RNZ                   
            MVI    L,223           ; <----COL10STATE
            ANA    M              
            RET                   

; Called when Display Functions key is pressed
FK_DISPFUNK:
            MVI    L,240           ; Let HL point to keyboard LED register
            MOV    A,M             ; Get LED register
            RRC                    ; Rotate B0 into carry. B0=LED1=Display functions LED
            MVI    B,331          
            JNC    L_032056       
            CALL   L_031157       
            JMP    P0_MAIN_INIT   


            NEWPAGE
; Sets PARSETABPTR Pointer to point to DISPFUNCTAB

STDISPFUNC: MVI    L,PARSETABPTR & 377Q ; <----PARSETABPTR
            MVI    M,DISPFUNCTAB & 377Q
            INR    L              
            MVI    M,DISPFUNCTAB >> 10Q
            CALL   L_007361       
            JMP    L_032025       

E_DISPFUNOFF:
            CALL   C_PRINTABLE     ; Display character in register A at current output position
            MVI    L,252          
            MOV    A,M            
            CPI    033            
            RNZ                   
L_031157:   CALL   STNORMAL       
            JMP    L_007364       

INITIO:     CALL   P0_POLLCOMSTAT  ; Get Communication Interface Status + Keep flagbit 7. On return A=New status B=Old C=actual interface
L_031170:   MVI    L,267          
            MVI    M,000          
            CALL   P0C_DC1        
CLRRXBUF:   MVI    L,RXBUFPTRIN & 377Q ; <----RXBUFPTRIN
            MVI    M,RXBUFLO & 377Q
            DCR    L              
            MVI    M,RXBUFLO & 377Q
            RET                   

L_031207:   MVI    L,266           ; <----FLAGS
            MOV    A,M            
            RRC                   
            RNC                   
            INR    L              
            MOV    A,M            
            RRC                   
            JC     L_031243       
            RRC                   
            JC     L_027230       
            RRC                   
            JC     L_034076       
            RRC                   
            JC     L_033021       
            RRC                   
            JC     L_026004       
            RET                   

L_031243:   MVI    L,267          
            MOV    A,M            
            ANI    376            
            MOV    M,A            
            JMP    L_007331       

DLY_10ms:   MVI    L,001          
DLY_n10ms:  MVI    H,237          
L_031260:   RST    010Q           
            DCR    H              
            JNZ    L_031260       
            DCR    L              
            JNZ    DLY_n10ms      
            MVI    H,077          
            RET                   

L_031274:   MVI    L,266           ; <----FLAGS
            MOV    A,M            
            ORA    A              
            JM     P0_MAIN_LOOP   
L_031303:   CALL   P0_DO_RX       
            JZ     P0_MAIN_LOOP   
            ORA    A              
            JM     L_031333       
            CALL   L_010166       
            JNZ    L_031343       
            ANI    003            
            JNZ    L_031303       
            JMP    P0_MAIN_LOOP   

L_031333:   CALL   P0_QRYDFUNCS   
            CZ     STNORMAL       
            MVI    C,177          
L_031343:   MVI    H,077          
            MVI    A,001          
            CALL   DISBITS        
            JMP    P0_KBDEVCTL    

L_031355:   MVI    L,321           ; <----CURXPOS
            MVI    M,000          
L_031361:   MVI    L,321           ; <----CURXPOS
            MOV    C,M            
L_031364:   MVI    L,365          
            MVI    M,377          
            CALL   L_012270       
            MVI    L,365          
            MVI    H,077          
            MVI    M,000          
            RET                   

            NEWPAGE
; Sets PARSETABPTR Pointer to point to NORMALTAB
; NORMALTAB is in Page 0 so address of NORMALTAB is fetched from
; a fixed address (000005)
STNORMAL:   MVI    L,NORMALTABPTR & 377Q ; Point to pointer to NORMALTAB from other PROM
            MVI    H,NORMALTABPTR >> 10Q
            MOV    A,M             ; Gets NORMALTAB Pointer into AB register 'pair'
            INR    L              
            MOV    B,M            
            MVI    L,PARSETABPTR & 377Q ; Set HL pointer to PARSETABPTR
            MVI    H,PARSETABPTR >> 10Q
            MOV    M,A             ; Store Pointer to NORMALTAB in PARSETABPTR
            INR    L              
            MOV    M,B            
            MVI    A,367           ; ?
            CALL   CLRFLAGS        ; Clear 'Flags' indicated by ~A
L_032025:   MVI    L,271           ; <----ESCSEM
            MVI    M,000          
            RET                   

; Called when the Memory Lock key is pressed
FK_MEMLCK:  MVI    L,266           ; <----FLAGS
            MVI    A,040          
            MVI    B,354          
            JMP    L_032051       

; Called when the Insert Character key is pressed
FK_INSCH:   MVI    A,002          
            MVI    B,321          
L_032047:   MVI    L,240           ; <----KBDLEDS
L_032051:   ANA    M              
            JZ     L_032056       
            INR    B              
L_032056:   MVI    L,234           ; <----KBDCHAR
            MOV    M,B            
            JMP    P0_KBDEVESC2   

; Called when the Format Mode key is pressed
FK_FMTMDE:  MVI    A,020          
            MVI    B,327          
            JMP    L_032047       

E_INSCH_OFF:
            MVI    A,375          
LEDOFF:     MVI    L,KBDLEDS & 377Q ; <----KBDLEDS
            ANA    M              
            MOV    M,A            
            OUT    11Q             ; Output to LED Latch and Alarm Generator
            RET                   

E_INSCH_ON: MVI    A,002          
LEDON:      MVI    L,KBDLEDS & 377Q ; <----KBDLEDS
            ORA    M              
            MOV    M,A            
            OUT    11Q             ; Output to LED Latch and Alarm Generator
            RET                   

; Check if block mode active
; If the keyboard button is pressed also check if disabled
; using options switches on keyboard interface
; A and L destroyed. Z indicates block mode
QRYBLKMODE: MVI    L,COL10STATE & 377Q ; Let HL point to Column 10 key state register
            MOV    A,M             ; Get Column 10 key state
            ANI    001             ; Mask bit 0 - 'Block Mode'
            RZ                     ; Return if not in 'Block Mode'
L_032121:   MVI    L,KEYOPTAH & 377Q ; Let HL point to Keyboard options A-H register
            MOV    A,M             ; Get Keyboard Options
            ANI    010             ; Mask B3 = Block mode option
            RET                   

L_032127:   MVI    H,077          
QRYFMTMODE: MVI    A,020          
            JMP    P0_QRYLEDS     

C_BS:       MVI    L,321           ; <----CURXPOS
            MOV    B,M            
            DCR    B              
            RM                    
            MOV    M,B            
            RET                   

            NEWPAGE
; Input to this proc is C and pointer in PARSETABPTR
; PARSETABPTR Points to a table with 4-byte entries
; C-(UNKN+4*n).B0 => an index IF
; (UNKN+4*n).B1>=C
PARSECHAR:  MVI    L,PARSETABPTR & 377Q ; Pointer at 077272
            CALL   MOV_HL_M        ; Get pointer into HL. Afterwards HL will point to one of the tables VTAB..ZTAB
PRSNEXTENT: RST    010Q           
            MOV    A,C             ; C to accumulator for compare
            SUB    M               ; Subtract first entry of table
            JM     HLPLUS4         ; If entry.B0>C then move to next table entry
            MOV    B,A             ; C>=Entry, save in B
            INR    L               ; Point to next value in same table entry
            MOV    A,M             ; Get the second byte of the entry
            CMP    C               ; Compare with incoming C
            JM     HLPLUS3         ; If C>Entry.B1 then move to next table entry
            INR    L               ; Fetch pointer following limits
            MOV    E,M             ; B2 Into E, eventually L
            INR    L              
            MOV    A,M             ; B3 Into A, eventually H
            ORA    A               ; Sets flags
            JM     STO_AE_JMP      ; If the adress at B2/B3 has MSB set, its a direct jump address
            MOV    H,A             ; If not, its a reference to a table of more entries
            MOV    A,B             ; Get the (difference between C and B0)/index into the table
; Get jump address. H=Pointer High E=Pointer Low A=Index
GET_JMP_ADR:
            RLC                    ; Multiply index by 2. (2 Bytes / table entry)
            ADD    E               ; Add low part of table address. (Note, no handling of carry)
            MOV    L,A             ; Use as Low part of pointer to correct table entry
            MOV    E,M             ; Gets LSB from table
            INR    L               ; Point to MSB
            MOV    A,M             ; Gets MSB from table
; Store jump address in RAM E=LowByte A=HighByte
STO_AE_JMP: MVI    L,RAMJMPLSB & 377Q ; <----RAMJMPLSB
            MVI    H,RAMJMPLSB >> 10Q
            MOV    M,E             ; Store LSB of jump address with JMP instruction in RAM
            INR    L               ; Move HL to RAMJMPMSB
            MOV    M,A             ; Store MSB of jump address with JMP instruction in RAM
            RET                   

; Move HL pointer to next table entry, then try it
HLPLUS4:    INR    L              
HLPLUS3:    INR    L              
            INR    L              
            INR    L              
            JMP    PRSNEXTENT     


            NEWPAGE
L_032226:   DCR    L              
L_032227:   MOV    A,M            
L_032230:   ANI    017            
            XRI    017            
            RET                   

L_032235:   MVI    A,015          
            CALL   L_037312       
            MVI    L,217           ; <----COL04STATE
            MOV    A,M            
            RRC                    ; Rotate 'Auto-LF' into Carry
            RNC                   
L_032247:   MVI    A,012          
            JMP    L_037312       

FK_ENTER:   CALL   L_025324       
            JMP    P0_MAIN_INIT   

SETFLAGS:   MVI    L,FLAGS & 377Q  ; Let HL point to 'Flags'
            ORA    M               ; Bitwise OR 'Flags' with A
            MOV    M,A             ; Store in 'Flags' register again
            RET                   

E_CURLOCPL: MVI    L,210          
            MVI    M,001          
            JMP    R_016366       

E_CURLOCMI: MVI    L,210          
            MVI    M,377          
            JMP    R_016366       

E_CURLOCDGT:
            MVI    L,324           ; <----RXCHAR
            MOV    A,M            
            ANI    017            
            MVI    L,211          
            MVI    C,012          
            JMP    L_035026       

L_032321:   MVI    L,333          
            CALL   L_032374       
L_032326:   MVI    L,326          
            MVI    M,000          
            CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            RZ                    
            MVI    L,254          
            MVI    M,001          
            RET                   

L_032343:   MVI    L,333          
            JMP    L_032357       

L_032350:   MVI    L,322          
            JMP    L_032361       

L_032355:   MVI    L,361          
L_032357:   MVI    H,077          
L_032361:   MOV    E,M            
            INR    L              
            MOV    D,M            
            RET                   

L_032365:   MVI    L,322          
            JMP    L_032374       

L_032372:   MVI    L,361          
L_032374:   MVI    H,077          
            MOV    M,E            
            INR    L              
            MOV    M,D            
            RET                   

L_033002:   MVI    L,363          
            MVI    H,077          
            MOV    A,M            
            ORA    A              
            RET                   

L_033011:   MVI    L,355          
MOV_M_AB:   MVI    H,077          
            MOV    M,A            
            INR    L              
            MOV    M,B            
            RET                   

L_033021:   MVI    A,367          
            CALL   L_036201       
            CALL   L_033044       
            MVI    L,241          
            MOV    A,M            
            ANI    177            
L_033036:   CALL   L_037312       
            JMP    L_007320       

L_033044:   MVI    A,033          
            JMP    L_037312       

L_033051:   MVI    L,265           ; <----DISABBITS
            MOV    A,M            
            ANI    100            
            RNZ                   
E_ENABLKBD: MVI    A,257          
ENABITS:    MVI    L,265           ; <----DISABBITS
            ANA    M              
            MOV    M,A            
            RET                   

E_DISABKBD: MVI    A,100          
            CALL   DISBITS        
L_033073:   CALL   L_030126       
            MVI    A,020          
DISBITS:    MVI    L,265           ; <----DISABBITS
            ORA    M              
            MOV    M,A            
            RET                   

L_033105:   MVI    H,077          
L_033107:   MVI    L,254          
            MOV    A,M            
            ORA    A              
            RET                   

E_DISPFUNESC:
            CALL   C_PRINTABLE     ; Display character in register A at current output position
L_033117:   MVI    L,204          
            MVI    M,000          
            RET                   

            NEWPAGE
STCURLOC:   MVI    L,PARSETABPTR & 377Q ; <----PARSETABPTR
            MVI    M,CURLOCTAB & 377Q
            INR    L              
            MVI    M,CURLOCTAB >> 10Q
            MVI    L,320           ; <----CURYPOS
            MOV    A,M            
            INR    L              
            MOV    B,M            
            MVI    L,313          
            MOV    M,A            
            INR    L              
            MVI    M,377          
            INR    L              
            MOV    M,B            
L_033150:   XRA    A              
            MVI    L,210          
            MOV    M,A            
            INR    L              
            MOV    M,A            
            INR    L              
            MOV    M,A            
            JMP    R_016366       

E_CURLOCC:  MVI    D,117          
            MVI    E,315          
            MVI    L,321           ; <----CURXPOS
            MOV    B,M            
            JMP    L_033222       

E_CURLOCY:  MVI    D,027          
            MVI    E,313          
            MVI    L,320           ; <----CURYPOS
            MOV    B,M            
            JMP    L_033222       

E_CURLOCR:  MVI    D,377          
            MVI    E,314          
            MVI    L,320           ; <----CURYPOS
            MOV    A,M            
            MVI    L,343          
            ADD    M              
            MOV    B,A            
L_033222:   CALL   L_033364       
            MVI    L,324           ; <----RXCHAR
            MOV    A,M            
            ANI    040            
            JNZ    L_033150       
            MVI    L,314          
            MOV    A,M            
            ORA    A              
            JP     L_033255       
            DCR    L              
            MOV    A,M            
            MOV    A,A            
            MVI    L,320           ; <----CURYPOS
            MOV    M,A            
            JMP    L_033265       

L_033255:   MVI    L,255          
            MOV    A,M            
            ORA    A              
            RNZ                   
            CALL   L_033306       
L_033265:   MVI    L,315          
            MOV    A,M            
L_033270:   MVI    L,321           ; <----CURXPOS
            MOV    M,A            
            CALL   L_012263       
            RZ                    
            MVI    L,325           ; <----PRCHAR
            MVI    M,040          
            JMP    L_011142       

L_033306:   MVI    L,314          
            MOV    A,M            
            MVI    L,343          
            SUB    M              
            MVI    L,320           ; <----CURYPOS
            JC     L_017214       
            CPI    030            
            MOV    M,A            
            RC                    
            MVI    M,027          
            SUI    027            
            MOV    C,A            
            MVI    L,367          
            MOV    M,C            
L_033335:   CALL   L_017145       
            MVI    L,367          
            MVI    H,077          
            MOV    A,M            
            SUB    C              
            RZ                    
            MOV    M,A            
            MVI    C,000          
            CALL   L_012270       
            RNZ                   
            MVI    L,367          
            MOV    C,M            
            JMP    L_033335       

L_033364:   MVI    L,212          
            MOV    A,M            
            ORA    A              
            MVI    L,210          
            MOV    A,M            
            JZ     L_034006       
            ORA    A              
            JP     L_034032       
L_034002:   XRA    A              
            JMP    L_034033       

L_034006:   MVI    L,211          
            ORA    A              
            MOV    A,B            
            JM     L_034045       
            JNZ    L_034036       
            MOV    A,M            
L_034021:   INR    D              
            JZ     L_034033       
            CMP    D              
            JC     L_034033       
            DCR    D              
L_034032:   MOV    A,D            
L_034033:   MOV    L,E            
            MOV    M,A            
            RET                   

L_034036:   ADD    M              
            JNC    L_034021       
            JMP    L_034032       

L_034045:   SUB    M              
            JNC    L_034033       
            JMP    L_034002       

E_TXCURREL: MVI    A,004          
            CALL   SETFLAGS        ; Set 'Flags' indicated by A
            JMP    L_034071       

E_TXCURABS: MVI    A,373          
            CALL   CLRFLAGS        ; Clear 'Flags' indicated by ~A
L_034071:   MVI    B,004          
            JMP    L_030333       

L_034076:   MVI    A,373          
            CALL   L_036201       
            CALL   L_033044       
            MVI    A,046          
            CALL   L_037312       
            MVI    A,141          
            CALL   L_037312       
            MVI    L,321           ; <----CURXPOS
            MOV    D,M            
            CALL   L_034173       
            MVI    A,143          
            CALL   L_037312       
            MVI    L,266           ; <----FLAGS
            MOV    A,M            
            ANI    004            
            MVI    L,320           ; <----CURYPOS
            JNZ    L_034162       
            MOV    A,M            
            MVI    L,343          
            ADD    M              
            MOV    D,A            
            CALL   L_034173       
            MVI    A,122          
            JMP    L_033036       

L_034162:   MOV    D,M            
            CALL   L_034173       
            MVI    A,131          
            JMP    L_033036       

L_034173:   MVI    C,144          
            CALL   L_034211       
            MVI    C,012          
            CALL   L_034211       
            MOV    A,D            
            JMP    L_037306       

L_034211:   MOV    A,D            
            MVI    E,057          
L_034214:   INR    E              
            SUB    C              
            JNC    L_034214       
            ADD    C              
            MOV    D,A            
            MOV    A,E            
            JMP    L_037312       

E_ENDUNPROT:
            MVI    B,300          
            JMP    L_034236       

E_STARTUNPROT:
            MVI    B,301          
L_034236:   CALL   QRYFMTMODE      ; Check if format mode active. Z=0 if active
            RNZ                   
            MOV    A,B            
            MVI    L,325           ; <----PRCHAR
            JMP    L_010301       

E_FMTMODE_OFF:
            MVI    L,254          
            MVI    M,000          
            MVI    A,357           ; Format mode LED off
            JMP    LEDOFF         


            NEWPAGE
; Set parse state to STLOADER

; You get here when the last character in the sequence Esc & b
; received. Sets the parse table pointer to point to the table used in
; the loader state. The loaders purpose is to allow loading code into the
; RAM of the terminal and execute it. This can be useful for test purposes
; or for amusement.

STLOADER:   MVI    C,246           ; Pointer to 'LOADER' message
            MVI    B,337          
            CALL   SETDISP         ; Sets display to show the 'LOADER' message. (Screen also cleared)
; Set PARSETABPTR pointer to point to LOADERTAB
            MVI    L,PARSETABPTR & 377Q ; HL points to the ParseTabPointer
            MVI    M,LOADERTAB & 377Q ; Make ParseTabPointer point to the loader Parse Table (LSB)
            INR    L               ; Point to MSB
            MVI    M,LOADERTAB >> 10Q ; Make ParseTabPointer point to the loader Parse Table (MSB)
; Clear Checksum Accumulator
            MVI    L,LDRCHECK & 377Q ; HL Points to checksum accumulator LSB
            XRA    A               ; Clear A
            MOV    M,A             ; Clear Checksum Accumulator LSB
            INR    L               ; Point to checksum accumulator MSB
            MOV    M,A             ; Clear Checksum Accumulator MSB
LDR_B:      MVI    L,RXCHAR & 377Q ; HL points to last received character
            MOV    A,M             ; Get character (077324)
            ANI    040             ; Mask bit 5. Will be 0 for 0-7 and a-d but 1 for A-E
            JZ     STNORMAL        ; => Go to normal state uppercase variant of command was used
; Clear Loader Buffer
            MVI    L,LDRBUF & 377Q ; HL Points to loader buffer LSB
            XRA    A               ; Clear A
            MOV    M,A             ; Clear loader buffer LSB
            INR    L               ; HL points to loader buffer MSB
            MOV    M,A             ; Clear loader buffer MSB
            JMP    R_016366       

; Loader A=Address command handler

; Moves what is in the loader buffer into the loader pointer
LDR_ADR:    MVI    L,LDRBUF & 377Q ; HL pointer to Loader Buffer LSB
            MOV    E,M             ; Get LSB
            INR    L               ; HL points to MSB
            MOV    D,M             ; Get MSB
            MVI    L,LDRPTR & 377Q ; HL points to Loader Pointer LSB
            MOV    M,E             ; Store Loader Buffers LSB into Loader Pointer LSB
            INR    L               ; HL Points to MSB
            MOV    M,D             ; Store Loader Buffers MSB into Loader Pointer MSB
LDRCSUM:    MVI    L,LDRCHECK & 377Q ; HL Pointer to Loader Checksum
            MVI    C,001           ; Plain add, no shifting
            CALL   ADDDIGIT        ; Accumulate the address to the checksum
            JMP    LDR_B          

; Loader D=Data command handler

; Moves what is in Loader Buffer into the location pointed to by Loader Pointer.
; Then increments Loader Pointer
LDR_D:      MVI    L,LDRBUF & 377Q ; HL pointer to Loader Buffer
            MOV    E,M             ; Fetch LSB. MSB actually ignored as we load byte by byte
            MVI    L,LDRPTR & 377Q ; HL pointer to Loader Pointer
            CALL   MOV_HL_M        ; Get Pointer into HL. After this, HL points to where the byte should go
            MOV    M,E             ; Store the byte from Loader Buffer where loader pointer points
            MVI    D,000           ; Prepare for checksumming. Only 8 bits to add
            MVI    L,LDRPTR & 377Q ; HL pointer to Loader Pointer again
            MVI    H,LDRPTR >> 10Q
L_034371:   MOV    C,M             ; Get byte from Loader Pointer
            INR    C               ; Increment byte
            MOV    M,C             ; Re-store byte into Loader Pointer
            JNZ    LDRCSUM         ; If it did not overflow we are done, go to checksum adding
            INR    L               ; Point to next byte of Loader Pointer
            JMP    L_034371        ; Repeat to increment next byte of Loader pointer

; Loader E=Execute command handler

; Turns on DMA and Display again after load operation and then
; Jumps to the location loaded by latest digits entered
; The jump is via a jump in RAM memory as indirect jump is not supported by
; the 8008 instruction set.
LDR_EXEC:   MVI    A,300           ; Cursor to row 0, DMA on ,Display On
            OUT    33Q             ; Output to Cursor Y Position (and Display on/off / DMA on/off) register
            MVI    L,LDRJMP & 377Q ; JMP instruction placed here (077246)
            MVI    M,104           ; Opcode for JMP
            JMP    LDRJMP          ; Jump to jump to loaded code

; Loader digit handler

; Called when a digit is received in loader state
; Adds the digit to the (right) end of the 16 bit value at 077324
LDR_DIGIT:  MVI    L,RXCHAR & 377Q ; Fetch from last character received
            MOV    A,M             ; Fetch the character
            ANI    007             ; Mask lower 3 bits. Makes '0'..'7' into 0..7
            MVI    L,LDRBUF & 377Q ; Point to Loader buffer
            MVI    C,010           ; Old content of buffer should be multiplied by 10Q (shifted 3 bits left)
L_035026:   MOV    E,A             ; New bits to add to loader buffer to E
            MVI    D,000           ; Only 8 (actually 3) bits to add
            CALL   ADDDIGIT        ; Call the Multiply and Accumulate function
            JMP    R_016366       

; Loader C=Checksum handler

; Checks the content of the loader checksum accumulator against the value
; in the loader buffer. If they dont match locks up the terminal
; And that must be the worst way to handle things ever...
LDR_C:      MVI    A,010          
            CALL   DISBITS        
            MVI    L,LDRBUF & 377Q ; HL Pointer to Loader Buffer LSB
            MOV    A,M             ; Fetch LSB
            INR    L               ; HL Pointer to Loader Buffer MSB
            MOV    B,M             ; Fetch MSB
            MVI    L,242           ; HL Pointer to Loader Checksum Accumulator
            XRA    M               ; Bitwise Compare of Checksum LSB with Buffer LSB value
            MOV    C,A             ; Store result of compare in C
            MOV    A,B             ; Fetch Buffer MSB to accumulator
            INR    L               ; HL Pointer to Checksum Accumulator MSB
            XRA    M               ; Bitwise compare of Checksum MSB with Buffer MSB value
            ORA    C               ; Bitwise or with any differences from LSB compare
            JZ     LDR_B           ; Should be 0, If it is jump away
            HLT                    ; There was a checksum difference, HALT. No message, no nothing...


; Adds input in DE to C*(HL)
; Used to add digits to accumulator in loader

; Side effects: Destroys every register available
ADDDIGIT:   RST    010Q            ; Poll Serial Input Interrupt
            MOV    A,M             ; Get LSB from (HL)
            ADD    E               ; Add E
            MOV    E,A             ; Keep LSB in E
            INR    L               ; Point to MSB
            MOV    A,M             ; Get MSB
            ADC    D               ; Add D and carry from LSB-Add to MSB
            MOV    D,A             ; Keep MSB in D
            DCR    L               ; Point to LSB again
            DCR    C               ; Decrement number of times to add (HL)
            JNZ    ADDDIGIT        ; If more times to add, repeat
            MOV    M,E             ; Move final sum to (HL) LSB
            INR    L               ; Point to MSB
            MOV    M,D             ; Move final sum to (HL) MSB
            XRA    A               ; Clear accumulator
            RET                    ; Finished

            NEWPAGE
; Startup code. Jumps here at reset

; Check how much RAM is installed
BOOTCODE:   MVI    L,000           ; HL points to Top of address space -2K
            MVI    H,070          
            MOV    B,L            
            MVI    C,377          
TRYNEXT1K:  MOV    M,B             ; Test write 0
            MOV    A,M            
            CMP    B              
            JNZ    NOTRAM          ; Compare readback, jump if not equal
            MOV    M,C             ; Test write 255
            MOV    A,M            
            CMP    C              
            JNZ    NOTRAM          ; Compare readback, jump if not equal
            MOV    A,H            
            SUI    004             ; Move pointer down 1K
            MOV    H,A            
            JMP    TRYNEXT1K       ; Try next 1K

NOTRAM:     MVI    A,074           ; Failed to write to address in HL
            SUB    H              
            MOV    E,A             ; Num 256d byte RAM pages to E

; Clear upper RAM page
L_035145:   MVI    A,300           ; Display off, DMA off, Cursor Y to Row 0
            OUT    33Q             ; Out to display control PCA
            MVI    L,002          
            CALL   DLY_n10ms       ; Delay 10ms*value in L
            XRA    A               ; Clear A
            MVI    C,313           ; 203d Bytes
            MVI    L,RAMTOP & 377Q ; Point to 077372 which is in top of RAM
            MVI    H,RAMTOP >> 10Q
L_035164:   MOV    M,A             ; Clear memory byte
            DCR    L               ; Point to next byte and decrement number bytes
            DCR    C              
            JNZ    L_035164        ; Loop until area cleared
            MVI    L,RAMSIZE & 377Q ; Point to 077263

; Initialize system variables
            MOV    M,E             ; Store # 256d byte pages of RAM
            MVI    A,036           ; Input keyboard interface jumpers A-H settings
            IN     1               ; Input from Keyboard interface
            MVI    L,KEYOPTAH & 377Q ; Let HL point to keyboard options A-H register
            MOV    M,A             ; Store keyboard option settings
            MVI    L,262           ; Index points to 077262
            MVI    M,377           ; Initialize to 377 (decimal 255)
            MVI    L,INITALTC & 377Q ; Let HL point to Initial Alternative Character Set
            MVI    H,INITALTC >> 10Q
            MOV    A,M             ; Get hard coded default value for alternative character set
            INR    L               ; Point to next value (NORMALTABPTR)
            MOV    B,M             ; Get LSB of pointer to normal parse table
            INR    L              
            MOV    C,M             ; Get MSB of pointer to normal parse table
            MVI    L,PARSETABPTR & 377Q ; Let HL point to parse table pointer
            MVI    H,PARSETABPTR >> 10Q
            MOV    M,B             ; Set LSB from ROM value
            INR    L              
            MOV    M,C             ; Set MSB from ROM value
            MVI    L,ALTCSET & 377Q ; Let HL point to selected alternative character set
            MOV    M,A             ; Set init alternative character set from the ROM value

; Initialize indirect JUMP location
            MVI    L,RAMJMP & 377Q ; Let HL point to the jump instruction in RAM. Used for indirect jumps
            MVI    M,104           ; Set it to the Opcode JMP

; Initialize IO parts (buffer pointers and status)
            CALL   INITIO         
            MVI    H,377          

; Initialize Memory blocks in list of free blocks.
; Each block is 16d bytes (20Q)
; They are linked to eachother through pointers where A pointer in the
; First two bytes of each block point to the tail of the next block
; The first block is point to by the FREELIST pointer in the system
; variables block (actually at 077371
; Pointersin the links have the upper 2 bits set (3xxxxx). The CPU
; does not care about these as there is only 14 address bits but the DMA
; also needs to follow these and use the 2 upper bits to identify links
; to follow. See Figure 15, DMA Data Encoding Diagram in Sep-10-77
; 13255-91112 for instance.

; Pointer alignment for first segment. 16 bytes starting at xxxFh
            MVI    A,057           ; First address below system variables     A:=57
            SUI    017             ; Move down to guarantee alignment         A:=40 (A-17)
            ORI    017             ; Set lowest 4 bits to 1s                  A:=57 (A or 17)

            SUI    003             ; A:=54 (A-3)
            MOV    B,A             ; B:=54 (B=A)
            MOV    D,H             ; D:=377 (D=H)
            MOV    L,A             ; L:=54 (L=A)
            MVI    M,000           ; @377054:=0 As 377 does not exist this is 077054=0

            MVI    L,FREELIST & 377Q ; Let HL point to free-list pointer 077371
            MVI    H,FREELIST >> 10Q
            SUI    001             ; A:=53 (A-1)
            MOV    M,A             ; @077371:=53
            INR    L              
            MOV    M,D             ; @077372:=377

            SUI    012             ; A:=41 (A-12)
L_035273:   MOV    L,A             ; L:=41,21,01 (A)
            MOV    H,D             ; H:=377 Index pointer to 377041,377021,377001
            SUI    002             ; A:=37,17,377 (A-2)
            JC     L_035312        ; Jumps on 3d pass (1-2=>carry)
L_035302:   MOV    M,D             ; @077041,077021:=377
            DCR    L               ; Index points to 377040,377020
            MOV    M,A             ; @077040:=36,@077020:=16
            SUI    016             ; A:=21,01 (A-16)
            JMP    L_035273        ; Loop back

L_035312:   DCR    D               ; D:=376 (D-1)
            DCR    E               ; Decrement E (number of 256d byte RAM pages)
            JNZ    L_035302       
            MVI    M,375          
            INR    B              
            DCR    L              
            MOV    M,B            

            CALL   L_014164       
            CALL   L_036042       
            INR    L              
            INR    L              
            MVI    M,000          
            SUI    002            
            MOV    L,A            
            MVI    M,314           ; End-Of-Line marker for DMA
            MVI    L,376           ; <----DISPLSLSB
            CALL   MOV_M_AB        ; Move content of register pair AB into memory at HL
            MVI    L,322          
            MOV    M,A            
            INR    L              
            MOV    M,B            
            ADI    001            
            MVI    L,333          
            MVI    C,004          
L_035363:   MOV    M,A            
            INR    L              
            MOV    M,B            
            INR    L              
            DCR    C              
            JNZ    L_035363       
            XRA    A              
            OUT    11Q             ; Output to LED Latch and Alarm Generator
            MVI    L,261          
            MOV    M,A            
            MVI    A,020          
            IN     6              
            ORA    A              
            JZ     L_036017       
            MVI    A,022          
            IN     6              
            MVI    M,001          
            JMP    L_036037       

L_036017:   MVI    A,060           ; Input status word from printer port
            IN     2              
            ORA    A              
            JZ     L_036037       
            MVI    A,120           ; Input FW Control word from printer port
            IN     2              
            ANI    037            
            RAL                   
            OUT    12Q            
            MVI    M,002          
L_036037:   JMP    P0_MAIN_INIT   

L_036042:   ORI    017            
            SUI    002            
            DCR    L              
            MOV    M,H            
            DCR    L              
            MOV    M,A            
            MOV    L,A            
            MVI    M,316           ; End-Of-Page marker for DMA
            DCR    L              
            MVI    M,000          
            RET                   

            NEWPAGE
; State ESC & entered

STESCAMP:   MVI    A,ESCAMPTAB & 377Q
            MVI    B,ESCAMPTAB >> 10Q
            JMP    L_016361       

E_RESTERM:  MVI    L,264           ; Let HL point to Keyboard options A-H register
            MOV    A,M             ; Get Keyboard Options
            ANI    040             ; Mask B5 = 2640/2644 mode switch
            JNZ    RESET           ; Do a hard reset if 1 (jumper out, 2640 mode)
            CALL   E_FMTMODE_OFF  
            CALL   E_MEMLCKOFF    
            CALL   E_HOMECURUP    
            RST    010Q           
            CALL   E_INSCH_OFF    
            MVI    L,277          
            MVI    H,077          
            MVI    C,012          
L_036123:   RST    010Q           
            XRA    A              
            MOV    M,A            
            INR    L              
            DCR    C              
            JNZ    L_036123       
            MVI    L,267          
            MOV    M,A            
            DCR    L              
            MOV    A,M            
            ANI    200            
            MOV    M,A            
            JMP    E_CLRMEM       

STENHDISP:  MVI    A,ENHDISPTAB & 377Q
            MVI    B,ENHDISPTAB >> 10Q
            JMP    L_016361       


            NEWPAGE
; State ESC ) entered

STESCCPAR:  MVI    A,ESCCPARTAB & 377Q
            MVI    B,ESCCPARTAB >> 10Q
            JMP    L_016361       

E_SELCSET:  MVI    L,325           ; <----PRCHAR
            MOV    A,M            
            ANI    003            
            RLC                   
            RLC                   
            RLC                   
            RLC                   
            MVI    L,270           ; <----ALTCSET
            MOV    M,A            
            RET                   

L_036201:   MVI    L,267          
            ANA    M              
            MOV    M,A            
            MVI    B,000          
            JNZ    L_030333       
            CALL   L_033051       
L_036215:   MVI    A,376          
CLRFLAGS:   MVI    L,FLAGS & 377Q  ; Let HL point to 'Flags'
            ANA    M               ; Bitwise AND 'Flags' with A
            MOV    M,A             ; Store in 'Flags' register again
            RET                   

E_TXSTATUS: MVI    B,002          
            JMP    L_030333       


            NEWPAGE
; Run selftests
; See 264XCompositeServiceManual Page 6-13 (Fig 6-2)
SELFTEST:   MVI    A,177          
            OUT    11Q             ; Output to LED Latch and Alarm Generator
            MVI    A,040           ; Set Selftest Running flag
            CALL   DISBITS        
; Test ROM
            MVI    H,000           ; Pointer to 0,0
            MOV    L,H            
            MOV    C,H             ; Clear checksum var
            MVI    E,040           ; Set page counter to 40 (32d*256d = 8192d = bytes of rom)
CHKSUMLP:   RST    010Q            ; Check serial
            MOV    A,C             ; Get current checksum
            ADD    M               ; Add byte from ROM
            ACI    000             ; Add any carry
            MOV    C,A             ; Save checksum in C
            INR    L               ; Point to next byte in ROM, Low part
            JNZ    CHKSUMLP        ; If pointer did not overflow, loop
            INR    H               ; Increment upper part of pointer
            DCR    E               ; Decrement page counter
            JNZ    CHKSUMLP        ; Pages left, then loop
            INR    C               ; Increments checksum by 1, sets flags (test for sum 377)
            MVI    C,233           ; Pointer to 'ROM TEST FAIL' message+1, Overwrites checksum but does not affect flags
            JNZ    ROMCHKFAIL      ; If checksum did not end up as 0, jump
; Test RAM
            MVI    A,300          
            OUT    33Q             ; Output Cursor Y Position (and Display on/DMA on)
            MVI    L,263           ; Let HL point to RAM Size in pages
            MVI    H,077          
            MOV    E,M             ; Get RAM size
            MVI    L,000           ; Let HL point to lowest address of page
TSTRAMRPT:  RST    010Q           
            MOV    A,M             ; Get current value at HL from RAM
            MOV    D,A             ; Keep value for later
            XRI    377             ; Invert value in A
            MOV    M,A             ; Store inverted value in RAM at original location
            CMP    M               ; Check that the value stored ok
            MOV    M,D             ; Store original value again
            JNZ    RAMCHKFAIL      ; Jump to error if inverted store was wrong
            MOV    A,D             ; Get original RAM value for comparison
            SUB    M               ; Subtract memory content. Sets flags and results in A=0 if correct
            JNZ    RAMCHKFAIL      ; Jump to error if Original store was wrong
            INR    L               ; Move on to next RAM byte
            JNZ    TSTRAMRPT       ; Repeat for next byte until L overflows
            DCR    H               ; Move to next (lower) RAM page
            DCR    E               ; Decrement number of pages to check
            JNZ    TSTRAMRPT       ; Repeat while still pages available
            OUT    33Q             ; Output Cursor Y Position (and Display on/DMA on)
            MVI    A,377           ; Trigger keyboard beep
            OUT    11Q             ; Output to LED Latch and Alarm Generator
; Test Character Sets
            MVI    L,TESTCSET & 377Q ; Let HL point to temp variable for character set selection
            MVI    H,077          
            MVI    A,300           ; 300 is above first charset but decremented on first loop through
TLOOPCS:    SUI    020             ; Set to next charset to display
            MOV    M,A             ; Set to loop variable (TESTCSET)
            MVI    L,TESTCHAR & 377Q ; Let HL point to temp variable for characters to output
            MVI    M,000           ; Initialize to Nul
TLOOPROW:   CALL   C_CR            ; Start on a new row
            CALL   L_016020       
            MVI    L,TESTCSET & 377Q ; Let HL point to temp variable for character set
            MOV    A,M             ; Get Current charset
            CALL   L_037271       
TLOOPCHR:   MVI    L,TESTCHAR & 377Q ; Let HL point to temp variable for characters to print
            MVI    H,077          
            MOV    A,M             ; Get next character to print
            MVI    L,PRCHAR & 377Q ; Let HL point to display print buffer
            MOV    M,A             ; Set character to print as next from loop variable
            ANI    007             ; Mask lower 3 bits (char mod 10Q)
            CPI    004             ; (char mod 10Q)=4?
            JNZ    SAMEGROUP       ; Skip printing of spaces if not=4. Creates spaces after 4,12,20... characters
            CALL   C_PRINTABLE     ; Print two 'blank' characters. Actually two #4 = EOT characters
            CALL   C_PRINTABLE     ; but given selected charset displayed as blanks
SAMEGROUP:  CALL   PRINTPRCHAR     ; Display character stored in PRCHAR at current output position
            MVI    L,TESTCHAR & 377Q ; Let HL point to character loop variable
            MOV    A,M             ; Get
            ADI    001             ; Increment
            MOV    M,A             ; Store in variable again
            CPI    100             ; Check if looping reached '@'
            JZ     TLOOPROW        ; If it did, continue on a new line
            ORA    A               ; Set more flags
            JP     TLOOPCHR        ; If character still <= Del, continue on same line
            CALL   CRLF            ; Blank line between character sets
            MVI    L,370           ; Let HL point to charset loop variable
            MOV    A,M             ; Get selected charset
            CPI    200             ; Reached last character set?
            JNZ    TLOOPCS         ; If not, repeat on next character set
; Test display enhancements
            MVI    L,TESTCHAR & 377Q ; Let HL point to test character loop variable
            MOV    M,A             ; A still 200 from previous test. Initialize enhancements loop
            CALL   L_016020       
TENHLOOP:   MVI    L,TESTCHAR & 377Q ; Let HL point to test character loop variable. Used for output + enhancement
            MVI    H,077          
            MOV    A,M             ; Get loop variable
            SUI    100             ; Calculate character to print
            MVI    L,PRCHAR & 377Q ; Let HL point to print buffer
            MOV    M,A             ; Set character to Enhancementcode - 100Q
            CALL   PRINTPRCHAR     ; Display character stored in PRCHAR at current output position
            MVI    L,TESTCHAR & 377Q ; Let HL point to loop variable
            MOV    A,M             ; Get current enhancement code from loop var
            ADI    001             ; Increment
            MOV    M,A             ; Store next enhancement
            MVI    L,325           ; Let HL point to print buffer
            CPI    220             ; Enhancement code reached 220Q?
            JZ     TENHRST         ; Terminate loop if it has
            MOV    M,A             ; Store updated code
            CALL   E_ENHDISP      
            JMP    TENHLOOP        ; Repeat with next enhancement selected

TENHRST:    MVI    M,200           ; HL still points to print buffer. Reset enhancements
            CALL   E_ENHDISP      
            CALL   C_PRINTABLE     ; Display character in register A at current output position
            CALL   C_PRINTABLE     ; Display character in register A at current output position
            MVI    A,004          
            CALL   DISBITS        
            JMP    L_027245       


            NEWPAGE
; Self test end
TEND:       CALL   CRLF           
            CALL   CRLF           
            MVI    A,337           ; Clear Selftest running flag
            CALL   ENABITS        
            CALL   L_033117        ; --- Clear 077204
            MVI    L,240           ; <----KBDLEDS
            MOV    A,M            
            OUT    11Q             ; Output to LED Latch and Alarm Generator
            JMP    P0_STRETURN    

RAMCHKFAIL: MVI    C,237           ; Pointer to 'RAM TEST FAIL' message+1
ROMCHKFAIL: CALL   L_037256       
            JNZ    RESET          
            MVI    B,337           ; Display Link byte ((037xxxQ>>10Q) OR 300Q
            CALL   SETDISP        
LOCKHALT:   JMP    LOCKHALT       


            NEWPAGE
; Static texts that can be shown on the display.
; These are not actually copied to display memory. Instead the DMA
; is pointed directly at these strings by setting the first block
; pointer (at 077377) to the suitable ROM address.
; Since the DMA starts at an address and then decrements to find the
; next byte to display the strings following are 'backwards'

            DB     316             ; Display end of page
            DB     114            
            DB     111            
            DB     101            
            DB     106            
            DB     040            
            DB     124            
            DB     123            
            DB     105            
            DB     124            
            DB     040            
            DB     115            
            DB     117            
            DB     122             ; ROM TEST FAIL
            DB     230             ; Display link byte LSB (037230Q AND 377Q)
            DB     337             ; Display link byte ((037230Q>>10Q) OR 300Q
            DB     101            
            DB     122             ; RA->M TEST FAIL
            DB     316             ; Display end of page
            DB     122            
            DB     105            
            DB     104            
            DB     101            
            DB     117            
            DB     114             ; LOADER
            DB     222             ; Display link byte LSB (037222Q AND 377Q)
            DB     337             ; Display Link byte ((037222Q>>10Q) OR 300Q
            DB     124            
            DB     116            
            DB     111            
            DB     122            
            DB     120             ; PRINT-> FAIL
            DB     202             ; Display Control byte, Set0, Inverse video

            NEWPAGE
L_037256:   MVI    H,077          
L_037260:   MVI    A,201          
            MVI    L,265           ; <----DISABBITS
            ANA    M              
            RET                   

            DB     301            
L_037267:   ORI    200            
L_037271:   MVI    L,276          
            MVI    M,017          
            MVI    L,325           ; <----PRCHAR
            JMP    L_010301       

L_037302:   RRC                   
            RRC                   
L_037304:   RRC                   
L_037305:   RRC                   
L_037306:   ANI    017            
            ADI    060            
L_037312:   MVI    L,324           ; <----RXCHAR
            MOV    M,A            
            MOV    B,A            
            MVI    L,265           ; <----DISABBITS
            MOV    A,M            
            ANI    040            
            JNZ    L_015202       
            MVI    L,346          
            MOV    M,C            
            DCR    L              
            MOV    M,E            
            DCR    L              
            MOV    M,D            
            MVI    L,214           ; <----COL01STATE
            MOV    A,M            
            RRC                    ; Rotate 'Remote' into Carry
            CC     P0_GATED_TX    
            MVI    L,346          
            MOV    C,M            
            DCR    L              
            MOV    E,M            
            DCR    L              
            MOV    D,M            
            RET                   

            MVI    E,040          
            JMP    L_035145       


            NEWPAGE
            DB     000            
            DB     000            
            DB     000            
            DB     000            
            DB     000            
            DB     000            
            DB     000            
            DB     000            
            DB     000            
            DB     000            
            DB     000            
            DB     000            
            DB     256             ; CRC 0
            DB     165             ; CRC 1
            DB     116             ; ROM Checksum

            NEWPAGE
RXBUFLOX:   EQU 077062
RXBUFLO:    EQU 077063
RXBUFHI:    EQU 077203
RXBUFPTROUT: EQU 077205
RXBUFPTRIN: EQU 077206
COMMSTATUS: EQU 077207
COL00STATE: EQU 077213
COL01STATE: EQU 077214
COL02STATE: EQU 077215
COL03STATE: EQU 077216
COL04STATE: EQU 077217
COL05STATE: EQU 077220
COL06STATE: EQU 077221
COL07STATE: EQU 077222
COL10STATE: EQU 077223
COL11STATE: EQU 077224
COL12STATE: EQU 077225
COL13STATE: EQU 077226
COL14STATE: EQU 077227
COL15STATE: EQU 077230
COLSTATEPTR: EQU 077231
KEYSCANCDE: EQU 077232
KEYREPTMR:  EQU 077233
KBDCHAR:    EQU 077234
COLNDX:     EQU 077236
COMMSETS:   EQU 077237
KBDLEDS:    EQU 077240
LDRCHECK:   EQU 077242
LDRCHECKMSB: EQU 077243
LDRBUF:     EQU 077244
LDRBUFMSB:  EQU 077245
LDRPTR:     EQU 077247
LDRPTRMSB:  EQU 077250
TESTCHAR:   EQU 077251
BLINKTMR:   EQU 077256
UNKN1:      EQU 077262
RAMSIZE:    EQU 077263
KEYOPTAH:   EQU 077264
DISABBITS:  EQU 077265
FLAGS:      EQU 077266
ALTCSET:    EQU 077270
ESCSEM:     EQU 077271
PARSETABPTR: EQU 077272
CURYPOS:    EQU 077320
CURXPOS:    EQU 077321
RXCHAR:     EQU 077324
PRCHAR:     EQU 077325
INTREGSL:   EQU 077347
INTREGSH:   EQU 077350
INTREGSC:   EQU 077351
TESTCSET:   EQU 077370
FREELIST:   EQU 077371
RAMTOP:     EQU 077372
RAMJMPLSB:  EQU 077374
RAMJMPMSB:  EQU 077375
DISPLSLSB:  EQU 077376
DISPLSMSB:  EQU 077377
DISPLISTS:  EQU 377377
LDRJMP:     EQU 077246
RAMJMP:     EQU 077373

            END
